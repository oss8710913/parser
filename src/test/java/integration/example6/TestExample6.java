package integration.example6;

import integration.example3.TestExample3;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;
import nl.novit.parser.util.StringParser;
import nl.novit.parser.util.StringParserDefault;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

interface MyTree
{
	MyTree left();

	MyTree right();

	Integer value();
}

class MyTreeNotLeaf
	implements MyTree
{
	public final MyTree left;
	public final MyTree right;

	public MyTreeNotLeaf(MyTree left, MyTree right)
	{
		this.left = left;
		this.right = right;
	}

	@Override
	public MyTree left()
	{
		return this.left;
	}

	@Override
	public MyTree right()
	{
		return this.right;
	}

	@Override
	public Integer value()
	{
		return null;
	}
}

class MyTreeLeaf
	implements MyTree
{
	public final Integer value;

	public MyTreeLeaf(Integer value)
	{
		this.value = value;
	}

	@Override
	public MyTree left()
	{
		return null;
	}

	@Override
	public MyTree right()
	{
		return null;
	}

	@Override
	public Integer value()
	{
		return this.value;
	}
}

public class TestExample6
	extends TestExample3
{
	public final RuleUnion<Character, MyTree> ruleMyTree;
	public final Rule<Character, MyTreeLeaf> ruleMyTreeLeaf;
	public final Rule<Character, MyTreeNotLeaf> ruleMyTreeNotLeaf;

	public TestExample6()
	{
		this.ruleMyTree = new RuleUnion<>();
		this.ruleMyTreeLeaf =
			identity1
				(
					MyTreeLeaf::new,
					this.ruleInteger
				);
		this.ruleMyTreeNotLeaf =
			identity2
				(
					MyTreeNotLeaf::new,
					concatenation11
						(
							concatenation01
								(
									dummy(terminal('(')),
									this.ruleMyTree
								),
							concatenation01
								(
									dummy(terminal(',')),
									concatenation10
										(
											this.ruleMyTree,
											dummy(terminal(')'))
										)
								)
						)
				);
		this.ruleMyTree.add(this.ruleMyTreeLeaf);
		this.ruleMyTree.add(this.ruleMyTreeNotLeaf);
	}

	@Test
	public void testMyTree()
		throws ExceptionParserInput
	{
		final StringParser<MyTree> parser = new StringParserDefault<>(new ParserLL1<>(this.ruleMyTree));
		final MyTree tree0 = parser.parse("34").getTarget();
		Assertions.assertNull(tree0.left());
		Assertions.assertNull(tree0.right());
		Assertions.assertEquals(34, tree0.value());
		final MyTree tree1 = parser.parse("(61,10)").getTarget();
		Assertions.assertEquals(61, tree1.left().value());
		Assertions.assertEquals(10, tree1.right().value());
		Assertions.assertNull(tree1.value());
		final MyTree tree2 = parser.parse("(27,((17,76),5))").getTarget();
		Assertions.assertEquals(27, tree2.left().value());
		Assertions.assertEquals(17, tree2.right().left().left().value());
		Assertions.assertEquals(76, tree2.right().left().right().value());
		Assertions.assertEquals(5, tree2.right().right().value());
		ExceptionParserInput exception = Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("(27,((17,76,5))"));
		Assertions.assertEquals(11, exception.index);
	}

	@Test
	public void testMyTrees()
		throws ExceptionParserInput
	{
		final Rule<Character, List<MyTree>> ruleMyTrees =
			repetition
				(
					this.ruleMyTree,
					dummy(terminal(','))
				);
		final StringParser<List<MyTree>> parser = new StringParserDefault<>(new ParserLL1<>(ruleMyTrees));
		final List<MyTree> trees0 = parser.parse("4").getTarget();
		Assertions.assertEquals(4, trees0.get(0).value());
		final List<MyTree> trees1 = parser.parse("6,38").getTarget();
		Assertions.assertEquals(38, trees1.get(1).value());
		final List<MyTree> trees2 = parser.parse("7,(87,(6,(41,90))),22").getTarget();
		Assertions.assertEquals(41, trees2.get(1).right().right().left().value());
		ExceptionParserInput exception = Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("7,(87,(6,((41,90))),22"));
		Assertions.assertEquals(17, exception.index);
	}
}