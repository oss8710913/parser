package integration.example1;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.Rules;
import nl.novit.parser.util.StringParser;
import nl.novit.parser.util.StringParserDefault;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestExample1
	extends Rules<Character>
{
	@Test
	public final void testA()
		throws ExceptionParserInput
	{
		final Rule<Character, Void> rule =
			dummy
				(
					terminal('a')
				);
		final StringParser<Void> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		parser.parse("a");
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("b"));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("aa"));
	}

	@Test
	public final void testAorB()
		throws ExceptionParserInput
	{
		final Rule<Character, Void> rule =
			union
				(
					dummy
						(
							terminal('a')
						),
					dummy
						(
							terminal('b')
						)
				);
		final StringParser<Void> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		parser.parse("a");
		parser.parse("b");
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("aa"));
	}

	@Test
	public final void testRepeatB()
		throws ExceptionParserInput
	{
		final Rule<Character, Void> rule =
			dummy
				(
					repetition
						(
							terminal('b')
						)
				);
		final StringParser<Void> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		parser.parse("b");
		parser.parse("bb");
		parser.parse("bbb");
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("a"));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("ba"));
	}

	@Test
	public final void testAOrRepeatB()
		throws ExceptionParserInput
	{
		final Rule<Character, Void> rule =
			union
				(
					dummy
						(
							terminal('a')
						),
					dummy
						(
							repetition
								(
									terminal('b')
								)
						)
				);
		final StringParser<Void> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		parser.parse("a");
		parser.parse("b");
		parser.parse("bb");
		parser.parse("bbb");
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("aa"));
	}

	@Test
	public final void testARepeatB()
		throws ExceptionParserInput
	{
		final Rule<Character, Void> rule =
			concatenation00
				(
					dummy
						(
							terminal('a')
						),
					dummy
						(
							repetition
								(
									terminal('b')
								)
						)
				);
		final StringParser<Void> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		parser.parse("ab");
		parser.parse("abb");
		parser.parse("abbb");
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("a"));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("b"));
	}

	@Test
	public final void testRepeatAOrB()
		throws ExceptionParserInput
	{
		final Rule<Character, Void> rule =
			dummy
				(
					repetition
						(
							union
								(
									terminal('a'),
									terminal('b')
								)
						)
				);
		final StringParser<Void> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		parser.parse("a");
		parser.parse("b");
		parser.parse("aa");
		parser.parse("ab");
		parser.parse("ba");
		parser.parse("bb");
		parser.parse("aaa");
		parser.parse("aab");
		parser.parse("aba");
		parser.parse("abb");
		parser.parse("baa");
		parser.parse("bab");
		parser.parse("bba");
		parser.parse("bbb");
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("c"));
	}

	@Test
	public void testAOptionalB()
		throws ExceptionParserInput
	{
		final Rule<Character, Void> rule =
			concatenation00
				(
					dummy
						(
							terminal('a')
						),
					dummy
						(
							option
								(
									terminal('b')
								)
						)
				);
		final StringParser<Void> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		parser.parse("a");
		parser.parse("ab");
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("b"));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("abb"));
	}
}