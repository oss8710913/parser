package integration.example7;

import integration.example3.TestExample3;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLALR1;
import nl.novit.parser.parser.Parser;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class TestExample7
	extends TestExample3
{
	public static final FactoryParser FACTORY_PARSER = FactoryParserLALR1.FACTORY_PARSER;
	final Parser<Character, List<Integer>> parser;

	public TestExample7()
	{
		this.parser = FACTORY_PARSER.parser(this.ruleDigits);
	}

	public static List<Character> generate(int size)
	{
		List<Character> result = new ArrayList<>();
		for (int index = 0; index < size; index++)
		{
			result.add((char) (index % 10 + 48));
		}
		return result;
	}

	public void testSpeed(int size)
		throws ExceptionParserInput
	{
		int tests = 10;
		System.out.print(size + ":\t");
		long start = System.nanoTime();
		for (int test = 0; test < tests; test++)
		{
			parser.parse(generate(size));
		}
		long end = System.nanoTime();
		long total = (end - start) / tests;
		System.out.print("\ttotal:\t" + (total / 1000000) + " ms");
		System.out.print("\tdivided:\t" + total / size + " ns");
		System.out.println();
	}

	@Test
	public void testSpeed()
		throws ExceptionParserInput
	{
		testSpeed(1);
		testSpeed(1 * 2);
		testSpeed(1 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
		testSpeed(1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2);
	}
}