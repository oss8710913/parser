package integration;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.StringParserDefault;

public abstract class TestsOneStage
	extends Tests<Character>
{
	public <TypeTarget> StringParserDefault<TypeTarget> parser(Rule<Character, TypeTarget> rule)
		throws ExceptionParserAmbiguous
	{
		return new StringParserDefault<>(factory().parser(rule));
	}
}