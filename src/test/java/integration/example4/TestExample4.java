package integration.example4;

import integration.example3.TestExample3;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.ParserLALR1;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;
import nl.novit.parser.util.StringParser;
import nl.novit.parser.util.StringParserDefault;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestExample4
	extends TestExample3
{
	public final RuleUnion<Character, Integer> ruleExpression;
	public final Rule<Character, Integer> rulePlus;
	public final Rule<Character, Integer> ruleMinus;

	public TestExample4()
	{
		super();
		this.ruleExpression = new RuleUnion<>();
		this.ruleExpression.add(this.ruleInteger);
		this.rulePlus =
			concatenation10
				(
					this.ruleExpression,
					dummy
						(
							terminal('+')
						)
				);
		this.ruleExpression.add
			(
				identity1
					(
						tuple -> tuple.value0 + tuple.value1,
						concatenation11
							(
								this.rulePlus,
								this.ruleInteger
							)
					)
			);
		this.ruleMinus =
			concatenation10
				(
					this.ruleExpression,
					dummy
						(
							terminal('-')
						)
				);
		this.ruleExpression.add
			(
				identity1
					(
						tuple -> tuple.value0 - tuple.value1,
						concatenation11
							(
								this.ruleMinus,
								this.ruleInteger
							)
					)
			);
	}

	@Test
	public void testRuleExpression()
		throws ExceptionParserInput
	{
		Assertions.assertThrows(ExceptionParserAmbiguous.class, () -> new ParserLL1<>(this.ruleExpression));
		final StringParser<Integer> parser = new StringParserDefault<>(new ParserLALR1<>(this.ruleExpression));
		Assertions.assertEquals(0, parser.parse("0").getTarget());
		Assertions.assertEquals(1, parser.parse("1+0").getTarget());
		Assertions.assertEquals(2, parser.parse("1+1").getTarget());
		Assertions.assertEquals(23, parser.parse("3+2+18").getTarget());
		Assertions.assertEquals(3 - 2 + 18, parser.parse("3-2+18").getTarget());
		Assertions.assertEquals(3 - 2 + 18 + 24 - 22 + 57, parser.parse("3-2+18+24-22+57").getTarget());
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("1-"));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("+1"));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("1+-1"));
	}
}