package integration.exception_parser_input;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLR1;

public class TestExceptionParserInput1LR
	extends TestExceptionParserInput
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLR1.FACTORY_PARSER;
	}
}