package integration.exception_parser_input;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLALR1;

public class TestExceptionParserInput1LALR
	extends TestExceptionParserInput
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLALR1.FACTORY_PARSER;
	}
}