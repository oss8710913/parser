package integration.exception_parser_input;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLL1;

public class TestExceptionParserInput1LL
	extends TestExceptionParserInput
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLL1.FACTORY_PARSER;
	}

	@Override
	public void testRule0()
	{
		assertAmbiguous(this.rule0);
	}
}