package integration.exception_parser_input;

import integration.TestsOneStage;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;
import nl.novit.parser.util.BuilderRule;
import org.junit.jupiter.api.Test;

import java.util.List;

public abstract class TestExceptionParserInput
	extends TestsOneStage
{
	public Rule<Character, List<Character>> terminalList(Character character)
	{
		return list(terminal(character));
	}

	public final Rule<Character, List<Character>> ruleM = terminalList('-');
	public final Rule<Character, List<Character>> ruleN = terminalList('n');
	public final Rule<Character, List<Character>> ruleO = terminalList('(');
	public final Rule<Character, List<Character>> ruleC = terminalList(')');
	public final Rule<Character, List<Character>> rule0 = new BuilderRule<Character, List<Character>>()
	{
		@Override
		public Rule<Character, List<Character>> rule()
		{
			RuleUnion<Character, List<Character>> result = new RuleUnion<>();
			Rule<Character, List<Character>> ruleRC = concatenationListList(result, ruleC);
			Rule<Character, List<Character>> ruleORC = concatenationListList(ruleO, ruleRC);
			Rule<Character, List<Character>> ruleNorORC = union(ruleN, ruleORC);
			Rule<Character, List<Character>> ruleMNorORC = concatenationListList(ruleM, ruleNorORC);
			Rule<Character, List<Character>> ruleEMNorORC = concatenationListList(result, ruleMNorORC);
			result.add(ruleNorORC);
			result.add(ruleEMNorORC);
			return result;
		}
	}.rule();
	public final Rule<Character, List<Character>> rule1 = new BuilderRule<Character, List<Character>>()
	{
		@Override
		public Rule<Character, List<Character>> rule()
		{
			RuleUnion<Character, List<Character>> result = new RuleUnion<>();
			Rule<Character, List<Character>> ruleRC = concatenationListList(result, ruleC);
			Rule<Character, List<Character>> ruleORC = concatenationListList(ruleO, ruleRC);
			Rule<Character, List<Character>> ruleNorORC = union(ruleN, ruleORC);
			Rule<Character, List<Character>> ruleMNorORC = concatenationListList(ruleM, ruleNorORC);
			RuleUnion<Character, List<Character>> ruleList = new RuleUnion<>();
			ruleList.add(emptyList());
			ruleList.add(concatenationListList(ruleMNorORC, ruleList));
			result.add(concatenationListList(ruleNorORC, ruleList));
			return result;
		}
	}.rule();

	public void test(Rule<Character, List<Character>> rule)
	{
		assertEquals(rule, "n");
		assertEquals(rule, "n-n");
		assertEquals(rule, "n-n-n");
		assertEquals(rule, "(n)");
		assertEquals(rule, "(n)-(n)");
		assertEquals(rule, "(n)-(n)-(n)");
		assertEquals(rule, "(n-n)");
		assertEquals(rule, "(n-n)-(n-n)");
		assertEquals(rule, "(n-n)-(n-n)-(n-n)");
		assertEquals(rule, "(n-n-n)");
		assertEquals(rule, "(n-n-n)-(n-n-n)");
		assertEquals(rule, "n-(n-n)");
		assertSyntaxIncorrect(rule, "", 0);
		assertSyntaxIncorrect(rule, "nn", 1);
		assertSyntaxIncorrect(rule, "n-", 2);
		assertSyntaxIncorrect(rule, "n-n-", 4);
		assertSyntaxIncorrect(rule, "(", 1);
		assertSyntaxIncorrect(rule, "(n", 2);
		assertSyntaxIncorrect(rule, "(n))", 3);
		assertSyntaxIncorrect(rule, "(n-n)-(n-n))", 11);
	}

	@Test
	public void testRule0()
	{
		test(this.rule0);
	}

	@Test
	public void testRule1()
	{
		test(this.rule1);
	}
}