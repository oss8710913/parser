package integration.exception_parser_input;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserUnger;

public class TestExceptionParserInputUnger
	extends TestExceptionParserInput
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserUnger.FACTORY_PARSER;
	}
}