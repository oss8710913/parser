package integration.parser;

import integration.TestsOneStage;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleConcatenation;
import nl.novit.parser.rule.RuleEmpty;
import nl.novit.parser.rule.RuleIdentity;
import nl.novit.parser.rule.RuleTerminal;
import nl.novit.parser.rule.RuleUnion;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public abstract class TestParserDefault
	extends TestsOneStage
{
	public final RuleEmpty<Character, List<Character>> ruleEmpty;
	public final RuleEmpty<Character, List<Character>> ruleEmptyCopy;
	public final RuleTerminal<Character, List<Character>> ruleA;
	public final RuleTerminal<Character, List<Character>> ruleACopy;
	public final RuleIdentity<Character, List<Character>, List<Character>> ruleIdentityA;
	public final RuleTerminal<Character, List<Character>> ruleB;
	public final RuleTerminal<Character, List<Character>> ruleC;
	public final RuleUnion<Character, List<Character>> ruleUnion;
	public final RuleUnion<Character, List<Character>> ruleUnionRecursive;
	public final RuleUnion<Character, List<Character>> ruleEmptyOrA;
	public final RuleUnion<Character, List<Character>> ruleAOrB;
	public final RuleUnion<Character, List<Character>> ruleEmptyOrEmptyCopy;
	public final RuleUnion<Character, List<Character>> ruleAOrACopy;
	public final RuleConcatenation<Character, List<Character>, List<Character>, List<Character>> ruleEmptyEmpty;
	public final RuleConcatenation<Character, List<Character>, List<Character>, List<Character>> ruleEmptyA;
	public final RuleConcatenation<Character, List<Character>, List<Character>, List<Character>> ruleEmptyB;
	public final RuleConcatenation<Character, List<Character>, List<Character>, List<Character>> ruleAEmpty;
	public final RuleConcatenation<Character, List<Character>, List<Character>, List<Character>> ruleAB;
	public final RuleConcatenation<Character, List<Character>, List<Character>, List<Character>> ruleAC;
	public final RuleConcatenation<Character, List<Character>, List<Character>, List<Character>> ruleIdentityAC;
	public final RuleUnion<Character, List<Character>> ruleABOrAC;
	public final RuleUnion<Character, List<Character>> ruleABOrIdentityAC;
	public final RuleUnion<Character, List<Character>> ruleRecursiveLeft;
	public final RuleUnion<Character, List<Character>> ruleRecursiveRight;
	public final RuleUnion<Character, List<Character>> ruleRecursiveEmptyLeft;
	public final RuleUnion<Character, List<Character>> ruleRecursiveEmptyRight;
	public final RuleConcatenation<Character, List<Character>, List<Character>, List<Character>> ruleEmptyOrAA;
	public final RuleUnion<Character, List<Character>> ruleEmptyAOrEmptyB;
	public final RuleUnion<Character, List<Character>> ruleInfinite;
	public final Rule<Character, List<Character>> ruleInfiniteA;
	public final Rule<Character, Void> ruleLALRvsLR_A;
	public final Rule<Character, Void> ruleLALRvsLR_B;
	public final Rule<Character, Void> ruleLALRvsLR_C;
	public final Rule<Character, Void> ruleLALRvsLR_D;
	public final Rule<Character, Void> ruleLALRvsLR_E;
	public final Rule<Character, Void> ruleLALRvsLR_F;
	public final Rule<Character, Void> ruleLALRvsLR;

	public TestParserDefault()
	{
		this.ruleEmpty = new RuleEmpty<>()
		{
			@Override
			public List<Character> getTarget()
			{
				return new ArrayList<>();
			}
		};
		this.ruleEmptyCopy = new RuleEmpty<>()
		{
			@Override
			public List<Character> getTarget()
			{
				return new ArrayList<>();
			}
		};
		this.ruleA = new RuleTerminal<>('a')
		{
			@Override
			public List<Character> getTarget()
			{
				return List.of('a');
			}
		};
		this.ruleACopy = new RuleTerminal<>('a')
		{
			@Override
			public List<Character> getTarget()
			{
				return List.of('a');
			}
		};
		this.ruleIdentityA = new RuleIdentity<>(this.ruleA)
		{
			@Override
			public List<Character> getTarget(List<Character> source)
			{
				return source;
			}
		};
		this.ruleB = new RuleTerminal<>('b')
		{
			@Override
			public List<Character> getTarget()
			{
				return List.of('b');
			}
		};
		this.ruleC = new RuleTerminal<>('c')
		{
			@Override
			public List<Character> getTarget()
			{
				return List.of('c');
			}
		};
		this.ruleUnion = new RuleUnion<>();
		this.ruleUnionRecursive = new RuleUnion<>();
		this.ruleUnionRecursive.add(this.ruleUnionRecursive);
		this.ruleEmptyOrA = new RuleUnion<>();
		this.ruleEmptyOrA.add(this.ruleEmpty);
		this.ruleEmptyOrA.add(this.ruleA);
		this.ruleAOrB = new RuleUnion<>();
		this.ruleAOrB.add(this.ruleA);
		this.ruleAOrB.add(this.ruleB);
		this.ruleEmptyOrEmptyCopy = new RuleUnion<>();
		this.ruleEmptyOrEmptyCopy.add(this.ruleEmpty);
		this.ruleEmptyOrEmptyCopy.add(this.ruleEmptyCopy);
		this.ruleAOrACopy = new RuleUnion<>();
		this.ruleAOrACopy.add(this.ruleA);
		this.ruleAOrACopy.add(this.ruleACopy);
		this.ruleEmptyEmpty = new RuleConcatenation<>(this.ruleEmpty, this.ruleEmpty)
		{
			@Override
			public List<Character> getTarget(List<Character> left, List<Character> right)
			{
				List<Character> result = new ArrayList<>(left);
				result.addAll(right);
				return result;
			}
		};
		this.ruleEmptyA = new RuleConcatenation<>(this.ruleEmpty, this.ruleA)
		{
			@Override
			public List<Character> getTarget(List<Character> left, List<Character> right)
			{
				List<Character> result = new ArrayList<>(left);
				result.addAll(right);
				return result;
			}
		};
		this.ruleEmptyB = new RuleConcatenation<>(this.ruleEmpty, this.ruleB)
		{
			@Override
			public List<Character> getTarget(List<Character> left, List<Character> right)
			{
				List<Character> result = new ArrayList<>(left);
				result.addAll(right);
				return result;
			}
		};
		this.ruleAEmpty = new RuleConcatenation<>(this.ruleA, this.ruleEmpty)
		{
			@Override
			public List<Character> getTarget(List<Character> left, List<Character> right)
			{
				List<Character> result = new ArrayList<>(left);
				result.addAll(right);
				return result;
			}
		};
		this.ruleAB = new RuleConcatenation<>(this.ruleA, this.ruleB)
		{
			@Override
			public List<Character> getTarget(List<Character> left, List<Character> right)
			{
				List<Character> result = new ArrayList<>(left);
				result.addAll(right);
				return result;
			}
		};
		this.ruleAC = new RuleConcatenation<>(this.ruleA, this.ruleC)
		{
			@Override
			public List<Character> getTarget(List<Character> left, List<Character> right)
			{
				List<Character> result = new ArrayList<>(left);
				result.addAll(right);
				return result;
			}
		};
		this.ruleIdentityAC = new RuleConcatenation<>(this.ruleIdentityA, this.ruleC)
		{
			@Override
			public List<Character> getTarget(List<Character> left, List<Character> right)
			{
				List<Character> result = new ArrayList<>(left);
				result.addAll(right);
				return result;
			}
		};
		this.ruleABOrAC = new RuleUnion<>();
		this.ruleABOrAC.add(this.ruleAB);
		this.ruleABOrAC.add(this.ruleAC);
		this.ruleABOrIdentityAC = new RuleUnion<>();
		this.ruleABOrIdentityAC.add(this.ruleAB);
		this.ruleABOrIdentityAC.add(this.ruleIdentityAC);
		this.ruleRecursiveLeft = new RuleUnion<>();
		this.ruleRecursiveLeft.add(this.ruleA);
		this.ruleRecursiveLeft.add
			(
				new RuleConcatenation<>(this.ruleRecursiveLeft, this.ruleA)
				{
					@Override
					public List<Character> getTarget(List<Character> left, List<Character> right)
					{
						List<Character> result = new ArrayList<>(left);
						result.addAll(right);
						return result;
					}
				}
			);
		this.ruleRecursiveRight = new RuleUnion<>();
		this.ruleRecursiveRight.add(this.ruleA);
		this.ruleRecursiveRight.add
			(
				new RuleConcatenation<>(this.ruleA, this.ruleRecursiveRight)
				{
					@Override
					public List<Character> getTarget(List<Character> left, List<Character> right)
					{
						List<Character> result = new ArrayList<>(left);
						result.addAll(right);
						return result;
					}
				}
			);
		this.ruleRecursiveEmptyLeft = new RuleUnion<>();
		this.ruleRecursiveEmptyLeft.add(this.ruleEmpty);
		this.ruleRecursiveEmptyLeft.add
			(
				new RuleConcatenation<>(this.ruleRecursiveEmptyLeft, this.ruleA)
				{
					@Override
					public List<Character> getTarget(List<Character> left, List<Character> right)
					{
						List<Character> result = new ArrayList<>(left);
						result.addAll(right);
						return result;
					}
				}
			);
		this.ruleRecursiveEmptyRight = new RuleUnion<>();
		this.ruleRecursiveEmptyRight.add(this.ruleEmpty);
		this.ruleRecursiveEmptyRight.add
			(
				new RuleConcatenation<>(this.ruleA, this.ruleRecursiveEmptyRight)
				{
					@Override
					public List<Character> getTarget(List<Character> left, List<Character> right)
					{
						List<Character> result = new ArrayList<>(left);
						result.addAll(right);
						return result;
					}
				}
			);
		this.ruleEmptyOrAA = new RuleConcatenation<>(this.ruleEmptyOrA, this.ruleA)
		{
			@Override
			public List<Character> getTarget(List<Character> left, List<Character> right)
			{
				List<Character> result = new ArrayList<>(left);
				result.addAll(right);
				return result;
			}
		};
		this.ruleEmptyAOrEmptyB = new RuleUnion<>();
		this.ruleEmptyAOrEmptyB.add(this.ruleEmptyA);
		this.ruleEmptyAOrEmptyB.add(this.ruleEmptyB);
		this.ruleInfinite = new RuleUnion<>();
		this.ruleInfinite.add(this.ruleInfinite);
		this.ruleInfiniteA = new RuleConcatenation<>(this.ruleInfinite, this.ruleA)
		{
			@Override
			public List<Character> getTarget(List<Character> left, List<Character> right)
			{
				return null;
			}
		};
		this.ruleLALRvsLR_A = dummy(terminal('a'));
		this.ruleLALRvsLR_B = dummy(terminal('b'));
		this.ruleLALRvsLR_C = union(this.ruleLALRvsLR_A);
		this.ruleLALRvsLR_D = concatenation00(this.ruleLALRvsLR_C, this.ruleLALRvsLR_B);
		this.ruleLALRvsLR_E = union(this.ruleLALRvsLR_A, this.ruleLALRvsLR_D);
		this.ruleLALRvsLR_F = concatenation00(this.ruleLALRvsLR_E, this.ruleLALRvsLR_E);
		this.ruleLALRvsLR = union(this.ruleLALRvsLR_C, this.ruleLALRvsLR_F);
	}

	@Test
	public void testRuleEmpty()
	{
		assertEquals(this.ruleEmpty, "");
		assertSyntaxIncorrect(this.ruleEmpty, "a", 0);
	}

	@Test
	public void testRuleA()
	{
		assertEquals(this.ruleA, "a");
		assertSyntaxIncorrect(this.ruleA, "", 0);
		assertSyntaxIncorrect(this.ruleA, "b", 0);
		assertSyntaxIncorrect(this.ruleA, "aa", 1);
	}

	@Test
	public void testRuleUnion()
	{
		assertSyntaxIncorrect(this.ruleUnion, "", 0);
		assertSyntaxIncorrect(this.ruleUnion, "a", 0);
	}

	@Test
	public void testRuleUnionRecursive()
	{
		assertSyntaxIncorrect(this.ruleUnionRecursive, "", 0);
		assertSyntaxIncorrect(this.ruleUnionRecursive, "a", 0);
	}

	@Test
	public void tesruletEmptyOrA()
	{
		assertEquals(this.ruleEmptyOrA, "");
		assertEquals(this.ruleEmptyOrA, "a");
		assertSyntaxIncorrect(this.ruleEmptyOrA, "b", 0);
		assertSyntaxIncorrect(this.ruleEmptyOrA, "aa", 1);
	}

	@Test
	public void testRuleAOrB()
	{
		assertEquals(this.ruleAOrB, "a");
		assertEquals(this.ruleAOrB, "b");
		assertSyntaxIncorrect(this.ruleAOrB, "", 0);
		assertSyntaxIncorrect(this.ruleAOrB, "aa", 1);
	}

	@Test
	public void testRuleEmptyEmpty()
	{
		assertEquals(this.ruleEmptyEmpty, "");
		assertSyntaxIncorrect(this.ruleEmptyEmpty, "a", 0);
	}

	@Test
	public void testRuleEmptyA()
	{
		assertEquals(this.ruleEmptyA, "a");
		assertSyntaxIncorrect(this.ruleEmptyA, "", 0);
		assertSyntaxIncorrect(this.ruleEmptyA, "aa", 1);
	}

	@Test
	public void testRuleAEmpty()
	{
		assertEquals(this.ruleAEmpty, "a");
		assertSyntaxIncorrect(this.ruleAEmpty, "", 0);
		assertSyntaxIncorrect(this.ruleAEmpty, "aa", 1);
	}

	@Test
	public void testRuleAB()
	{
		assertEquals(this.ruleAB, "ab");
		assertSyntaxIncorrect(this.ruleAB, "", 0);
		assertSyntaxIncorrect(this.ruleAB, "a", 1);
		assertSyntaxIncorrect(this.ruleAB, "b", 0);
		assertSyntaxIncorrect(this.ruleAB, "aa", 1);
		assertSyntaxIncorrect(this.ruleAB, "bb", 0);
		assertSyntaxIncorrect(this.ruleAB, "aba", 2);
		assertSyntaxIncorrect(this.ruleAB, "abb", 2);
	}

	public void testRuleABOrACUnambiguous()
	{
		assertEquals(this.ruleABOrAC, "ab");
		assertEquals(this.ruleABOrAC, "ac");
		assertSyntaxIncorrect(this.ruleABOrAC, "", 0);
		assertSyntaxIncorrect(this.ruleABOrAC, "a", 1);
		assertSyntaxIncorrect(this.ruleABOrAC, "b", 0);
		assertSyntaxIncorrect(this.ruleABOrAC, "c", 0);
		assertSyntaxIncorrect(this.ruleABOrAC, "aa", 1);
		assertSyntaxIncorrect(this.ruleABOrAC, "bb", 0);
		assertSyntaxIncorrect(this.ruleABOrAC, "cc", 0);
		assertSyntaxIncorrect(this.ruleABOrAC, "aba", 2);
		assertSyntaxIncorrect(this.ruleABOrAC, "aca", 2);
		assertSyntaxIncorrect(this.ruleABOrAC, "abb", 2);
		assertSyntaxIncorrect(this.ruleABOrAC, "acc", 2);
		assertSyntaxIncorrect(this.ruleABOrAC, "abac", 2);
	}

	@Test
	public abstract void testRuleABOrAC();

	@Test
	public abstract void testRuleABOrIdentityAC();

	@Test
	public abstract void testRuleEmptyOrEmptyCopy();

	@Test
	public abstract void testRuleAOrACopy();

	public void testRuleRecursiveLeftUnambiguous()
	{
		assertEquals(this.ruleRecursiveLeft, "a");
		assertEquals(this.ruleRecursiveLeft, "aa");
		assertEquals(this.ruleRecursiveLeft, "aaa");
		assertSyntaxIncorrect(this.ruleRecursiveLeft, "", 0);
	}

	@Test
	public abstract void testRuleRecursiveLeft();

	public void testRuleRecursiveRightUnambiguous()
	{
		assertEquals(this.ruleRecursiveRight, "a");
		assertEquals(this.ruleRecursiveRight, "aa");
		assertEquals(this.ruleRecursiveRight, "aaa");
		assertSyntaxIncorrect(this.ruleRecursiveRight, "", 0);
	}

	@Test
	public abstract void testRuleRecursiveRight();

	public void testRuleRecursiveEmptyLeftUnambiguous()
	{
		assertEquals(this.ruleRecursiveEmptyLeft, "");
		assertEquals(this.ruleRecursiveEmptyLeft, "a");
		assertEquals(this.ruleRecursiveEmptyLeft, "aa");
		assertEquals(this.ruleRecursiveEmptyLeft, "aaa");
	}

	@Test
	public abstract void testRuleRecursiveEmptyLeft();

	@Test
	public void testRuleRecursiveEmptyRight()
	{
		assertEquals(this.ruleRecursiveEmptyRight, "");
		assertEquals(this.ruleRecursiveEmptyRight, "a");
		assertEquals(this.ruleRecursiveEmptyRight, "aa");
		assertEquals(this.ruleRecursiveEmptyRight, "aaa");
	}

	@Test
	public abstract void testRuleAOrAA();

	@Test
	public void testRuleEmptyAOrEmptyB()
	{
		assertSyntaxIncorrect(this.ruleEmptyAOrEmptyB, "", 0);
		assertEquals(this.ruleEmptyAOrEmptyB, "a");
		assertEquals(this.ruleEmptyAOrEmptyB, "b");
	}

	@Test
	public void testRuleInfinite()
	{
		assertUnambiguous(this.ruleInfinite);
	}

	@Test
	public abstract void testRuleInfiniteA();

	@Test
	public abstract void testRuleRuleLALRvsLR();
}