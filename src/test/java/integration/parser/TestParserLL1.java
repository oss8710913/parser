package integration.parser;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLL1;
import org.junit.jupiter.api.Test;

public class TestParserLL1
	extends TestParserDefault
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLL1.FACTORY_PARSER;
	}

	@Test
	@Override
	public void testRuleABOrAC()
	{
		assertAmbiguous(this.ruleABOrAC);
	}

	@Test
	@Override
	public void testRuleABOrIdentityAC()
	{
		assertAmbiguous(this.ruleABOrIdentityAC);
	}

	@Test
	@Override
	public void testRuleEmptyOrEmptyCopy()
	{
		assertAmbiguous(this.ruleEmptyOrEmptyCopy);
	}

	@Test
	@Override
	public void testRuleAOrACopy()
	{
		assertAmbiguous(this.ruleAOrACopy);
	}

	@Test
	@Override
	public void testRuleRecursiveLeft()
	{
		assertAmbiguous(this.ruleRecursiveLeft);
	}

	@Test
	@Override
	public void testRuleRecursiveRight()
	{
		assertAmbiguous(this.ruleRecursiveRight);
	}

	@Test
	@Override
	public void testRuleRecursiveEmptyLeft()
	{
		assertAmbiguous(this.ruleRecursiveEmptyLeft);
	}

	@Test
	@Override
	public void testRuleAOrAA()
	{
		assertAmbiguous(this.ruleEmptyOrAA);
	}

	@Test
	@Override
	public void testRuleInfiniteA()
	{
		assertUnambiguous(this.ruleInfiniteA);
	}

	@Test
	@Override
	public void testRuleRuleLALRvsLR()
	{
		assertAmbiguous(this.ruleLALRvsLR);
	}
}