package integration.parser;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserUnger;
import org.junit.jupiter.api.Test;

public class TestParserUnger
	extends TestParserDefault
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserUnger.FACTORY_PARSER;
	}

	@Test
	@Override
	public void testRuleABOrAC()
	{
		testRuleABOrACUnambiguous();
	}

	@Test
	@Override
	public void testRuleABOrIdentityAC()
	{
		testRuleABOrACUnambiguous();
	}

	@Test
	@Override
	public void testRuleEmptyOrEmptyCopy()
	{
	}

	@Test
	@Override
	public void testRuleAOrACopy()
	{
	}

	@Test
	@Override
	public void testRuleRecursiveLeft()
	{
		testRuleRecursiveLeftUnambiguous();
	}

	@Test
	@Override
	public void testRuleRecursiveRight()
	{
		testRuleRecursiveRightUnambiguous();
	}

	@Test
	@Override
	public void testRuleRecursiveEmptyLeft()
	{
		testRuleRecursiveEmptyLeftUnambiguous();
	}

	@Test
	@Override
	public void testRuleAOrAA()
	{
		assertEquals(this.ruleEmptyOrAA, "a");
		assertEquals(this.ruleEmptyOrAA, "aa");
		assertSyntaxIncorrect(this.ruleEmptyOrAA, "", 0);
		assertSyntaxIncorrect(this.ruleEmptyOrAA, "aaa", 2);
	}

	@Test
	@Override
	public void testRuleInfiniteA()
	{
		assertUnambiguous(this.ruleInfiniteA);
	}

	@Test
	@Override
	public void testRuleRuleLALRvsLR()
	{
		assertUnambiguous(this.ruleLALRvsLR);
	}
}