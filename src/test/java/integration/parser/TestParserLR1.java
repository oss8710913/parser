package integration.parser;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLR1;
import org.junit.jupiter.api.Test;

public class TestParserLR1
	extends TestParserDefault
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLR1.FACTORY_PARSER;
	}

	@Test
	@Override
	public void testRuleABOrAC()
	{
		testRuleABOrACUnambiguous();
	}

	@Test
	@Override
	public void testRuleABOrIdentityAC()
	{
		testRuleABOrACUnambiguous();
	}

	@Test
	@Override
	public void testRuleEmptyOrEmptyCopy()
	{
		assertAmbiguous(this.ruleEmptyOrEmptyCopy);
	}

	@Test
	@Override
	public void testRuleAOrACopy()
	{
		assertAmbiguous(this.ruleAOrACopy);
	}

	@Test
	@Override
	public void testRuleRecursiveLeft()
	{
		testRuleRecursiveLeftUnambiguous();
	}

	@Test
	@Override
	public void testRuleRecursiveRight()
	{
		testRuleRecursiveRightUnambiguous();
	}

	@Test
	@Override
	public void testRuleRecursiveEmptyLeft()
	{
		testRuleRecursiveEmptyLeftUnambiguous();
	}

	@Test
	@Override
	public void testRuleAOrAA()
	{
		assertAmbiguous(this.ruleEmptyOrAA);
	}

	@Test
	@Override
	public void testRuleInfiniteA()
	{
		assertAmbiguous(this.ruleInfiniteA);
	}

	@Test
	@Override
	public void testRuleRuleLALRvsLR()
	{
		assertUnambiguous(this.ruleLALRvsLR);
	}
}