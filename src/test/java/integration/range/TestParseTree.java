package integration.range;

import integration.TestsOneStage;
import nl.novit.parser.common.ParseTree;
import nl.novit.parser.rule.Rule;
import nl.novit.util.Tuple2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public abstract class TestParseTree
	extends TestsOneStage
{
	@Test
	public void testAB()
	{
		final Rule<Character, Tuple2<Character, Character>> rule =
			concatenation11
				(
					terminal('a'),
					terminal('b')
				);
		Assertions.assertDoesNotThrow
			(
				() ->
				{
					final String input = "ab";
					ParseTree<? extends Tuple2<Character, Character>> actual = parser(rule).parse(input);
					Assertions.assertEquals(new Tuple2<>('a', 'b'), actual.getTarget());
					Assertions.assertEquals("a", substring(input, actual.find(actual.getTarget().value0).getRange()));
					Assertions.assertEquals("b", substring(input, actual.find(actual.getTarget().value1).getRange()));
					Assertions.assertEquals("ab", substring(input, actual.find(actual.getTarget()).getRange()));
				}
			);
	}

	@Test
	public void testAXBX()
	{
		final Rule<Character, Tuple2<Character, Character>> rule =
			concatenation10
				(
					concatenation11
						(
							concatenation10
								(
									terminal('a'),
									dummy(terminal('x'))
								),
							terminal('b')
						),
					dummy(terminal('x'))
				);
		Assertions.assertDoesNotThrow
			(
				() ->
				{
					final String input = "axbx";
					ParseTree<? extends Tuple2<Character, Character>> actual = parser(rule).parse(input);
					Assertions.assertEquals(new Tuple2<>('a', 'b'), actual.getTarget());
					Assertions.assertEquals("a", substring(input, actual.find(actual.getTarget().value0).getRange()));
					Assertions.assertEquals("b", substring(input, actual.find(actual.getTarget().value1).getRange()));
					Assertions.assertEquals("axb", substring(input, actual.find(actual.getTarget()).getRange()));
				}
			);
	}

	@Test
	public void testABC()
	{
		final Rule<Character, Tuple2<Character, Tuple2<Character, Character>>> rule =
			concatenation11
				(
					terminal('a'),
					concatenation11
						(
							terminal('b'),
							union
								(
									terminal('c')
								)
						)
				);
		Assertions.assertDoesNotThrow
			(
				() ->
				{
					final String input = "abc";
					ParseTree<? extends Tuple2<Character, Tuple2<Character, Character>>> actual = parser(rule).parse(input);
					Assertions.assertEquals(new Tuple2<>('a', new Tuple2<>('b', 'c')), actual.getTarget());
					Assertions.assertEquals("a", substring(input, actual.find(actual.getTarget().value0).getRange()));
					Assertions.assertEquals("b", substring(input, actual.find(actual.getTarget().value1.value0).getRange()));
					Assertions.assertEquals("c", substring(input, actual.find(actual.getTarget().value1.value1).getRange()));
					Assertions.assertEquals("bc", substring(input, actual.find(actual.getTarget().value1).getRange()));
					Assertions.assertEquals("abc", substring(input, actual.find(actual.getTarget()).getRange()));
				}
			);
	}
}