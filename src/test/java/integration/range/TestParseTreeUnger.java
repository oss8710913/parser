package integration.range;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserUnger;

public class TestParseTreeUnger
	extends TestParseTree
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserUnger.FACTORY_PARSER;
	}
}