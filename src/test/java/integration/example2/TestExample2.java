package integration.example2;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.Rules;
import nl.novit.parser.util.StringParser;
import nl.novit.parser.util.StringParserDefault;
import nl.novit.util.Tuple2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestExample2
	extends Rules<Character>
{
	@Test
	public final void testAOrRepeatB()
		throws ExceptionParserInput
	{
		final Rule<Character, List<Character>> rule =
			union
				(
					list
						(
							terminal('a')
						),
					repetition
						(
							terminal('b')
						)
				);
		final StringParser<List<Character>> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		Assertions.assertEquals(List.of('a'), parser.parse("a").getTarget());
		Assertions.assertEquals(List.of('b'), parser.parse("b").getTarget());
		Assertions.assertEquals(List.of('b', 'b'), parser.parse("bb").getTarget());
		Assertions.assertEquals(List.of('b', 'b', 'b'), parser.parse("bbb").getTarget());
	}

	@Test
	public final void testRepeatAOrB()
		throws ExceptionParserInput
	{
		final Rule<Character, List<Character>> rule =
			repetition
				(
					union
						(
							terminal('a'),
							terminal('b')
						)
				);
		final StringParser<List<Character>> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		Assertions.assertEquals(List.of('a'), parser.parse("a").getTarget());
		Assertions.assertEquals(List.of('b'), parser.parse("b").getTarget());
		Assertions.assertEquals(List.of('a', 'a'), parser.parse("aa").getTarget());
		Assertions.assertEquals(List.of('a', 'b'), parser.parse("ab").getTarget());
		Assertions.assertEquals(List.of('b', 'a'), parser.parse("ba").getTarget());
		Assertions.assertEquals(List.of('b', 'b'), parser.parse("bb").getTarget());
		Assertions.assertEquals(List.of('a', 'a', 'a'), parser.parse("aaa").getTarget());
		Assertions.assertEquals(List.of('a', 'a', 'b'), parser.parse("aab").getTarget());
		Assertions.assertEquals(List.of('a', 'b', 'a'), parser.parse("aba").getTarget());
		Assertions.assertEquals(List.of('a', 'b', 'b'), parser.parse("abb").getTarget());
		Assertions.assertEquals(List.of('b', 'a', 'a'), parser.parse("baa").getTarget());
		Assertions.assertEquals(List.of('b', 'a', 'b'), parser.parse("bab").getTarget());
		Assertions.assertEquals(List.of('b', 'b', 'a'), parser.parse("bba").getTarget());
		Assertions.assertEquals(List.of('b', 'b', 'b'), parser.parse("bbb").getTarget());
	}

	@Test
	public void testAOptionalB()
		throws ExceptionParserInput
	{
		final Rule<Character, Tuple2<Character, Character>> rule =
			concatenation11
				(
					terminal('a'),
					option
						(
							terminal('b')
						)
				);
		final StringParser<Tuple2<Character, Character>> parser = new StringParserDefault<>(new ParserLL1<>(rule));
		Tuple2<Character, Character> actual = parser.parse("a").getTarget();
		Assertions.assertEquals('a', actual.value0);
		Assertions.assertNull(actual.value1);
		Assertions.assertEquals(new Tuple2<>('a', 'b'), parser.parse("ab").getTarget());
	}
}