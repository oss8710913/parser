package integration.expression;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLL1;

public class TestExpressionLL1
	extends TestExpression
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLL1.FACTORY_PARSER;
	}
}