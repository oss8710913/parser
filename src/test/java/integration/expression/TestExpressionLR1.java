package integration.expression;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLR1;

public class TestExpressionLR1
	extends TestExpression
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLR1.FACTORY_PARSER;
	}
}