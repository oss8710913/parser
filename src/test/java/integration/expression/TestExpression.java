package integration.expression;

import integration.TestsOneStage;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleTerminal;
import nl.novit.parser.rule.RuleUnion;
import nl.novit.parser.util.BuilderRule;
import nl.novit.util.Algorithm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.List;

public abstract class TestExpression
	extends TestsOneStage
{
	public static BigInteger faculty(BigInteger n)
	{
		return n.equals(BigInteger.ZERO) ? BigInteger.ONE : n.multiply(faculty(n.subtract(BigInteger.ONE)));
	}

	public static BigInteger facultyRepetition(BigInteger n, Integer repetitions)
	{
		return repetitions.equals(0) ? n : faculty(facultyRepetition(n, repetitions - 1));
	}

	public static final Rule<Character, BigInteger> RULE_EXPRESSION =
		new BuilderRule<Character, BigInteger>()
		{
			private void addDigit(RuleUnion<Character, BigInteger> ruleDigit, final Character digit)
			{
				ruleDigit.add(new RuleTerminal<>(digit)
				{
					public BigInteger getTarget()
					{
						return new BigInteger(digit.toString());
					}
				});
			}

			@Override
			public Rule<Character, BigInteger> rule()
			{
				RuleUnion<Character, BigInteger> ruleDigit = new RuleUnion<>();
				addDigit(ruleDigit, '0');
				addDigit(ruleDigit, '1');
				addDigit(ruleDigit, '2');
				addDigit(ruleDigit, '3');
				addDigit(ruleDigit, '4');
				addDigit(ruleDigit, '5');
				addDigit(ruleDigit, '6');
				addDigit(ruleDigit, '7');
				addDigit(ruleDigit, '8');
				addDigit(ruleDigit, '9');
				Rule<Character, BigInteger> ruleInteger = identity1
					(
						digits ->
						{
							BigInteger result = BigInteger.ZERO;
							for (BigInteger digit: digits)
							{
								result = result.multiply(BigInteger.TEN);
								result = result.add(digit);
							}
							return result;
						},
						repetition(ruleDigit)
					);
				RuleUnion<Character, BigInteger> ruleFactor = new RuleUnion<>();
				RuleUnion<Character, Integer> ruleFacultyList = new RuleUnion<>();
				ruleFacultyList.add
					(
						identity1(x -> 0, empty())
					);
				ruleFacultyList.add
					(
						identity1
							(
								x -> x + 1,
								concatenation01
									(
										dummy(terminal('!')),
										ruleFacultyList
									)
							)
					);
				Rule<Character, BigInteger> ruleFaculty =
					identity2
						(
							TestExpression::facultyRepetition,
							concatenation11
								(
									ruleFactor,
									ruleFacultyList
								)
						);
				RuleUnion<Character, BigInteger> ruleFactorPlusMin = new RuleUnion<>();
				ruleFactorPlusMin.add(ruleFaculty);
				ruleFactorPlusMin.add
					(
						identity1
							(
								BigInteger::negate,
								concatenation01
									(
										dummy(terminal('-')),
										ruleFactorPlusMin
									)
							)
					);
				Rule<Character, BigInteger> ruleProduct = identity1
					(
						factors -> factors.stream().reduce(BigInteger.ONE, BigInteger::multiply),
						repetition(ruleFactorPlusMin, dummy(terminal('*')))
					);
				Rule<Character, BigInteger> ruleSum = concatenation01(dummy(terminal('+')), ruleProduct);
				Rule<Character, BigInteger> ruleSub = concatenation01(dummy(terminal('-')), identity1(BigInteger::negate, ruleProduct));
				RuleUnion<Character, List<BigInteger>> ruleSumSubList = new RuleUnion<>();
				ruleSumSubList.add(emptyList());
				ruleSumSubList.add(concatenationElementList(ruleSum, ruleSumSubList));
				ruleSumSubList.add(concatenationElementList(ruleSub, ruleSumSubList));
				Rule<Character, BigInteger> ruleSumSub =
					identity1
						(
							products -> products.stream().reduce(BigInteger.ZERO, BigInteger::add),
							concatenationElementList
								(
									ruleProduct,
									ruleSumSubList
								)
						);
				ruleFactor.add(ruleInteger);
				ruleFactor.add
					(
						concatenation01
							(
								dummy(terminal('(')),
								concatenation10
									(
										ruleSumSub,
										dummy(terminal(')'))
									)
							)
					);
				return ruleSumSub;
			}
		}.rule();

	public void testRULE_EXPRESSION(long expected, String actual)
	{
		assertEquals(RULE_EXPRESSION, actual, BigInteger.valueOf(expected));
	}

	@Test
	public void testFaculty()
	{
		Assertions.assertEquals(BigInteger.ONE, faculty(BigInteger.ZERO));
		Assertions.assertEquals(BigInteger.ONE, faculty(BigInteger.ONE));
		Assertions.assertEquals(BigInteger.valueOf(2), faculty(BigInteger.valueOf(2)));
		Assertions.assertEquals(BigInteger.valueOf(2 * 3), faculty(BigInteger.valueOf(3)));
		Assertions.assertEquals(BigInteger.valueOf(2 * 3 * 4), faculty(BigInteger.valueOf(4)));
	}

	@Test
	public void testFacultyRepetition()
	{
		BigInteger n = BigInteger.valueOf(3);
		Assertions.assertEquals(faculty(faculty(n)), facultyRepetition(n, 2));
		Assertions.assertEquals(faculty(faculty(faculty(n))), facultyRepetition(n, 3));
	}

	@Test
	public void testRULE_EXPRESSION()
	{
		testRULE_EXPRESSION(4, "4");
		testRULE_EXPRESSION(42, "42");
		testRULE_EXPRESSION((42), "(42)");
		testRULE_EXPRESSION(42 + 6, "42+6");
		testRULE_EXPRESSION(42 + (6), "42+(6)");
		testRULE_EXPRESSION(42 + 6 + 231, "42+6+231");
		testRULE_EXPRESSION(57 * 8, "57*8");
		testRULE_EXPRESSION(57 * 8 * 67, "57*8*67");
		testRULE_EXPRESSION(57 * 8 * 67, "57*8*67");
		testRULE_EXPRESSION(42 + 57 * 8, "42+57*8");
		testRULE_EXPRESSION(57 * (3 + 2), "57*(3+2)");
		testRULE_EXPRESSION(18 + 57 * (3 + 2) * 4, "18+57*(3+2)*4");
		testRULE_EXPRESSION(-5, "-5");
		testRULE_EXPRESSION(3 * -5, "3*-5");
		testRULE_EXPRESSION(3 * (-5), "3*(-5)");
		testRULE_EXPRESSION(3 * -(5), "3*-(5)");
		testRULE_EXPRESSION(-3 * -(5), "-3*-(5)");
		testRULE_EXPRESSION(4 + -6, "4+-6");
		testRULE_EXPRESSION(4 - -6, "4--6");
		testRULE_EXPRESSION(4 - 5 - 6, "4-5-6");
		testRULE_EXPRESSION(4 - 5 - -6 - 7, "4-5--6-7");
		testRULE_EXPRESSION(3 * -(2 * 3), "3*-(2*3)");
		testRULE_EXPRESSION(2, "--2");
		testRULE_EXPRESSION(2, "----2");
		testRULE_EXPRESSION(2, "----2");
		testRULE_EXPRESSION(4 + 2, "4+----2");
		testRULE_EXPRESSION(4 * 3 * 2, "4!");
		testRULE_EXPRESSION(Algorithm.faculty(3 * 2), "3!!");
		testRULE_EXPRESSION(Algorithm.faculty(3 * 2) + 4 * 3 * 2 - Algorithm.faculty(8 - 3), "3!!+4!-(8-3)!");
		assertSyntaxIncorrect(RULE_EXPRESSION, "", 0);
		assertSyntaxIncorrect(RULE_EXPRESSION, "1+", 2);
		assertSyntaxIncorrect(RULE_EXPRESSION, "(", 1);
	}
}