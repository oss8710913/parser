package integration;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.common.Range;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.Rules;
import nl.novit.parser.util.StringParser;
import nl.novit.util.Util;
import nl.novit.util.Wrapper;
import org.junit.jupiter.api.Assertions;

import java.util.List;

public abstract class Tests<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends Rules<TypeTerminal>
{
	public static String substring(String string, Range range)
	{
		return string.substring(range.start(), range.end());
	}

	public abstract FactoryParser factory();

	public abstract <TypeTarget> StringParser<TypeTarget> parser(Rule<TypeTerminal, TypeTarget> rule)
		throws ExceptionParserAmbiguous;

	public <TypeTarget> ParseTree<? extends TypeTarget> assertSyntaxCorrect(final Rule<TypeTerminal, TypeTarget> rule, final String input)
	{
		final Wrapper<ParseTree<? extends TypeTarget>> wrapper = new Wrapper<>();
		Assertions.assertDoesNotThrow(() -> wrapper.value = parser(rule).parse(input));
		return wrapper.value;
	}

	public <TypeTarget> void assertSyntaxIncorrect(final Rule<TypeTerminal, TypeTarget> rule, final String input, int index)
	{
		Assertions.assertEquals(index, Assertions.assertThrows(ExceptionParserInput.class, () -> parser(rule).parse(input)).index);
	}

	public <TypeTarget> void assertNull(final Rule<TypeTerminal, TypeTarget> rule, final String input)
	{
		Assertions.assertNull(assertSyntaxCorrect(rule, input).getTarget());
	}

	public void assertTrue(final Rule<TypeTerminal, Boolean> rule, final String input)
	{
		Assertions.assertTrue(assertSyntaxCorrect(rule, input).getTarget());
	}

	public void assertFalse(final Rule<TypeTerminal, Boolean> rule, final String input)
	{
		Assertions.assertFalse(assertSyntaxCorrect(rule, input).getTarget());
	}

	public <TypeTarget> void assertEquals(final Rule<TypeTerminal, TypeTarget> rule, final String input, final TypeTarget target)
	{
		Assertions.assertEquals(target, assertSyntaxCorrect(rule, input).getTarget());
	}

	public void assertEquals(final Rule<TypeTerminal, List<Character>> rule, final String input)
	{
		assertEquals(rule, input, Util.convert(input));
	}

	public void assertUnambiguous(final Rule<TypeTerminal, ?> rule)
	{
		Assertions.assertDoesNotThrow(() -> parser(rule));
	}

	public void assertAmbiguous(final Rule<TypeTerminal, ?> rule)
	{
		Assertions.assertThrows(ExceptionParserAmbiguous.class, () -> parser(rule));
	}
}