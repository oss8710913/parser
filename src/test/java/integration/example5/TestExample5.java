package integration.example5;

import integration.example3.TestExample3;
import nl.novit.parser.common.ParseTree;
import nl.novit.parser.common.Range;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.ParserLALR1;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.StringParser;
import nl.novit.parser.util.StringParserDefault;
import nl.novit.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

abstract class Animal
{
	public final List<Character> name;

	public Animal(final List<Character> name)
	{
		this.name = name;
	}

	public boolean equals(final Animal that)
	{
		return Util.compareIterable(this.name, that.name) == 0;
	}
}

class Spider
	extends Animal
{
	public final Integer eyes;

	public Spider
		(
			final List<Character> name,
			final Integer eyes
		)
	{
		super(name);
		this.eyes = eyes;
	}

	public boolean equals(final Spider that)
	{
		return super.equals(that) && this.eyes.equals(that.eyes);
	}

	@Override
	public boolean equals(final Object that)
	{
		return that instanceof Spider && equals((Spider) that);
	}
}

class Bird
	extends Animal
{
	public final Boolean canFly;
	public final List<Spotting> spottings;

	public Bird
		(
			final List<Character> name,
			final Boolean canFly,
			final List<Spotting> spottings
		)
	{
		super(name);
		this.canFly = canFly;
		this.spottings = spottings;
	}

	public boolean equals(final Bird that)
	{
		return this.canFly.equals(that.canFly) && Util.compareIterable(this.spottings, that.spottings) == 0;
	}

	@Override
	public boolean equals(final Object that)
	{
		return that instanceof Bird && equals((Bird) that);
	}
}

class Spotting
	implements Comparable<Spotting>
{
	public final List<Character> by;
	public final Integer daysAgo;

	public Spotting
		(
			final List<Character> by,
			final Integer daysAgo
		)
	{
		this.by = by;
		this.daysAgo = daysAgo;
	}

	@Override
	public int compareTo(final Spotting that)
	{
		int result = Util.compareIterable(this.by, that.by);
		if (result == 0)
		{
			result = this.daysAgo.compareTo(that.daysAgo);
		}
		return result;
	}

	public boolean equals(final Spotting that)
	{
		return compareTo(that) == 0;
	}

	@Override
	public boolean equals(final Object that)
	{
		return that instanceof Spotting && equals((Spotting) that);
	}
}

public class TestExample5
	extends TestExample3
{
	public final Rule<Character, Character> ruleCharacter;
	public final Rule<Character, List<Character>> ruleName;
	public final Rule<Character, Spotting> ruleSpotting;
	public final Rule<Character, List<Spotting>> ruleSpottingsNo;
	public final Rule<Character, List<Spotting>> ruleSpottingsYes;
	public final Rule<Character, List<Spotting>> ruleSpottings;
	public final Rule<Character, Spider> ruleSpider;
	public final Rule<Character, Bird> ruleBird;
	public final Rule<Character, Animal> ruleAnimal;
	public final Rule<Character, List<Animal>> ruleZoo;

	public TestExample5()
	{
		this.ruleCharacter =
			unionTerminals
				(
					List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z')
				);
		this.ruleName =
			repetition
				(
					this.ruleCharacter
				);
		this.ruleSpotting =
			identity2
				(
					(daysAgo, by) -> new Spotting(by, daysAgo),
					concatenation11
						(
							this.ruleInteger,
							concatenation01
								(
									dummy
										(
											concatenationTerminals
												(
													Util.convert(" days ago by ")
												)
										),
									this.ruleName
								)
						)
				);
		this.ruleSpottingsNo =
			identity1
				(
					(x) -> new ArrayList<>(),
					dummy
						(
							concatenationTerminals
								(
									Util.convert("never spotted")
								)
						)
				);
		this.ruleSpottingsYes =
			concatenation01
				(
					dummy
						(
							concatenationTerminals
								(
									Util.convert("spotted ")
								)
						),
					repetition
						(
							this.ruleSpotting,
							dummy
								(
									concatenationTerminals
										(
											Util.convert(" and ")
										)
								)
						)
				);
		this.ruleSpottings = union(this.ruleSpottingsNo, this.ruleSpottingsYes);
		this.ruleSpider =
			identity2
				(
					Spider::new,
					concatenation11
						(
							this.ruleName,
							concatenation01
								(
									dummy
										(
											concatenationTerminals
												(
													Util.convert(" the spider with ")
												)
										),
									concatenation10
										(
											this.ruleInteger,
											dummy
												(
													concatenationTerminals
														(
															Util.convert(" eyes.")
														)
												)
										)
								)
						)
				);
		this.ruleBird =
			identity3
				(
					Bird::new,
					concatenation111
						(
							concatenation10
								(
									this.ruleName,
									dummy
										(
											concatenationTerminals
												(
													Util.convert(" the bird who can")
												)
										)
								),
							union
								(
									identity1
										(
											x -> true,
											dummy
												(
													concatenationTerminals
														(
															Util.convert(" ")
														)
												)
										),
									identity1
										(
											x -> false,
											dummy(concatenationTerminals
												(
													Util.convert("not "))
											)
										)
								),
							concatenation01
								(
									dummy
										(
											concatenationTerminals
												(
													Util.convert("fly and who was ")
												)
										),
									concatenation10
										(
											this.ruleSpottings,
											dummy(terminal('.'))
										)
								)
						)
				);
		this.ruleAnimal = union
			(
				this.ruleSpider,
				this.ruleBird
			);
		this.ruleZoo =
			concatenation01
				(
					dummy
						(
							concatenationTerminals
								(
									Util.convert("The zoo consists of the following animals. ")
								)
						),
					repetition
						(
							this.ruleAnimal
						)
				);
	}

	@Test
	public void testRuleSpotting()
		throws ExceptionParserInput
	{
		final StringParser<Spotting> parser = new StringParserDefault<>(new ParserLL1<>(this.ruleSpotting));
		Assertions.assertEquals
			(
				new Spotting
					(
						Util.convert("Caroline"),
						16
					),
				parser.parse("16 days ago by Caroline").getTarget()
			);
		Assertions.assertEquals
			(
				new Spotting
					(
						Util.convert("Josh"),
						423
					),
				parser.parse("423 days ago by Josh").getTarget()
			);
	}

	@Test
	public void testRuleSpottings()
		throws ExceptionParserInput
	{
		final StringParser<List<Spotting>> parser = new StringParserDefault<>(new ParserLL1<>(this.ruleSpottings));
		Assertions.assertEquals
			(
				Collections.emptyList(),
				parser.parse("never spotted").getTarget()
			);
		Assertions.assertEquals
			(
				List.of
					(
						new Spotting
							(
								Util.convert("Caroline"),
								16
							),
						new Spotting
							(
								Util.convert("Josh"),
								423
							)
					),
				parser.parse("spotted 16 days ago by Caroline and 423 days ago by Josh").getTarget()
			);
	}

	@Test
	public void testRuleSpider()
		throws ExceptionParserInput
	{
		final StringParser<Spider> parser = new StringParserDefault<>(new ParserLL1<>(this.ruleSpider));
		Assertions.assertEquals
			(
				new Spider
					(
						Util.convert("Creep"),
						18
					),
				parser.parse("Creep the spider with 18 eyes.").getTarget()
			);
		Assertions.assertEquals
			(
				new Spider
					(
						Util.convert("Yikes"),
						24
					),
				parser.parse("Yikes the spider with 24 eyes.").getTarget()
			);
	}

	@Test
	public void testRuleBird()
		throws ExceptionParserInput
	{
		final StringParser<Bird> parser = new StringParserDefault<>(new ParserLL1<>(this.ruleBird));
		Assertions.assertEquals
			(
				new Bird
					(
						Util.convert("Blue"),
						true,
						List.of()
					),
				parser.parse("Blue the bird who can fly and who was never spotted.").getTarget()
			);
		Assertions.assertEquals
			(
				new Bird
					(
						Util.convert("Yellow"),
						false,
						List.of
							(
								new Spotting
									(
										Util.convert("Nancy"),
										5
									),
								new Spotting
									(
										Util.convert("Robert"),
										16
									)
							)
					),
				parser.parse("Yellow the bird who cannot fly and who was spotted 5 days ago by Nancy and 16 days ago by Robert.").getTarget()
			);
	}

	@Test
	public void testRuleZoo()
		throws ExceptionParserInput
	{
		Range range;
		final String input = "The zoo consists of the following animals. " +
			"Creep the spider with 18 eyes." +
			"Blue the bird who can fly and who was never spotted." +
			"Yikes the spider with 24 eyes." +
			"Yellow the bird who cannot fly and who was spotted 5 days ago by Nancy and 16 days ago by Robert.";
		final StringParser<List<Animal>> parser = new StringParserDefault<>(new ParserLALR1<>(this.ruleZoo));
		ParseTree<? extends List<Animal>> animals = parser.parse(input);
		Assertions.assertEquals
			(
				List.of
					(
						new Spider(Util.convert("Creep"), 18),
						new Bird(Util.convert("Blue"), true, List.of()),
						new Spider(Util.convert("Yikes"), 24),
						new Bird(Util.convert("Yellow"), false, List.of(new Spotting(Util.convert("Nancy"), 5), new Spotting(Util.convert("Robert"), 16)))
					),
				animals.getTarget()
			);
		range = animals.find(animals.getTarget()).getRange();
		Assertions.assertEquals
			(
				"Creep the spider with 18 eyes." +
					"Blue the bird who can fly and who was never spotted." +
					"Yikes the spider with 24 eyes." +
					"Yellow the bird who cannot fly and who was spotted 5 days ago by Nancy and 16 days ago by Robert.",
				input.substring(range.start(), range.end())
			);
		range = animals.find(animals.getTarget().get(0)).getRange();
		Assertions.assertEquals
			(
				"Creep the spider with 18 eyes.",
				input.substring(range.start(), range.end())
			);
		range = animals.find(animals.getTarget().get(1)).getRange();
		Assertions.assertEquals
			(
				"Blue the bird who can fly and who was never spotted.",
				input.substring(range.start(), range.end())
			);
		range = animals.find(((Bird) animals.getTarget().get(3)).spottings.get(0)).getRange();
		Assertions.assertEquals
			(
				"5 days ago by Nancy",
				input.substring(range.start(), range.end())
			);
		range = animals.find(((Bird) animals.getTarget().get(3)).spottings.get(1)).getRange();
		Assertions.assertEquals
			(
				"16 days ago by Robert",
				input.substring(range.start(), range.end())
			);
	}
}