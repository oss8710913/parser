package integration.example3;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.Rules;
import nl.novit.parser.util.StringParser;
import nl.novit.parser.util.StringParserDefault;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestExample3
	extends Rules<Character>
{
	public final Rule<Character, Integer> ruleDigit;
	public final Rule<Character, List<Integer>> ruleDigits;
	public final Rule<Character, Integer> ruleInteger;

	public TestExample3()
	{
		this.ruleDigit =
			union
				(
					terminal('0', 0),
					terminal('1', 1),
					terminal('2', 2),
					terminal('3', 3),
					terminal('4', 4),
					terminal('5', 5),
					terminal('6', 6),
					terminal('7', 7),
					terminal('8', 8),
					terminal('9', 9)
				);
		this.ruleDigits = repetition(this.ruleDigit);
		this.ruleInteger =
			identity1
				(
					digits -> digits.stream().reduce(0, (x, y) -> 10 * x + y),
					this.ruleDigits
				);
	}

	@Test
	public void testRuleDigit()
		throws ExceptionParserInput
	{
		final StringParser<Integer> parser = new StringParserDefault<>(new ParserLL1<>(this.ruleDigit));
		Assertions.assertEquals(0, parser.parse("0").getTarget());
		Assertions.assertEquals(1, parser.parse("1").getTarget());
		Assertions.assertEquals(2, parser.parse("2").getTarget());
		Assertions.assertEquals(3, parser.parse("3").getTarget());
		Assertions.assertEquals(4, parser.parse("4").getTarget());
		Assertions.assertEquals(5, parser.parse("5").getTarget());
		Assertions.assertEquals(6, parser.parse("6").getTarget());
		Assertions.assertEquals(7, parser.parse("7").getTarget());
		Assertions.assertEquals(8, parser.parse("8").getTarget());
		Assertions.assertEquals(9, parser.parse("9").getTarget());
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("a"));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("00"));
	}

	@Test
	public void testRuleDigits()
		throws ExceptionParserInput
	{
		final StringParser<List<Integer>> parser = new StringParserDefault<>(new ParserLL1<>(this.ruleDigits));
		Assertions.assertEquals(List.of(0), parser.parse("0").getTarget());
		Assertions.assertEquals(List.of(1), parser.parse("1").getTarget());
		Assertions.assertEquals(List.of(0, 1), parser.parse("01").getTarget());
		Assertions.assertEquals(List.of(3, 7, 4), parser.parse("374").getTarget());
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("55a56"));
	}

	@Test
	public void testRuleInteger()
		throws ExceptionParserInput
	{
		final StringParser<Integer> parser = new StringParserDefault<>(new ParserLL1<>(this.ruleInteger));
		Assertions.assertEquals(0, parser.parse("0").getTarget());
		Assertions.assertEquals(1, parser.parse("1").getTarget());
		Assertions.assertEquals(1, parser.parse("01").getTarget());
		Assertions.assertEquals(374, parser.parse("374").getTarget());
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse(""));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("a"));
		Assertions.assertThrows(ExceptionParserInput.class, () -> parser.parse("1a"));
	}
}