package integration.random;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserUnger;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled
public class TestRandomUnger
	extends TestRandom
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserUnger.FACTORY_PARSER;
	}

	@Test
	@Override
	public void test()
	{
		super.test();
	}

	@Test
	@Override
	public void testRecursive()
	{
		super.test();
	}

	@Test
	@Override
	public void testEmpty()
	{
		super.test();
	}

	@Test
	@Override
	public void testEmptyRecursive()
	{
		super.test();
	}
}