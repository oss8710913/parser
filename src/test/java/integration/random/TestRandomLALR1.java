package integration.random;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLALR1;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled
public class TestRandomLALR1
	extends TestRandom
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLALR1.FACTORY_PARSER;
	}

	@Test
	@Override
	public void test()
	{
		super.test();
	}

	@Test
	@Override
	public void testRecursive()
	{
		super.test();
	}

	@Test
	@Override
	public void testEmpty()
	{
		super.test();
	}

	@Test
	@Override
	public void testEmptyRecursive()
	{
		super.test();
	}
}