package integration.random;

import integration.TestsOneStage;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleDefault;
import nl.novit.parser.util.BuilderRule;
import nl.novit.parser.util.BuilderRuleRandom;
import nl.novit.parser.util.StringParser;
import nl.novit.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public abstract class TestRandom
	extends TestsOneStage
{
	public final int testsBuild = 1000;
	public final int testsParse = 1000;

	public void testParse(final Rule<Character, List<Character>> rule)
		throws ExceptionParserAmbiguous
	{
		final StringParser<List<Character>> parser = parser(rule);
		Assertions.assertDoesNotThrow
			(
				() ->
				{
					for (int testParse = 0; testParse < testsParse; testParse++)
					{
						try
						{
							final String input = Util.convert(rule.random());
							final List<Character> list = parser.parse(input).getTarget();
							Assertions.assertEquals(input, Util.convert(list));
						}
						catch (StackOverflowError exception)
						{
						}
					}
				}
			);
	}

	public void testBuild
		(
			int builderIterations,
			List<Character> builderCharacters,
			boolean builderIncludeEmpty,
			int builderMaximumSizeUnion,
			boolean builderRecursive
		)
	{
		final BuilderRule<Character, List<Character>> builderRule =
			new BuilderRuleRandom
				(
					builderIterations,
					builderCharacters,
					builderIncludeEmpty,
					builderMaximumSizeUnion,
					builderRecursive
				);
		int testBuild = 0;
		while (testBuild < this.testsBuild)
		{
			try
			{
				RuleDefault.nextId = 0;
				Rule<Character, List<Character>> rule = builderRule.rule();
				testParse(rule);
				testBuild++;
			}
			catch (ExceptionParserAmbiguous exceptionParserAmbiguous)
			{
			}
		}
	}

	public void test
		(
			boolean builderIncludeEmpty,
			boolean builderRecursive
		)
	{
		testBuild
			(
				10,
				List.of('a', 'b', 'c'),
				builderIncludeEmpty,
				3,
				builderRecursive
			);
	}

	@Test
	public void test()
	{
		test
			(
				false,
				false
			);
	}

	@Test
	public void testRecursive()
	{
		test
			(
				false,
				true
			);
	}

	@Test
	public void testEmpty()
	{
		test
			(
				true,
				false
			);
	}

	@Test
	public void testEmptyRecursive()
	{
		test
			(
				true,
				true
			);
	}
}