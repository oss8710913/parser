package integration.two_stages;

import integration.Tests;
import integration.two_stages.model.Declaration;
import integration.two_stages.model.Identifier;
import integration.two_stages.model.Method;
import integration.two_stages.model.MethodAbstract;
import integration.two_stages.model.MethodConcrete;
import integration.two_stages.model.Module;
import integration.two_stages.model.TypeAbstract;
import integration.two_stages.model.TypeConcrete;
import nl.novit.parser.common.ParseTree;
import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLALR1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;
import nl.novit.parser.util.StringParserDefault;
import nl.novit.util.Tuple2;
import nl.novit.util.Tuple3;
import nl.novit.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class TestParser<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends Tests<TypeTerminal>
{
	public static final Set<Character> CHARACTERS_IDENTIFIER = StringParserDefault.CHARACTERS;
	public static final List<Character> KEYWORD_ABSTRACT = Util.convert("abstract");
	public static final List<Character> KEYWORD_CLASS = Util.convert("class");
	public static final List<Character> KEYWORD_MODULE = Util.convert("module");
	public static final Set<List<Character>> KEYWORDS = Set.of
		(
			KEYWORD_ABSTRACT,
			KEYWORD_CLASS,
			KEYWORD_MODULE
		);
	public static final Character SYMBOL_BLOCK_START = '{';
	public static final Character SYMBOL_BLOCK_END = '}';
	public static final Character SYMBOL_END = ';';
	public static final Character SYMBOL_PARAMETERS_METHOD_START = '(';
	public static final Character SYMBOL_PARAMETERS_METHOD_END = ')';
	public static final Character SYMBOL_PARAMETERS_SEPARATOR = ',';
	public static final Set<Character> SYMBOLS = Set.of
		(
			SYMBOL_BLOCK_START,
			SYMBOL_BLOCK_END,
			SYMBOL_END,
			SYMBOL_PARAMETERS_METHOD_START,
			SYMBOL_PARAMETERS_METHOD_END,
			SYMBOL_PARAMETERS_SEPARATOR
		);
	public final Rule<TypeTerminal, Void> ruleWhiteSpace;
	public final Rule<TypeTerminal, Void> ruleWSO;
	public final Rule<TypeTerminal, Void> ruleWS;
	public final Rule<TypeTerminal, Void> ruleKeywordAbstractWS;
	public final Rule<TypeTerminal, Void> ruleKeywordClassWS;
	public final Rule<TypeTerminal, Void> ruleKeywordModuleWS;
	public final Rule<TypeTerminal, Void> ruleSymbolBlockStartWSO;
	public final Rule<TypeTerminal, Void> ruleSymbolBlockEndWSO;
	public final Rule<TypeTerminal, Void> ruleSymbolEndWSO;
	public final Rule<TypeTerminal, Void> ruleSymbolParametersStartWSO;
	public final Rule<TypeTerminal, Void> ruleSymbolParametersEndWSO;
	public final Rule<TypeTerminal, Void> ruleSymbolParametersSeparatorWSO;
	public final Rule<TypeTerminal, Identifier> ruleIdentifier;
	public final Rule<TypeTerminal, Identifier> ruleIdentifierWS;
	public final Rule<TypeTerminal, Identifier> ruleIdentifierWSO;
	public final Rule<TypeTerminal, Declaration> ruleDeclarationWSO;
	public final Rule<TypeTerminal, Tuple3<Identifier, Identifier, List<Declaration>>> ruleMethodTempWSO;
	public final Rule<TypeTerminal, Method> ruleMethodWSO;
	public final RuleUnion<TypeTerminal, Tuple2<List<Declaration>, List<Method>>> ruleTypeBodyContentWSO;
	public final Rule<TypeTerminal, Tuple3<Identifier, List<Declaration>, List<Method>>> ruleTypeIdentifierBodyContentWSO;
	public final Rule<TypeTerminal, integration.two_stages.model.Type> ruleTypeWSO;
	public final Rule<TypeTerminal, Module> ruleModuleWSO;

	public abstract Rule<TypeTerminal, Void> ruleWhiteSpace();

	public abstract Rule<TypeTerminal, Void> ruleKeyword(List<Character> keyword);

	public abstract Rule<TypeTerminal, Void> ruleSymbol(Character character);

	public abstract Rule<TypeTerminal, Identifier> ruleIdentifier();

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> ws(Rule<TypeTerminal, TypeTarget> rule)
	{
		return
			concatenation10
				(
					rule,
					this.ruleWS
				);
	}

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> wso(Rule<TypeTerminal, TypeTarget> rule)
	{
		return
			concatenation10
				(
					rule,
					this.ruleWSO
				);
	}

	public Rule<TypeTerminal, Void> ruleSymbolWSO(Character character)
	{
		return wso(ruleSymbol(character));
	}

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> endWSO(Rule<TypeTerminal, TypeTarget> rule)
	{
		return
			concatenation10
				(
					rule,
					this.ruleSymbolEndWSO
				);
	}

	public TestParser()
	{
		this.ruleWhiteSpace = ruleWhiteSpace();
		this.ruleWS =
			dummy(
				repetition
					(
						this.ruleWhiteSpace
					)
			);
		this.ruleWSO = option(this.ruleWS);
		this.ruleKeywordAbstractWS = ws(ruleKeyword(KEYWORD_ABSTRACT));
		this.ruleKeywordClassWS = ws(ruleKeyword(KEYWORD_CLASS));
		this.ruleKeywordModuleWS = ws(ruleKeyword(KEYWORD_MODULE));
		this.ruleSymbolBlockStartWSO = ruleSymbolWSO(SYMBOL_BLOCK_START);
		this.ruleSymbolBlockEndWSO = ruleSymbolWSO(SYMBOL_BLOCK_END);
		this.ruleSymbolEndWSO = ruleSymbolWSO(SYMBOL_END);
		this.ruleSymbolParametersStartWSO = ruleSymbolWSO(SYMBOL_PARAMETERS_METHOD_START);
		this.ruleSymbolParametersEndWSO = ruleSymbolWSO(SYMBOL_PARAMETERS_METHOD_END);
		this.ruleSymbolParametersSeparatorWSO = ruleSymbolWSO(SYMBOL_PARAMETERS_SEPARATOR);
		this.ruleIdentifier = ruleIdentifier();
		this.ruleIdentifierWS = ws(this.ruleIdentifier);
		this.ruleIdentifierWSO = wso(this.ruleIdentifier);
		Rule<TypeTerminal, Tuple2<Identifier, Identifier>> ruleIdentifierIdentifier =
			concatenation11
				(
					this.ruleIdentifierWS,
					this.ruleIdentifierWSO
				);
		this.ruleDeclarationWSO =
			identity2
				(
					Declaration::new,
					ruleIdentifierIdentifier
				);
		this.ruleMethodTempWSO =
			concatenation21
				(
					ruleIdentifierIdentifier,
					concatenation01
						(
							this.ruleSymbolParametersStartWSO,
							concatenation10
								(
									union
										(
											emptyList(),
											repetition
												(
													this.ruleDeclarationWSO,
													this.ruleSymbolParametersSeparatorWSO
												)
										),
									this.ruleSymbolParametersEndWSO
								)
						)
				);
		this.ruleMethodWSO =
			union
				(
					identity3
						(
							MethodAbstract::new,
							concatenation01
								(
									this.ruleKeywordAbstractWS,
									this.ruleMethodTempWSO
								)
						),
					identity3
						(
							MethodConcrete::new,
							concatenation10
								(
									this.ruleMethodTempWSO,
									concatenation00
										(
											this.ruleSymbolBlockStartWSO,
											this.ruleSymbolBlockEndWSO
										)
								)
						)
				);
		this.ruleTypeBodyContentWSO =
			new RuleUnion<>();
		this.ruleTypeBodyContentWSO.add
			(
				identity1
					(
						x -> new Tuple2<>(new ArrayList<>(), new ArrayList<>()),
						empty()
					)
			);
		this.ruleTypeBodyContentWSO.add
			(
				identity2
					(
						(tuple, declaration) ->
						{
							tuple.value0.add(declaration);
							return tuple;
						},
						concatenation11
							(
								this.ruleTypeBodyContentWSO,
								endWSO
									(
										this.ruleDeclarationWSO
									)
							)
					)
			);
		this.ruleTypeBodyContentWSO.add
			(
				identity2
					(
						(tuple, method) ->
						{
							tuple.value1.add(method);
							return tuple;
						},
						concatenation11
							(
								this.ruleTypeBodyContentWSO,
								endWSO
									(
										this.ruleMethodWSO
									)
							)
					)
			);
		this.ruleTypeIdentifierBodyContentWSO =
			concatenation12
				(
					concatenation01
						(
							this.ruleKeywordClassWS,
							this.ruleIdentifierWSO
						),
					concatenation01
						(
							this.ruleSymbolBlockStartWSO,
							concatenation10
								(
									this.ruleTypeBodyContentWSO,
									this.ruleSymbolBlockEndWSO
								)
						)
				);
		this.ruleTypeWSO =
			union
				(
					identity3
						(
							TypeAbstract::new,
							concatenation01
								(
									this.ruleKeywordAbstractWS,
									this.ruleTypeIdentifierBodyContentWSO
								)
						),
					identity3
						(
							TypeConcrete::new,
							this.ruleTypeIdentifierBodyContentWSO
						)
				);
		this.ruleModuleWSO =
			identity2
				(
					Module::new,
					concatenation11
						(
							endWSO
								(
									concatenation01
										(
											this.ruleKeywordModuleWS,
											this.ruleIdentifierWSO
										)
								),
							repetition
								(
									this.ruleTypeWSO
								)
						)
				);
	}

	@Override
	public FactoryParser factory()
	{
		return FactoryParserLALR1.FACTORY_PARSER;
	}

	@Test
	public void testKeywordModuleWS()
	{
		assertSyntaxCorrect(this.ruleKeywordModuleWS, "module ");
		assertSyntaxIncorrect(this.ruleKeywordModuleWS, "module", 6);
		assertSyntaxIncorrect(this.ruleKeywordModuleWS, "module  ;;", 8);
	}

	@Test
	public void testRuleIdentifier()
	{
		assertSyntaxCorrect(this.ruleIdentifier, "a");
		assertSyntaxIncorrect(this.ruleIdentifier, "", 0);
		assertSyntaxIncorrect(this.ruleIdentifier, " ", 0);
	}

	@Test
	public void testRuleIdentifierWS()
	{
		assertSyntaxCorrect(this.ruleIdentifierWS, "a ");
		assertSyntaxCorrect(this.ruleIdentifierWS, "ab ");
		assertSyntaxCorrect(this.ruleIdentifierWS, "ab  ");
		assertSyntaxIncorrect(this.ruleIdentifierWS, "", 0);
		assertSyntaxIncorrect(this.ruleIdentifierWS, " ", 0);
		assertSyntaxIncorrect(this.ruleIdentifierWS, "a", 1);
		assertSyntaxIncorrect(this.ruleIdentifierWS, "ab", 2);
	}

	@Test
	public void testRuleIdentifierWSO()
	{
		assertSyntaxCorrect(this.ruleIdentifierWSO, "a");
		assertSyntaxCorrect(this.ruleIdentifierWSO, "ab");
		assertSyntaxCorrect(this.ruleIdentifierWSO, "a ");
		assertSyntaxCorrect(this.ruleIdentifierWSO, "ab ");
		assertSyntaxCorrect(this.ruleIdentifierWSO, "ab  ");
		assertSyntaxIncorrect(this.ruleIdentifierWSO, "", 0);
		assertSyntaxIncorrect(this.ruleIdentifierWSO, " ", 0);
		assertSyntaxIncorrect(this.ruleIdentifierWSO, " a", 0);
	}

	@Test
	public void testRuleDeclarationWSO()
	{
		assertSyntaxCorrect(this.ruleDeclarationWSO, "A a");
		assertSyntaxCorrect(this.ruleDeclarationWSO, "Aad   aef ");
		assertSyntaxIncorrect(this.ruleDeclarationWSO, " A a", 0);
		assertSyntaxIncorrect(this.ruleDeclarationWSO, "A ", 2);
	}

	@Test
	public void testRuleMethodTempWSO()
	{
		assertSyntaxCorrect(this.ruleMethodTempWSO, "A foo(X x, Y y)");
	}

	@Test
	public void testRuleMethodWSO()
	{
		assertSyntaxCorrect
			(
				this.ruleMethodWSO, "A foo()" +
					"{" +
					"}"
			);
		assertSyntaxCorrect
			(
				this.ruleMethodWSO, "A foo(X x, Y y)" +
					"{" +
					"}"
			);
		assertSyntaxCorrect
			(
				this.ruleMethodWSO, "abstract A foo(X x, Y y)"
			);
		assertSyntaxIncorrect
			(
				this.ruleMethodWSO, "A    foo(X x, ) {    }        ",
				14
			);
		assertSyntaxIncorrect
			(
				this.ruleMethodWSO, "abstract  A    foo(X x, Y y    ) {    }        ",
				33
			);
		assertSyntaxIncorrect
			(
				this.ruleMethodWSO, "abstract  A    foo(X x, Y y  ,  )        ",
				32
			);
		assertSyntaxIncorrect
			(
				this.ruleMethodWSO, "abstract  A    foo(X x, Y y  ,   B   )        ",
				37
			);
		assertSyntaxIncorrect
			(
				this.ruleMethodWSO, "A   foo       (X x, Y y)  ",
				26
			);
	}

	@Test
	public void testRuleTypeBodyContentWSO()
	{
		assertSyntaxCorrect
			(
				this.ruleTypeBodyContentWSO,
				""
			);
		assertSyntaxCorrect
			(
				this.ruleTypeBodyContentWSO,
				"Afse aefb;"
			);
		assertSyntaxCorrect
			(
				this.ruleTypeBodyContentWSO,
				"Afse aefb;" +
					"    B b;"
			);
	}

	@Test
	public void testRuleTypeIdentifierBodyContentWSO()
	{
		assertSyntaxCorrect
			(
				this.ruleTypeIdentifierBodyContentWSO,
				"class TestClass{}"
			);
	}

	@Test
	public void testTypeWSO()
	{
		assertSyntaxCorrect
			(
				this.ruleTypeWSO,
				"class TestClass" +
					"{" +
					"}"
			);
		assertSyntaxCorrect
			(
				this.ruleTypeWSO,
				"class TestClass" +
					"{" +
					"    Afse aefb;" +
					"}"
			);
		assertSyntaxCorrect
			(
				this.ruleTypeWSO,
				"class TestClass" +
					"{" +
					"    Afse aefb;" +
					"    B b;" +
					"}"
			);
	}

	@Test
	public void testModule()
	{
		String input = "module        test;" +
			"class   TestClassA       " +
			"{" +
			"    A     a;" +
			"    B  b ;    " +
			"}" +
			"class TestClassB" +
			"{" +
			"    Xasd xeee;" +
			"    A foo(T t,     K k)  " +
			"    {         " +
			"    };" +
			"    Es bar(T t,  U    ii  ,  Kff  ksddsgf  )" +
			"    {         " +
			"    };" +
			"}";
		ParseTree<? extends Module> parseTree = assertSyntaxCorrect(this.ruleModuleWSO, input);
		Module module = parseTree.getTarget();
		Assertions.assertEquals
			(
				"TestClassA",
				substring(input, parseTree.find(module.types.get(0).identifier).getRange())
			);
		Assertions.assertEquals
			(
				"TestClassB",
				substring(input, parseTree.find(module.types.get(1).identifier).getRange())
			);
		Assertions.assertEquals
			(
				"ksddsgf",
				substring(input, parseTree.find(module.types.get(1).methods.get(1).parametersFormal.get(2).variable).getRange())
			);
		Assertions.assertEquals
			(
				"A foo(T t,     K k)  " +
					"    {         " +
					"    }",
				substring(input, parseTree.find(module.types.get(1).methods.get(0)).getRange())
			);
	}
}


