package integration.two_stages.model;

public class Declaration
{
	public final Identifier type;
	public final Identifier variable;

	public Declaration(Identifier type, Identifier variable)
	{
		this.type = type;
		this.variable = variable;
	}
}