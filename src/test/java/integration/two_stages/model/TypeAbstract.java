package integration.two_stages.model;

import java.util.List;

public class TypeAbstract
	extends Type
{
	public TypeAbstract(Identifier identifier, List<Declaration> variables, List<Method> methods)
	{
		super(identifier, variables, methods);
	}
}