package integration.two_stages.model;

import java.util.List;

public class Type
{
	public final Identifier identifier;
	public final List<Declaration> variables;
	public final List<Method> methods;

	public Type(Identifier identifier, List<Declaration> variables, List<Method> methods)
	{
		this.identifier = identifier;
		this.variables = variables;
		this.methods = methods;
	}
}