package integration.two_stages.model;

import java.util.List;

public class Module
{
	public final Identifier identifier;
	public final List<Type> types;

	public Module(Identifier identifier, List<Type> types)
	{
		this.identifier = identifier;
		this.types = types;
	}
}