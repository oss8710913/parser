package integration.two_stages.model;

import java.util.List;

public class Method
{
	public final Identifier typeReturn;
	public final Identifier identifier;
	public final List<Declaration> parametersFormal;

	public Method(Identifier typeReturn, Identifier identifier, List<Declaration> parametersFormal)
	{
		this.typeReturn = typeReturn;
		this.identifier = identifier;
		this.parametersFormal = parametersFormal;
	}
}