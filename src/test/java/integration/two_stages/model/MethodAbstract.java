package integration.two_stages.model;

import java.util.List;

public class MethodAbstract
	extends Method
{
	public MethodAbstract(Identifier identifier, Identifier typeReturn, List<Declaration> parametersFormal)
	{
		super(identifier, typeReturn, parametersFormal);
	}
}