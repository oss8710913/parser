package integration.two_stages.model;

import nl.novit.util.Util;

import java.util.List;

public class Identifier
	implements Comparable<Identifier>
{
	public final List<Character> name;

	public Identifier(List<Character> name)
	{
		this.name = name;
	}

	@Override
	public int compareTo(Identifier that)
	{
		return Util.compareIterable(this.name, that.name);
	}
}