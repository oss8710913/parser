package integration.two_stages.model;

import java.util.List;

public class TypeConcrete
	extends Type
{
	public TypeConcrete(Identifier identifier, List<Declaration> variables, List<Method> methods)
	{
		super(identifier, variables, methods);
	}
}