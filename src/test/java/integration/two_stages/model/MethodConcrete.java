package integration.two_stages.model;

import java.util.List;

public class MethodConcrete
	extends Method
{
	public MethodConcrete(Identifier identifier, Identifier typeReturn, List<Declaration> parametersFormal)
	{
		super(identifier, typeReturn, parametersFormal);
	}
}