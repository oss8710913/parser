package integration.two_stages;

import integration.two_stages.model.Identifier;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.StringParserDefault;
import nl.novit.util.Util;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class TestParserOneStage
	extends TestParser<Character>
{
	@Override
	public <TypeTarget> StringParserDefault<TypeTarget> parser(Rule<Character, TypeTarget> rule)
		throws ExceptionParserAmbiguous
	{
		return new StringParserDefault<>(factory().parser(rule));
	}

	@Override
	public Rule<Character, Void> ruleWhiteSpace()
	{
		return
			dummy
				(
					unionTerminals
						(
							StringParserDefault.WHITESPACES
						)
				);
	}

	@Override
	public Rule<Character, Void> ruleKeyword(List<Character> keyword)
	{
		return dummy
			(
				concatenationTerminals
					(
						keyword
					)
			);
	}

	@Override
	public Rule<Character, Void> ruleSymbol(Character character)
	{
		return dummy(terminal(character));
	}

	@Override
	public Rule<Character, Identifier> ruleIdentifier()
	{
		List<List<Character>> keywords = new ArrayList<>(KEYWORDS);
		keywords.add(Util.convert(""));
		return identity1(Identifier::new, notKeyword(CHARACTERS_IDENTIFIER, keywords));
	}

	@Test
	public void testRuleIdentifierWSOIncorrect()
	{
		assertSyntaxIncorrect(this.ruleIdentifierWSO, "class", 5);
	}
}