package integration.two_stages;

import integration.two_stages.model.Identifier;
import integration.two_stages.token.Token;
import integration.two_stages.token.TokenCharacter;
import integration.two_stages.token.TokenIdentifier;
import integration.two_stages.token.TokenKeyword;
import integration.two_stages.token.TokenSymbol;
import integration.two_stages.token.TokenWhitespace;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.BuilderRule;
import nl.novit.parser.util.ParserTwoStages;
import nl.novit.parser.util.StringParserDefault;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

public class TestParserTwoStages
	extends TestParser<Token>
{
	public final Rule<Character, List<Token>> ruleToken;

	public TestParserTwoStages()
	{
		this.ruleToken =
			new BuilderRule<Character, List<Token>>()
			{
				@Override
				public Rule<Character, List<Token>> rule()
				{
					final Rule<Character, List<Token>> ruleIdentifierOrKeyword =
						keywordOrNot
							(
								CHARACTERS_IDENTIFIER,
								KEYWORDS,
								TokenCharacter::new,
								TokenKeyword.TOKEN_KEYWORD,
								TokenIdentifier.TOKEN_IDENTIFIER
							);
					final Rule<Character, List<Token>> ruleSymbolsOrWhitespaces =
						repetition
							(
								union
									(
										identity1
											(
												TokenSymbol::new,
												unionTerminals(SYMBOLS)
											),
										identity1
											(
												character -> TokenWhitespace.TOKEN_WHITESPACE,
												unionTerminals(StringParserDefault.WHITESPACES)
											)
									)
							);
					return
						flatten
							(
								alternating
									(
										ruleIdentifierOrKeyword,
										ruleSymbolsOrWhitespaces
									)
							);
				}
			}.rule();
	}

	@Override
	public <TypeTarget> StringParserDefault<TypeTarget> parser(Rule<Token, TypeTarget> rule)
		throws ExceptionParserAmbiguous
	{
		return
			new StringParserDefault<>
				(
					new ParserTwoStages<>
						(
							new ParserLL1<>(this.ruleToken),
							factory().parser(rule)
						)
				);
	}

	@Override
	public Rule<Token, Void> ruleWhiteSpace()
	{
		return dummy(terminal(TokenWhitespace.TOKEN_WHITESPACE));
	}

	@Override
	public Rule<Token, Void> ruleKeyword(List<Character> keyword)
	{
		return
			dummy
				(
					concatenation11
						(
							terminal
								(
									TokenKeyword.TOKEN_KEYWORD
								),
							concatenationTerminals(keyword.stream().map(TokenCharacter::new).collect(Collectors.toList()))
						)
				);
	}

	@Override
	public Rule<Token, Void> ruleSymbol(Character character)
	{
		return dummy(terminal(new TokenSymbol(character)));
	}

	@Override
	public Rule<Token, Identifier> ruleIdentifier()
	{
		return identity1
			(
				Identifier::new,
				concatenation01
					(
						dummy
							(
								terminal
									(
										TokenIdentifier.TOKEN_IDENTIFIER
									)
							),
						repetition
							(
								union
									(
										StringParserDefault.CHARACTERS
											.stream()
											.map
												(
													character -> new TokenCharacter(character).rule()
												)
											.collect(Collectors.toList())
									)
							)
					)
			);
	}

	@Test
	public void testRuleIdentifierWSOIncorrect()
	{
		assertSyntaxIncorrect(this.ruleIdentifierWSO, "class", 0);
	}
}