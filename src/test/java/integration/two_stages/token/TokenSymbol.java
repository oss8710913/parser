package integration.two_stages.token;

public class TokenSymbol
	implements Token
{
	public final char symbol;

	public TokenSymbol(char symbol)
	{
		this.symbol = symbol;
	}

	@Override
	public int compareTo(TokenCharacter that)
	{
		return 1;
	}

	@Override
	public int compareTo(TokenIdentifier that)
	{
		return 1;
	}

	@Override
	public int compareTo(TokenKeyword that)
	{
		return 1;
	}

	@Override
	public int compareTo(TokenSymbol that)
	{
		return Character.compare(this.symbol, that.symbol);
	}

	@Override
	public int compareTo(TokenWhitespace that)
	{
		return -1;
	}

	@Override
	public int compareTo(Token that)
	{
		return -that.compareTo(this);
	}

	@Override
	public String toString()
	{
		return String.valueOf(this.symbol);
	}
}
