package integration.two_stages.token;

import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleTerminal;

public class TokenCharacter
	implements Token
{
	public final char character;

	public TokenCharacter(char character)
	{
		this.character = character;
	}

	@Override
	public int compareTo(TokenCharacter that)
	{
		return Character.compare(this.character, that.character);
	}

	@Override
	public int compareTo(TokenIdentifier that)
	{
		return -1;
	}

	@Override
	public int compareTo(TokenKeyword that)
	{
		return -1;
	}

	@Override
	public int compareTo(TokenSymbol that)
	{
		return -1;
	}

	@Override
	public int compareTo(TokenWhitespace that)
	{
		return -1;
	}

	@Override
	public int compareTo(Token that)
	{
		return -that.compareTo(this);
	}

	@Override
	public String toString()
	{
		return String.valueOf(this.character);
	}

	public Rule<Token, Character> rule()
	{
		return new RuleTerminal<>(this)
		{
			@Override
			public Character getTarget()
			{
				return TokenCharacter.this.character;
			}
		};
	}
}