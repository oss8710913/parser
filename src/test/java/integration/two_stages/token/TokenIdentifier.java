package integration.two_stages.token;

public class TokenIdentifier
	implements Token
{
	public static final TokenIdentifier TOKEN_IDENTIFIER = new TokenIdentifier();

	private TokenIdentifier()
	{
	}

	@Override
	public int compareTo(TokenCharacter that)
	{
		return 1;
	}

	@Override
	public int compareTo(TokenIdentifier that)
	{
		return 0;
	}

	@Override
	public int compareTo(TokenKeyword that)
	{
		return -1;
	}

	@Override
	public int compareTo(TokenSymbol that)
	{
		return -1;
	}

	@Override
	public int compareTo(TokenWhitespace that)
	{
		return -1;
	}

	@Override
	public int compareTo(Token that)
	{
		return -that.compareTo(this);
	}

	@Override
	public String toString()
	{
		return "!~";
	}
}