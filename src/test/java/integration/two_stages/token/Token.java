package integration.two_stages.token;

public interface Token
	extends Comparable<Token>
{
	int compareTo(TokenCharacter that);

	int compareTo(TokenIdentifier that);

	int compareTo(TokenKeyword that);

	int compareTo(TokenSymbol that);

	int compareTo(TokenWhitespace that);
}