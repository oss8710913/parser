package integration.rules;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLL1;
import org.junit.jupiter.api.Test;

public class TestRulesLL1
	extends TestRules
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLL1.FACTORY_PARSER;
	}

	@Test
	@Override
	public void testruleRepetitionA()
	{
		assertAmbiguous(this.ruleRepetition_A);
	}

	@Test
	@Override
	public void testRuleRepetitionLR_A()
	{
		assertAmbiguous(this.ruleRepetitionLR_A);
	}
}