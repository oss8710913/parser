package integration.rules;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLALR1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestRulesLALR1
	extends TestRules
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLALR1.FACTORY_PARSER;
	}

	@Test
	@Override
	public void testruleRepetitionA()
	{
		assertAmbiguous(this.ruleRepetition_A);
	}

	@Test
	@Override
	public void testRuleRepetitionLR_A()
	{
		Assertions.assertDoesNotThrow(() -> parser(this.ruleRepetitionLR_A));
	}
}