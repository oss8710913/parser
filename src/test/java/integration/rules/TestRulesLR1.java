package integration.rules;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserLR1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestRulesLR1
	extends TestRules
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserLR1.FACTORY_PARSER;
	}

	@Test
	@Override
	public void testruleRepetitionA()
	{
		assertAmbiguous(this.ruleRepetition_A);
	}

	@Test
	@Override
	public void testRuleRepetitionLR_A()
	{
		Assertions.assertDoesNotThrow(() -> parser(this.ruleRepetitionLR_A));
	}
}