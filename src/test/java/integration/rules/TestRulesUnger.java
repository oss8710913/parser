package integration.rules;

import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.factory.FactoryParserUnger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestRulesUnger
	extends TestRules
{
	@Override
	public FactoryParser factory()
	{
		return FactoryParserUnger.FACTORY_PARSER;
	}

	@Test
	@Override
	public void testruleRepetitionA()
	{
		Assertions.assertDoesNotThrow(() -> parser(this.ruleRepetition_A));
	}

	@Test
	@Override
	public void testRuleRepetitionLR_A()
	{
		Assertions.assertDoesNotThrow(() -> parser(this.ruleRepetitionLR_A));
	}
}