package integration.rules;

import integration.TestsOneStage;
import nl.novit.parser.rule.Rule;
import nl.novit.util.Tuple2;
import nl.novit.util.Tuple3;
import nl.novit.util.Tuple4;
import nl.novit.util.Tuple5;
import nl.novit.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class TestRules
	extends TestsOneStage
{
	@Test
	public void testEmpty()
	{
		assertNull(this.empty, "");
		assertSyntaxIncorrect(this.empty, "a", 0);
		Assertions.assertEquals(this.empty.getId(), empty().getId());
	}

	@Test
	public void testEmptyTrue()
	{
		assertTrue(this.emptyTrue, "");
		assertSyntaxIncorrect(this.emptyTrue, "a", 0);
	}

	@Test
	public void testEmptyFalse()
	{
		assertFalse(this.emptyFalse, "");
		assertSyntaxIncorrect(this.emptyFalse, "a", 0);
	}

	public final Rule<Character, Integer> ruleEmptyInteger = empty();
	public final Rule<Character, Integer> ruleEmptyCharacter = empty();

	@Test
	public void testRuleEmptyInteger()
	{
		assertNull(this.ruleEmptyInteger, "");
		assertSyntaxIncorrect(this.ruleEmptyInteger, "a", 0);
		assertNull(this.ruleEmptyCharacter, "");
		assertSyntaxIncorrect(this.ruleEmptyCharacter, "a", 0);
	}

	public final Rule<Character, List<Integer>> ruleEmptyList = emptyList();

	@Test
	public void testRuleEmptyList()
	{
		assertEquals(this.ruleEmptyList, "", List.of());
		assertSyntaxIncorrect(this.ruleEmptyList, "a", 0);
	}

	public final Rule<Character, Character> ruleTerminalA = terminal('a');
	public final Rule<Character, Character> ruleTerminalB = terminal('b');
	public final Rule<Character, Character> ruleTerminalC = terminal('c');
	public final Rule<Character, Character> ruleTerminalD = terminal('d');
	public final Rule<Character, Character> ruleTerminalE = terminal('e');

	@Test
	public void testRuleTerminalA()
	{
		assertEquals(this.ruleTerminalA, "a", 'a');
		assertSyntaxIncorrect(this.ruleTerminalA, "", 0);
		assertSyntaxIncorrect(this.ruleTerminalA, "b", 0);
		assertSyntaxIncorrect(this.ruleTerminalA, "aa", 1);
		Assertions.assertEquals(this.ruleTerminalA.getId(), terminal('a').getId());
	}

	public final Rule<Character, Integer> ruleTerminalInteger = terminal('1', 1);
	public final Rule<Character, Boolean> ruleTerminalBoolean = terminal('T', true);

	@Test
	public void testRuleTerminalTarget()
	{
		assertEquals(this.ruleTerminalInteger, "1", 1);
		assertEquals(this.ruleTerminalBoolean, "T", true);
		assertSyntaxIncorrect(this.ruleTerminalInteger, "", 0);
		assertSyntaxIncorrect(this.ruleTerminalInteger, "2", 0);
		Assertions.assertEquals(this.ruleTerminalInteger.getId(), terminal('1', 1).getId());
	}

	public final Rule<Character, List<Character>> ruleTerminalsEmpty = concatenationTerminals(Util.convert(""));

	@Test
	public void testRuleTerminalsEmpty()
	{
		assertEquals(this.ruleTerminalsEmpty, "");
		assertSyntaxIncorrect(this.ruleTerminalsEmpty, "a", 0);
	}

	public final Rule<Character, Void> ruleDummyTerminalA = dummy(ruleTerminalA);

	@Test
	public void testRuleDummyTerminalA()
	{
		assertNull(this.ruleDummyTerminalA, "a");
		assertSyntaxIncorrect(this.ruleDummyTerminalA, "", 0);
		assertSyntaxIncorrect(this.ruleDummyTerminalA, "b", 0);
		assertSyntaxIncorrect(this.ruleDummyTerminalA, "aa", 1);
	}

	public final Rule<Character, Character> ruleAs = as(this.ruleDummyTerminalA);

	@Test
	public void testRuleAs()
	{
		assertNull(ruleAs, "a");
	}

	public <TypeTarget> void testRuleConcatenationXX(Rule<Character, TypeTarget> rule)
	{
		assertSyntaxIncorrect(rule, "", 0);
		assertSyntaxIncorrect(rule, "a", 1);
		assertSyntaxIncorrect(rule, "b", 0);
		assertSyntaxIncorrect(rule, "aba", 2);
	}

	public final Rule<Character, Void> ruleConcatenation00 = concatenation00(dummy(ruleTerminalA), dummy(ruleTerminalB));

	@Test
	public void testRuleConcatenation00()
	{
		assertNull(this.ruleConcatenation00, "ab");
		testRuleConcatenationXX(this.ruleConcatenation00);
	}

	public final Rule<Character, Character> ruleConcatenation01 = concatenation01(dummy(ruleTerminalA), ruleTerminalB);

	@Test
	public void testRuleConcatenation01()
	{
		assertEquals(this.ruleConcatenation01, "ab", 'b');
		testRuleConcatenationXX(this.ruleConcatenation01);
	}

	public final Rule<Character, Character> ruleConcatenation10 = concatenation10(ruleTerminalA, dummy(ruleTerminalB));

	@Test
	public void testRuleConcatenation10()
	{
		assertEquals(this.ruleConcatenation10, "ab", 'a');
		testRuleConcatenationXX(this.ruleConcatenation10);
	}

	public final Rule<Character, Tuple2<Character, Character>> ruleConcatenation11 = concatenation11(ruleTerminalA, ruleTerminalB);

	@Test
	public void testRuleConcatenation11()
	{
		assertEquals(this.ruleConcatenation11, "ab", new Tuple2<>('a', 'b'));
		testRuleConcatenationXX(this.ruleConcatenation11);
	}

	public final Rule<Character, Tuple3<Character, Character, Character>> ruleConcatenation111 = concatenation111(ruleTerminalA, ruleTerminalB, ruleTerminalC);

	@Test
	public void testRuleConcatenation111()
	{
		assertEquals(this.ruleConcatenation111, "abc", new Tuple3<>('a', 'b', 'c'));
		assertSyntaxIncorrect(this.ruleConcatenation111, "ab", 2);
	}

	public final Rule<Character, Tuple4<Character, Character, Character, Character>> ruleConcatenation1111 = concatenation1111(ruleTerminalA, ruleTerminalB, ruleTerminalC, ruleTerminalD);

	@Test
	public void testRuleConcatenation1111()
	{
		assertEquals(this.ruleConcatenation1111, "abcd", new Tuple4<>('a', 'b', 'c', 'd'));
		assertSyntaxIncorrect(this.ruleConcatenation1111, "abc", 3);
	}

	public final Rule<Character, Tuple5<Character, Character, Character, Character, Character>> ruleConcatenation11111 = concatenation11111(ruleTerminalA, ruleTerminalB, ruleTerminalC, ruleTerminalD, ruleTerminalE);

	@Test
	public void testRuleConcatenation11111()
	{
		assertEquals(this.ruleConcatenation11111, "abcde", new Tuple5<>('a', 'b', 'c', 'd', 'e'));
		assertSyntaxIncorrect(this.ruleConcatenation11111, "abcd", 4);
	}

	public final Rule<Character, List<Character>> ruleConcatenationTerminalsHi = concatenationTerminals(Util.convert("hi"));

	@Test
	public void testRuleConcatenationTerminalsHi()
	{
		assertEquals(this.ruleConcatenationTerminalsHi, "hi");
		assertSyntaxIncorrect(this.ruleConcatenationTerminalsHi, "", 0);
		assertSyntaxIncorrect(this.ruleConcatenationTerminalsHi, "h", 1);
		assertSyntaxIncorrect(this.ruleConcatenationTerminalsHi, "i", 0);
		assertSyntaxIncorrect(this.ruleConcatenationTerminalsHi, "hi!", 2);
	}

	public final Rule<Character, List<Character>> ruleConcatenationTerminalsAB = concatenationTerminals(Util.convert("AB"));
	public final Rule<Character, List<Character>> ruleConcatenationListList = concatenationListList(ruleConcatenationTerminalsHi, ruleConcatenationTerminalsAB);

	@Test
	public void testRuleConcatenationListList()
	{
		assertEquals(this.ruleConcatenationListList, "hiAB");
		assertSyntaxIncorrect(this.ruleConcatenationListList, "", 0);
		assertSyntaxIncorrect(this.ruleConcatenationListList, "h", 1);
		assertSyntaxIncorrect(this.ruleConcatenationListList, "hi", 2);
		assertSyntaxIncorrect(this.ruleConcatenationListList, "hiA", 3);
		assertSyntaxIncorrect(this.ruleConcatenationListList, "hiABB", 4);
		assertSyntaxIncorrect(this.ruleConcatenationListList, "hiABhi", 4);
		assertSyntaxIncorrect(this.ruleConcatenationListList, "hihiAB", 2);
		assertSyntaxIncorrect(this.ruleConcatenationListList, "hiABAN", 4);
	}

	public final Rule<Character, List<Character>> ruleConcatenationElementElement = concatenationElementElement(ruleTerminalA, ruleTerminalB);

	@Test
	public void testRuleConcatenationElementElement()
	{
		assertEquals(this.ruleConcatenationElementElement, "ab", Arrays.asList('a', 'b'));
		testRuleConcatenationXX(this.ruleConcatenationElementElement);
	}

	public final Rule<Character, List<Character>> ruleConcatenationElementList = concatenationElementList(ruleTerminalA, ruleConcatenationTerminalsHi);

	@Test
	public void testRuleConcatenationElementList()
	{
		assertEquals(this.ruleConcatenationElementList, "ahi");
		assertSyntaxIncorrect(this.ruleConcatenationElementList, "", 0);
		assertSyntaxIncorrect(this.ruleConcatenationElementList, "a", 1);
		assertSyntaxIncorrect(this.ruleConcatenationElementList, "ah", 2);
		assertSyntaxIncorrect(this.ruleConcatenationElementList, "hi", 0);
		assertSyntaxIncorrect(this.ruleConcatenationElementList, "hia", 0);
	}

	public final Rule<Character, List<Character>> ruleConcatenationListElement = concatenationListElement(ruleConcatenationTerminalsHi, ruleTerminalA);

	@Test
	public void testRuleConcatenationListElement()
	{
		assertEquals(this.ruleConcatenationListElement, "hia");
		assertSyntaxIncorrect(this.ruleConcatenationListElement, "", 0);
		assertSyntaxIncorrect(this.ruleConcatenationListElement, "h", 1);
		assertSyntaxIncorrect(this.ruleConcatenationListElement, "hi", 2);
		assertSyntaxIncorrect(this.ruleConcatenationListElement, "ia", 0);
		assertSyntaxIncorrect(this.ruleConcatenationListElement, "ahi", 0);
	}

	public final Rule<Character, Character> ruleUnionA = union(ruleTerminalA);

	@Test
	public void testRuleUnionA()
	{
		assertEquals(this.ruleUnionA, "a", 'a');
		assertSyntaxIncorrect(this.ruleUnionA, "", 0);
		assertSyntaxIncorrect(this.ruleUnionA, "b", 0);
	}

	public final Rule<Character, Character> ruleUnionAB = union(ruleTerminalA, ruleTerminalB);

	@Test
	public void testRuleUnionAB()
	{
		assertEquals(this.ruleUnionAB, "a", 'a');
		assertEquals(this.ruleUnionAB, "b", 'b');
		assertSyntaxIncorrect(this.ruleUnionAB, "", 0);
		assertSyntaxIncorrect(this.ruleUnionAB, "c", 0);
	}

	public final Rule<Character, Character> ruleUnionABC = union(ruleTerminalA, ruleTerminalB, ruleTerminalC);

	@Test
	public void testRuleUnionAbc()
	{
		assertEquals(this.ruleUnionABC, "a", 'a');
		assertEquals(this.ruleUnionABC, "b", 'b');
		assertEquals(this.ruleUnionABC, "c", 'c');
		assertSyntaxIncorrect(this.ruleUnionABC, "", 0);
		assertSyntaxIncorrect(this.ruleUnionABC, "d", 0);
	}

	public final Rule<Character, Character> ruleUnionABCD = union(ruleTerminalA, ruleTerminalB, ruleTerminalC, ruleTerminalD);

	@Test
	public void testRuleUnionABCD()
	{
		assertEquals(this.ruleUnionABCD, "a", 'a');
		assertEquals(this.ruleUnionABCD, "b", 'b');
		assertEquals(this.ruleUnionABCD, "c", 'c');
		assertEquals(this.ruleUnionABCD, "d", 'd');
		assertSyntaxIncorrect(this.ruleUnionABCD, "", 0);
		assertSyntaxIncorrect(this.ruleUnionABCD, "e", 0);
	}

	public void testUnion(Rule<Character, Character> rule)
	{
		assertEquals(rule, "a", 'a');
		assertEquals(rule, "b", 'b');
		assertEquals(rule, "c", 'c');
		assertEquals(rule, "d", 'd');
		assertEquals(rule, "e", 'e');
		assertSyntaxIncorrect(rule, "", 0);
		assertSyntaxIncorrect(rule, "abcde", 1);
	}

	public final Rule<Character, Character> ruleUnionABCDE = union(ruleTerminalA, ruleTerminalB, ruleTerminalC, ruleTerminalD, ruleTerminalE);

	@Test
	public void testRuleUnionABCDE()
	{
		testUnion(this.ruleUnionABCDE);
	}

	public final Rule<Character, Character> ruleUnionSetABCDE = union(Set.of(ruleTerminalA, ruleTerminalB, ruleTerminalC, ruleTerminalD, ruleTerminalE));

	@Test
	public void testruleUnionSetABCDE()
	{
		testUnion(this.ruleUnionSetABCDE);
	}

	public final Rule<Character, Character> ruleUnionTerminalsABCDE = unionTerminals(Set.of('a', 'b', 'c', 'd', 'e'));

	@Test
	public void testRuleUnionTerminalsABCDE()
	{
		assertEquals(this.ruleUnionTerminalsABCDE, "a", 'a');
		assertEquals(this.ruleUnionTerminalsABCDE, "b", 'b');
		assertEquals(this.ruleUnionTerminalsABCDE, "c", 'c');
		assertEquals(this.ruleUnionTerminalsABCDE, "d", 'd');
		assertEquals(this.ruleUnionTerminalsABCDE, "e", 'e');
		assertSyntaxIncorrect(this.ruleUnionTerminalsABCDE, "", 0);
		assertSyntaxIncorrect(this.ruleUnionTerminalsABCDE, "abcde", 1);
	}

	public final Rule<Character, String> ruleIdentity1 = identity1(t0 -> "" + t0, ruleTerminalA);

	@Test
	public void testRuleIdentity1()
	{
		assertEquals(this.ruleIdentity1, "a", "a");
	}

	public final Rule<Character, String> ruleIdentity2 = identity2((t0, t1) -> "" + t0 + t1, ruleConcatenation11);

	@Test
	public void testRuleIdentity2()
	{
		assertEquals(this.ruleIdentity2, "ab", "ab");
	}

	public final Rule<Character, String> ruleIdentity3 = identity3((t0, t1, t2) -> "" + t0 + t1 + t2, ruleConcatenation111);

	@Test
	public void testRuleIdentity3()
	{
		assertEquals(this.ruleIdentity3, "abc", "abc");
	}

	public final Rule<Character, String> ruleIdentity4 = identity4((t0, t1, t2, t3) -> "" + t0 + t1 + t2 + t3, ruleConcatenation1111);

	@Test
	public void testRuleIdentity4()
	{
		assertEquals(this.ruleIdentity4, "abcd", "abcd");
	}

	public final Rule<Character, String> ruleIdentity5 = identity5((t0, t1, t2, t3, t4) -> "" + t0 + t1 + t2 + t3 + t4, ruleConcatenation11111);

	@Test
	public void testruleIdentity5()
	{
		assertEquals(this.ruleIdentity5, "abcde", "abcde");
	}

	public final Rule<Character, List<Character>> ruleListA = list(ruleTerminalA);

	@Test
	public void testRuleListA()
	{
		assertEquals(this.ruleListA, "a", List.of('a'));
		assertSyntaxIncorrect(this.ruleListA, "", 0);
		assertSyntaxIncorrect(this.ruleListA, "aa", 1);
	}

	public final Rule<Character, List<Character>> ruleFlatten = flatten(concatenationElementElement(this.ruleListA, this.ruleListA));

	@Test
	public void testRuleFlatten()
	{
		assertEquals(this.ruleFlatten, "aa", List.of('a', 'a'));
		assertSyntaxIncorrect(this.ruleFlatten, "", 0);
		assertSyntaxIncorrect(this.ruleFlatten, "a", 1);
		assertSyntaxIncorrect(this.ruleFlatten, "aaa", 2);
		assertSyntaxIncorrect(this.ruleFlatten, "aaaa", 2);
	}

	public final Rule<Character, Character> ruleOption = option(this.ruleTerminalA);

	@Test
	public void testRuleOption()
	{
		assertNull(this.ruleOption, "");
		assertEquals(this.ruleOption, "a", 'a');
		assertSyntaxIncorrect(this.ruleOption, "aa", 1);
	}

	public final Rule<Character, List<Character>> ruleOptionList = optionList(this.ruleListA);

	@Test
	public void testRuleOptionList()
	{
		assertEquals(this.ruleOptionList, "", List.of());
		assertEquals(this.ruleOptionList, "a", List.of('a'));
		assertSyntaxIncorrect(this.ruleOptionList, "aa", 1);
	}

	public final Rule<Character, List<Character>> ruleRepetition = repetition(this.ruleTerminalA);

	@Test
	public void testRuleRepetition()
	{
		assertEquals(this.ruleRepetition, "a");
		assertEquals(this.ruleRepetition, "aa");
		assertEquals(this.ruleRepetition, "aaa");
		assertSyntaxIncorrect(this.ruleRepetition, "", 0);
	}

	public final Rule<Character, List<Character>> ruleRepetition_A = concatenation10(this.ruleRepetition, this.ruleDummyTerminalA);

	@Test
	public abstract void testruleRepetitionA();

	public final Rule<Character, List<Character>> ruleRepetitionLR_A = concatenation10(repetitionLR(this.ruleTerminalA), this.ruleDummyTerminalA);

	@Test
	public abstract void testRuleRepetitionLR_A();

	public final Rule<Character, List<Character>> ruleRepetitionSeperator = repetition(ruleTerminalA, dummy(terminal(',')));

	@Test
	public void testRuleRepetitionSeperator()
	{
		assertEquals(this.ruleRepetitionSeperator, "a", List.of('a'));
		assertEquals(this.ruleRepetitionSeperator, "a,a", List.of('a', 'a'));
		assertEquals(this.ruleRepetitionSeperator, "a,a,a", List.of('a', 'a', 'a'));
		assertSyntaxIncorrect(this.ruleRepetitionSeperator, "", 0);
		assertSyntaxIncorrect(this.ruleRepetitionSeperator, "aa", 1);
	}

	@Test
	public void testKeywordOrNot()
	{
		Rule<Character, List<Character>> rule =
			keywordOrNot
				(
					Set.of('a', 'b', 'c'),
					List.of(Util.convert("aaa"), Util.convert("ab")),
					x -> x,
					'K',
					'N'
				);
		assertSyntaxIncorrect(rule, "", 0);
		assertEquals(rule, "a", Util.convert("Na"));
		assertEquals(rule, "aaa", Util.convert("Kaaa"));
		assertEquals(rule, "aaaa", Util.convert("Naaaa"));
		assertEquals(rule, "ab", Util.convert("Kab"));
		assertEquals(rule, "abc", Util.convert("Nabc"));
	}

	public void testNotKeyword(Set<String> keywords)
	{
		Rule<Character, List<Character>> rule = notKeyword
			(
				Set.of('a', 'b'),
				keywords.stream().map(Util::convert).collect(Collectors.toSet())
			);
		for (String word: List.of("", "a", "b", "aa", "ab", "ba", "bb", "aaa", "aab", "aba", "abb", "baa", "bab", "bba", "bbb"))
		{
			if (keywords.contains(word))
			{
				assertSyntaxIncorrect(rule, word, word.length());
			}
			else
			{
				assertEquals(rule, word);
			}
		}
	}

	@Test
	public void testNotKeyword()
	{
		testNotKeyword(Set.of());
		testNotKeyword(Set.of(""));
		testNotKeyword(Set.of("a"));
		testNotKeyword(Set.of("", "a"));
		testNotKeyword(Set.of("aa"));
		testNotKeyword(Set.of("ab"));
		testNotKeyword(Set.of("", "ab"));
	}

	public final Rule<Character, List<Character>> ruleAlternating = alternating(this.ruleTerminalA, this.ruleTerminalB);

	@Test
	public void testRuleAlternating()
	{
		assertEquals(this.ruleAlternating, "");
		assertEquals(this.ruleAlternating, "a");
		assertEquals(this.ruleAlternating, "b");
		assertEquals(this.ruleAlternating, "ab");
		assertEquals(this.ruleAlternating, "ba");
		assertEquals(this.ruleAlternating, "aba");
		assertEquals(this.ruleAlternating, "bab");
		assertEquals(this.ruleAlternating, "abab");
		assertEquals(this.ruleAlternating, "baba");
		assertEquals(this.ruleAlternating, "ababa");
		assertEquals(this.ruleAlternating, "babab");
		assertSyntaxIncorrect(this.ruleAlternating, "aa", 1);
		assertSyntaxIncorrect(this.ruleAlternating, "bb", 1);
		assertSyntaxIncorrect(this.ruleAlternating, "aab", 1);
		assertSyntaxIncorrect(this.ruleAlternating, "abb", 2);
		assertSyntaxIncorrect(this.ruleAlternating, "baa", 2);
		assertSyntaxIncorrect(this.ruleAlternating, "bba", 1);
	}
}