package nl.novit.parser.mock;

import nl.novit.parser.parser.Parser;

import java.util.List;

public abstract class MockParser
	implements Parser<Character, List<Character>>
{
}