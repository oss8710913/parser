package nl.novit.parser.mock;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Input;
import nl.novit.parser.internal.InputImmutable;
import nl.novit.parser.internal.InputStackForward;
import nl.novit.parser.internal.InputStackReverse;
import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleDefault;
import nl.novit.parser.rule.RuleUnion;
import nl.novit.util.Tuple2Comparable;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class MockRuleDefault<TypeTarget>
	extends RuleDefault<Character, TypeTarget>
{
	public static MockRuleDefault<?> RULE_0 = new MockRuleDefault<>();
	public static MockRuleDefault<?> RULE_1 = new MockRuleDefault<>();

	@Override
	public ParseTree<? extends TypeTarget> parseUnger(InputImmutable<Character> input, Set<Tuple2Comparable<Rule<Character, ?>, Input<Character>>> visited)
		throws ExceptionParserInput
	{
		return null;
	}

	@Override
	public boolean initializeFirstTerminals(Set<RuleUnion<Character, ?>> visited)
	{
		return false;
	}

	@Override
	public Set<TerminalOrEmpty<Character>> firstTerminals()
	{
		return null;
	}

	@Override
	public void checkLL1(Map<RuleUnion<Character, ?>, Set<TerminalOrEmpty<Character>>> visited, Set<TerminalOrEmpty<Character>> followingTerminals)
		throws ExceptionParserAmbiguous
	{
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLL1(InputStackForward<Character> input)
		throws ExceptionParserInput
	{
		return null;
	}

	@Override
	public Set<Item<Character, ?>> items()
	{
		return null;
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLR(InputStackReverse<Character> input, Stack<Rule<Character, ?>> rules)
	{
		return null;
	}

	@Override
	public void random(List<Character> result)
	{
	}
}