package nl.novit.parser.mock;

import nl.novit.parser.internal.InputStackReverse;

import java.util.List;

public class MockInputStackReverse
	extends InputStackReverse<Character>
{
	public MockInputStackReverse(List<Character> list)
	{
		super(list);
	}
}