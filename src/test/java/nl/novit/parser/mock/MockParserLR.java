package nl.novit.parser.mock;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.internal.State;
import nl.novit.parser.parser.ParserLR;
import nl.novit.parser.rule.Rule;

import java.util.List;

public abstract class MockParserLR<TypeState extends State<Character, TypeState>>
	extends ParserLR<Character, List<Character>, TypeState>
{
	public MockParserLR(Rule<Character, List<Character>> ruleStart)
		throws ExceptionParserAmbiguous
	{
		super(ruleStart);
	}
}