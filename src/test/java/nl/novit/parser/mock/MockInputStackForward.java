package nl.novit.parser.mock;

import nl.novit.parser.internal.InputStackForward;

import java.util.List;

public class MockInputStackForward
	extends InputStackForward<Character>
{
	public MockInputStackForward(List<Character> list)
	{
		super(list);
	}
}