package nl.novit.parser.mock;

import nl.novit.parser.internal.ExtendedItem;
import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.ItemDefaultShift;
import nl.novit.parser.internal.ItemDefaultShiftConcatenationDotLeftRight;
import nl.novit.parser.internal.ItemDefaultShiftConcatenationLeftDotRight;
import nl.novit.parser.internal.ItemDefaultShiftTerminal;
import nl.novit.parser.internal.ItemDefaultShiftUnion;
import nl.novit.parser.internal.RuleOrTerminal;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.parser.rule.Rule;

import java.util.Set;

public class MockItemDefaultShift
	extends ItemDefaultShift<Character, Rule<Character, ?>>
{
	public MockItemDefaultShift(Rule<Character, ?> ruleLHS, Item<Character, ?> itemNext)
	{
		super(ruleLHS, itemNext);
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationDotLeftRight<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationLeftDotRight<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultShiftTerminal<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultShiftUnion<Character> that)
	{
		return 0;
	}

	@Override
	public Set<ExtendedItem<Character, ?>> close(TerminalOrEmpty<Character> lookAhead)
	{
		return null;
	}

	@Override
	public RuleOrTerminal<Character> getActive()
	{
		return null;
	}

	@Override
	public int compareTo(Item<Character, ?> characterItem)
	{
		return 0;
	}
}