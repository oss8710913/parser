package nl.novit.parser.mock;

import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.ItemDefaultReduce;
import nl.novit.parser.internal.ItemDefaultReduceConcatenation;
import nl.novit.parser.internal.ItemDefaultReduceEmpty;
import nl.novit.parser.internal.ItemDefaultReduceTerminal;
import nl.novit.parser.internal.ItemDefaultReduceUnion;
import nl.novit.parser.rule.Rule;

public class MockItemDefaultReduce
	extends ItemDefaultReduce<Character, Rule<Character, ?>>
{
	public MockItemDefaultReduce(Rule<Character, ?> ruleLHS)
	{
		super(ruleLHS);
	}

	@Override
	public int compareTo(ItemDefaultReduceConcatenation<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultReduceEmpty<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultReduceTerminal<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultReduceUnion<Character> that)
	{
		return 0;
	}

	@Override
	public int getReduced()
	{
		return 0;
	}

	@Override
	public int compareTo(Item<Character, ?> characterItem)
	{
		return 0;
	}
}