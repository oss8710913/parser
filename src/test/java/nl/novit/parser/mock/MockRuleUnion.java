package nl.novit.parser.mock;

import nl.novit.parser.rule.RuleUnion;

import java.util.List;

public class MockRuleUnion
	extends RuleUnion<Character, List<Character>>
{
	public static final MockRuleUnion RULE_0 = new MockRuleUnion();
	public static final MockRuleUnion RULE_1 = new MockRuleUnion();
}