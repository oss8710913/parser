package nl.novit.parser.mock;

import nl.novit.parser.rule.Rule;

import java.util.List;

public abstract class MockRule
	extends Rule<Character, List<Character>>
{
	public MockRule()
	{
		super();
	}
}