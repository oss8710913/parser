package nl.novit.parser.mock;

import nl.novit.parser.rule.RuleEmpty;

public class MockRuleEmpty<TypeTarget>
	extends RuleEmpty<Character, TypeTarget>
{
	public static MockRuleEmpty<?> RULE_0 = new MockRuleEmpty<>();
	public static MockRuleEmpty<?> RULE_1 = new MockRuleEmpty<>();

	@Override
	public TypeTarget getTarget()
	{
		return null;
	}
}