package nl.novit.parser.mock;

import nl.novit.parser.rule.RuleTerminal;

public class MockRuleTerminal<TypeTarget>
	extends RuleTerminal<Character, TypeTarget>
{
	public static MockRuleTerminal<?> RULE_A = new MockRuleTerminal<>('a');
	public static MockRuleTerminal<?> RULE_B = new MockRuleTerminal<>('b');

	public MockRuleTerminal(Character character)
	{
		super(character);
	}

	@Override
	public TypeTarget getTarget()
	{
		return null;
	}
}