package nl.novit.parser.mock;

import nl.novit.parser.internal.ItemDefaultReduceConcatenation;
import nl.novit.parser.rule.RuleConcatenation;

public class MockItemDefaultReduceConcatenation
	extends ItemDefaultReduceConcatenation<Character>
{
	public MockItemDefaultReduceConcatenation(RuleConcatenation<Character, ?, ?, ?> ruleLHS)
	{
		super(ruleLHS);
	}
}