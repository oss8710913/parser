package nl.novit.parser.mock;

import nl.novit.parser.common.Range;
import nl.novit.parser.internal.Input;

import java.util.List;

public class MockInput
	extends Input<Character>
{
	public MockInput(List<Character> list, Range range)
	{
		super(list, range);
	}

	public MockInput(List<Character> list)
	{
		super(list);
	}

	@Override
	public Range rangeEmpty()
	{
		return null;
	}

	@Override
	public Range rangeTerminal()
	{
		return null;
	}
}