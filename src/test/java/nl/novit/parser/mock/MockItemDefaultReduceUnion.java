package nl.novit.parser.mock;

import nl.novit.parser.internal.ItemDefaultReduceUnion;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;

public class MockItemDefaultReduceUnion
	extends ItemDefaultReduceUnion<Character>
{
	public MockItemDefaultReduceUnion(RuleUnion<Character, ?> ruleLHS, Rule<Character, ?> ruleRHS)
	{
		super(ruleLHS, ruleRHS);
	}
}