package nl.novit.parser.mock;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.internal.ExtendedItem;
import nl.novit.parser.internal.State;

import java.util.Set;

public abstract class MockState<TypeState extends State<Character, TypeState>>
	extends State<Character, TypeState>
{
	public MockState(Set<ExtendedItem<Character, ?>> extendedItems)
		throws ExceptionParserAmbiguous
	{
		super(extendedItems);
	}
}