package nl.novit.parser.mock;

import nl.novit.parser.internal.ItemDefaultReduceTerminal;
import nl.novit.parser.rule.RuleTerminal;

public class MockItemDefaultReduceTerminal
	extends ItemDefaultReduceTerminal<Character>
{
	public MockItemDefaultReduceTerminal(RuleTerminal<Character, ?> ruleLHS)
	{
		super(ruleLHS);
	}
}