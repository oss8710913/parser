package nl.novit.parser.mock;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.internal.State;
import nl.novit.parser.parser.ParserLR;
import nl.novit.parser.rule.Rule;

import java.util.List;

public abstract class MockParserLALR1<TypeState extends State<Character, TypeState>>
	extends ParserLR<Character, List<Character>, TypeState>
{
	public MockParserLALR1(Rule<Character, List<Character>> ruleStart)
		throws ExceptionParserAmbiguous
	{
		super(ruleStart);
	}
}