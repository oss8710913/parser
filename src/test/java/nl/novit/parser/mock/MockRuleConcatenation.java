package nl.novit.parser.mock;

import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleConcatenation;

public class MockRuleConcatenation<TypeTarget, TypeSourceLeft, TypeSourceRight>
	extends RuleConcatenation<Character, TypeTarget, TypeSourceLeft, TypeSourceRight>
{
	public static MockRuleConcatenation<?, ?, ?> RULE_0 = new MockRuleConcatenation<>(MockRuleDefault.RULE_0, MockRuleDefault.RULE_1);
	public static MockRuleConcatenation<?, ?, ?> RULE_1 = new MockRuleConcatenation<>(MockRuleDefault.RULE_0, MockRuleDefault.RULE_1);

	public MockRuleConcatenation(Rule<Character, TypeSourceLeft> ruleLeft, Rule<Character, TypeSourceRight> ruleRight)
	{
		super(ruleLeft, ruleRight);
	}

	@Override
	public TypeTarget getTarget(TypeSourceLeft typeSourceLeft, TypeSourceRight typeSourceRight)
	{
		return null;
	}
}