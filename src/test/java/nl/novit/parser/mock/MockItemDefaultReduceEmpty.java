package nl.novit.parser.mock;

import nl.novit.parser.internal.ItemDefaultReduceEmpty;
import nl.novit.parser.rule.RuleEmpty;

public class MockItemDefaultReduceEmpty
	extends ItemDefaultReduceEmpty<Character>
{
	public MockItemDefaultReduceEmpty(RuleEmpty<Character, ?> ruleLHS)
	{
		super(ruleLHS);
	}
}