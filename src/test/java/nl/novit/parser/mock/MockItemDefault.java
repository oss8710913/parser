package nl.novit.parser.mock;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.internal.ExtendedItem;
import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.ItemDefault;
import nl.novit.parser.internal.ItemDefaultReduceConcatenation;
import nl.novit.parser.internal.ItemDefaultReduceEmpty;
import nl.novit.parser.internal.ItemDefaultReduceTerminal;
import nl.novit.parser.internal.ItemDefaultReduceUnion;
import nl.novit.parser.internal.ItemDefaultShiftConcatenationDotLeftRight;
import nl.novit.parser.internal.ItemDefaultShiftConcatenationLeftDotRight;
import nl.novit.parser.internal.ItemDefaultShiftTerminal;
import nl.novit.parser.internal.ItemDefaultShiftUnion;
import nl.novit.parser.internal.State;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.parser.rule.Rule;

import java.util.Set;

public class MockItemDefault
	extends ItemDefault<Character, Rule<Character, ?>>
{
	public static final MockItemDefault ITEM = new MockItemDefault(MockRuleDefault.RULE_0);

	public MockItemDefault(Rule<Character, ?> ruleLHS)
	{
		super(ruleLHS);
	}

	@Override
	public int compareTo(ItemDefaultReduceConcatenation<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultReduceEmpty<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultReduceTerminal<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultReduceUnion<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationDotLeftRight<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationLeftDotRight<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultShiftTerminal<Character> that)
	{
		return 0;
	}

	@Override
	public int compareTo(ItemDefaultShiftUnion<Character> that)
	{
		return 0;
	}

	@Override
	public Set<ExtendedItem<Character, ?>> close(TerminalOrEmpty<Character> lookAhead)
	{
		return null;
	}

	@Override
	public boolean add(State<Character, ?> state, TerminalOrEmpty<Character> lookAhead)
		throws ExceptionParserAmbiguous
	{
		return false;
	}

	@Override
	public int compareTo(Item<Character, ?> characterItem)
	{
		return 0;
	}
}