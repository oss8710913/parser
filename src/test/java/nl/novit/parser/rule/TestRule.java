package nl.novit.parser.rule;

import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.ExtendedItem;
import nl.novit.parser.internal.TestTerminal;
import nl.novit.parser.mock.MockItemDefault;
import nl.novit.parser.mock.MockRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static org.mockito.Mockito.*;

public class TestRule
{
	public static final int MAGIC_NUMBER = 12345;
	public MockRule rule;

	@BeforeEach
	public void initialise()
	{
		this.rule = mock(MockRule.class);
		when(this.rule.getId()).thenReturn(MAGIC_NUMBER);
		when(this.rule.isEmpty()).thenCallRealMethod();
		when(this.rule.getTerminalOrEmpty(Mockito.any())).thenCallRealMethod();
		when(this.rule.getRuleOrTerminal(Mockito.any())).thenCallRealMethod();
		doCallRealMethod().when(this.rule).initializeFirstTerminals();
		when(this.rule.isPossiblyEmpty()).thenCallRealMethod();
		when(this.rule.extendedItems(any())).thenCallRealMethod();
		when(this.rule.toString()).thenCallRealMethod();
	}

	@Test
	public void testIsEmpty()
	{
		Assertions.assertFalse(this.rule.isEmpty());
	}

	@Test
	public void testGetTerminalOrEmpty()
	{
		Assertions.assertNull(this.rule.getTerminalOrEmpty(new TreeMap<>()));
	}

	@Test
	public void testGetRuleOrTerminal()
	{
		Assertions.assertEquals('a', this.rule.getRuleOrTerminal(Map.of(this.rule, 'a')));
	}

	@Test
	public void testInitializeFirstTerminals1()
	{
		when(this.rule.initializeFirstTerminals(new TreeSet<>())).thenReturn(false);
		this.rule.initializeFirstTerminals();
		verify(this.rule, times(1)).initializeFirstTerminals(new TreeSet<>());
	}

	@Test
	public void testInitializeFirstTerminals2()
	{
		when(this.rule.initializeFirstTerminals(new TreeSet<>())).thenReturn(true, false);
		this.rule.initializeFirstTerminals();
		verify(this.rule, times(2)).initializeFirstTerminals(new TreeSet<>());
	}

	@Test
	public void testInitializeFirstTerminals3()
	{
		when(this.rule.initializeFirstTerminals(new TreeSet<>())).thenReturn(true, true, false);
		this.rule.initializeFirstTerminals();
		verify(this.rule, times(3)).initializeFirstTerminals(new TreeSet<>());
	}

	@Test
	public void testIsPossiblyEmptyFalse()
	{
		when(this.rule.firstTerminals()).thenReturn(Set.of());
		Assertions.assertFalse(this.rule.isPossiblyEmpty());
	}

	@Test
	public void testIsPossiblyEmptyTrue()
	{
		when(this.rule.firstTerminals()).thenReturn(Set.of(new Empty<>()));
		Assertions.assertTrue(this.rule.isPossiblyEmpty());
	}

	@Test
	public void testExtendedItems0()
	{
		when(this.rule.items()).thenReturn(Set.of());
		Assertions.assertEquals(Set.of(), this.rule.extendedItems(TestTerminal.TERMINAL_A));
	}

	@Test
	public void testExtendedItems1()
	{
		MockItemDefault mockItem0 = mock(MockItemDefault.class);
		when(this.rule.items()).thenReturn(Set.of(mockItem0));
		Assertions.assertEquals(Set.of(new ExtendedItem<>(mockItem0, TestTerminal.TERMINAL_A)), this.rule.extendedItems(TestTerminal.TERMINAL_A));
	}

	@Test
	public void testExtendedItems2()
	{
		MockItemDefault mockItem0 = mock(MockItemDefault.class);
		MockItemDefault mockItem1 = mock(MockItemDefault.class);
		when(mockItem0.compareTo(mockItem1)).thenReturn(-1);
		when(mockItem1.compareTo(mockItem0)).thenReturn(1);
		when(this.rule.items()).thenReturn(Set.of(mockItem0, mockItem1));
		Assertions.assertEquals(Set.of(new ExtendedItem<>(mockItem0, TestTerminal.TERMINAL_A), new ExtendedItem<>(mockItem1, TestTerminal.TERMINAL_A)), this.rule.extendedItems(TestTerminal.TERMINAL_A));
	}

	@Test
	public void testToStringId()
	{
		when(this.rule.toStringId()).thenCallRealMethod();
		Assertions.assertEquals("#" + MAGIC_NUMBER, this.rule.toStringId());
	}

	@Test
	public void testToString()
	{
		when(this.rule.toStringId()).thenReturn("#" + MAGIC_NUMBER);
		Assertions.assertEquals("Rule(#" + MAGIC_NUMBER + "->", this.rule.toString());
	}
}