package nl.novit.parser.rule;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.Terminal;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.parser.mock.MockRule;
import nl.novit.util.UtilCollections;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static org.mockito.Mockito.*;

public class TestRuleUnion
{
	public static RuleUnion<Character, List<Character>> RULE;
	public static MockRule MOCK0;
	public static MockRule MOCK1;
	public static Set<RuleUnion<Character, ?>> VISITED_EMPTY;
	public static Set<RuleUnion<Character, ?>> VISITED_RULE;

	@BeforeEach
	public void initialise()
	{
		RULE = new RuleUnion<>()
		{
			@Override
			public Integer getId()
			{
				return 0;
			}
		};
		MOCK0 = mock(MockRule.class);
		MOCK1 = mock(MockRule.class);
		when(MOCK0.getId()).thenReturn(1);
		when(MOCK1.getId()).thenReturn(2);
		RULE.add(MOCK0);
		RULE.add(MOCK1);
		VISITED_EMPTY = new TreeSet<>();
		VISITED_RULE = new TreeSet<>();
		VISITED_RULE.add(RULE);
	}

	public void testInitializeFirstTerminals(boolean expected, Set<RuleUnion<Character, ?>> visited)
	{
		Set<RuleUnion<Character, ?>> visitedOriginal = new TreeSet<>(visited);
		Assertions.assertEquals(expected, RULE.initializeFirstTerminals(visited));
		visitedOriginal.add(RULE);
		Assertions.assertEquals(visitedOriginal, visited);
	}

	@Test
	public void testInitializeFirstTerminalsVisitIsFalse()
	{
		testInitializeFirstTerminals(false, VISITED_RULE);
		verify(MOCK0, never()).initializeFirstTerminals(anySet());
		verify(MOCK1, never()).initializeFirstTerminals(anySet());
	}

	public void testInitializeFirstTerminalsVisitIsTrue
		(
			boolean expected,
			boolean initializeFirstTerminals0,
			boolean initializeFirstTerminals1,
			Set<TerminalOrEmpty<Character>> firstTerminals0,
			Set<TerminalOrEmpty<Character>> firstTerminals1
		)
	{
		when(MOCK0.initializeFirstTerminals(VISITED_RULE)).thenReturn(initializeFirstTerminals0);
		when(MOCK1.initializeFirstTerminals(VISITED_RULE)).thenReturn(initializeFirstTerminals1);
		when(MOCK0.firstTerminals()).thenReturn(firstTerminals0);
		when(MOCK1.firstTerminals()).thenReturn(firstTerminals1);
		testInitializeFirstTerminals(expected, VISITED_EMPTY);
		verify(MOCK0, times(1)).initializeFirstTerminals(VISITED_EMPTY);
		verify(MOCK1, times(1)).initializeFirstTerminals(VISITED_EMPTY);
		Assertions.assertEquals(UtilCollections.union(firstTerminals0, firstTerminals1), RULE.firstTerminals);
	}

	@Test
	public void testInitializeFirstTerminalsIsTrue1()
	{
		testInitializeFirstTerminalsVisitIsTrue(false, false, false, Collections.emptySet(), Collections.emptySet());
	}

	@Test
	public void testInitializeFirstTerminalsIsTrue2()
	{
		testInitializeFirstTerminalsVisitIsTrue(true, false, false, Set.of(new Empty<>()), Collections.emptySet());
	}

	@Test
	public void testInitializeFirstTerminalsIsTrue3()
	{
		testInitializeFirstTerminalsVisitIsTrue(true, false, false, Collections.emptySet(), Set.of(new Terminal<>('a')));
	}

	@Test
	public void testInitializeFirstTerminalsIsTrue4()
	{
		testInitializeFirstTerminalsVisitIsTrue(true, false, false, Set.of(new Terminal<>('a')), Collections.emptySet());
	}

	@Test
	public void testInitializeFirstTerminalsIsTrue5()
	{
		testInitializeFirstTerminalsVisitIsTrue(true, false, true, Collections.emptySet(), Collections.emptySet());
	}

	@Test
	public void testInitializeFirstTerminalsIsTrue6()
	{
		testInitializeFirstTerminalsVisitIsTrue(true, true, false, Collections.emptySet(), Collections.emptySet());
	}

	@Test
	public void testInitializeFirstTerminalsIsTrue7()
	{
		testInitializeFirstTerminalsVisitIsTrue(true, true, true, Collections.emptySet(), Collections.emptySet());
	}

	@Test
	public void testInitializeFirstTerminalsIsTrue8()
	{
		testInitializeFirstTerminalsVisitIsTrue(true, true, true, Collections.emptySet(), Set.of(new Empty<>()));
	}

	public void testCheckLL1
		(
			Map<RuleUnion<Character, ?>, Set<TerminalOrEmpty<Character>>> visited,
			Set<TerminalOrEmpty<Character>> firstTerminals0,
			Set<TerminalOrEmpty<Character>> firstTerminals1,
			Set<TerminalOrEmpty<Character>> followingTerminals
		)
		throws ExceptionParserAmbiguous
	{
		when(MOCK0.firstTerminals()).thenReturn(firstTerminals0);
		when(MOCK1.firstTerminals()).thenReturn(firstTerminals1);
		RULE.initializeFirstTerminals();
		RULE.checkLL1(visited, followingTerminals);
	}

	public void testCheckLL1VisitIsFalse
		(
			Map<RuleUnion<Character, ?>, Set<TerminalOrEmpty<Character>>> visited,
			Set<TerminalOrEmpty<Character>> firstTerminals0,
			Set<TerminalOrEmpty<Character>> firstTerminals1,
			Set<TerminalOrEmpty<Character>> followingTerminals
		)
		throws ExceptionParserAmbiguous
	{
		testCheckLL1
			(
				visited,
				firstTerminals0,
				firstTerminals1,
				followingTerminals
			);
		verify(MOCK0, never()).checkLL1(visited, followingTerminals);
		verify(MOCK1, never()).checkLL1(visited, followingTerminals);
	}

	@Test
	public void testCheckLL1VisitIsFalse1()
		throws ExceptionParserAmbiguous
	{
		Set<TerminalOrEmpty<Character>> followingTerminals = Collections.emptySet();
		Map<RuleUnion<Character, ?>, Set<TerminalOrEmpty<Character>>> visited = UtilCollections.mapOf(RULE, followingTerminals);
		testCheckLL1VisitIsFalse
			(
				visited,
				UtilCollections.setOf(new Terminal<>('a')),
				UtilCollections.setOf(new Terminal<>('b')),
				followingTerminals
			);
	}

	@Test
	public void testCheckLL1VisitIsFalse2()
		throws ExceptionParserAmbiguous
	{
		Set<TerminalOrEmpty<Character>> followingTerminals = UtilCollections.setOf(new Terminal<>('c'));
		Map<RuleUnion<Character, ?>, Set<TerminalOrEmpty<Character>>> visited = UtilCollections.mapOf(RULE, followingTerminals);
		testCheckLL1VisitIsFalse
			(
				visited,
				UtilCollections.setOf(new Terminal<>('a')),
				UtilCollections.setOf(new Terminal<>('b')),
				followingTerminals
			);
	}

	public void testCheckLL1VisitIsTrue
		(
			Map<RuleUnion<Character, ?>, Set<TerminalOrEmpty<Character>>> visited,
			Set<TerminalOrEmpty<Character>> firstTerminals0,
			Set<TerminalOrEmpty<Character>> firstTerminals1,
			Set<TerminalOrEmpty<Character>> followingTerminals
		)
		throws ExceptionParserAmbiguous
	{
		testCheckLL1
			(
				visited,
				firstTerminals0,
				firstTerminals1,
				followingTerminals
			);
		verify(MOCK0, times(1)).checkLL1(visited, followingTerminals);
		verify(MOCK1, times(1)).checkLL1(visited, followingTerminals);
	}

	public void testCheckLL1VisitIsTrueDoesNotThrow
		(
			Map<RuleUnion<Character, ?>, Set<TerminalOrEmpty<Character>>> visited,
			Set<TerminalOrEmpty<Character>> firstTerminals0,
			Set<TerminalOrEmpty<Character>> firstTerminals1,
			Set<TerminalOrEmpty<Character>> followingTerminals
		)
	{
		Assertions.assertDoesNotThrow
			(
				() ->
				{
					testCheckLL1VisitIsTrue
						(
							visited,
							firstTerminals0,
							firstTerminals1,
							followingTerminals
						);
				}
			);
	}

	@Test
	public void testCheckLL1VisitIsTrueDoesNotThrow1()
	{
		testCheckLL1VisitIsTrueDoesNotThrow
			(
				UtilCollections.mapOf(RULE, new TreeSet<>()),
				UtilCollections.setOf(new Terminal<>('a')),
				UtilCollections.setOf(new Terminal<>('b')),
				UtilCollections.setOf(new Terminal<>('c'))
			);
	}

	@Test
	public void testCheckLL1VisitIsTrueDoesNotThrow2()
	{
		Set<TerminalOrEmpty<Character>> followingTerminals = UtilCollections.setOf(new Terminal<>('c'));
		testCheckLL1VisitIsTrueDoesNotThrow
			(
				new TreeMap<>(),
				UtilCollections.setOf(new Terminal<>('a')),
				UtilCollections.setOf(new Terminal<>('b')),
				followingTerminals
			);
	}

	@Test
	public void testCheckLL1VisitIsTrueDoesNotThrow3()
	{
		Set<TerminalOrEmpty<Character>> followingTerminals = UtilCollections.setOf(new Terminal<>('a'));
		testCheckLL1VisitIsTrueDoesNotThrow
			(
				new TreeMap<>(),
				UtilCollections.setOf(new Terminal<>('a')),
				UtilCollections.setOf(new Terminal<>('b')),
				followingTerminals
			);
	}

	@Test
	public void testCheckLL1VisitIsTrueDoesNotThrow4()
	{
		Set<TerminalOrEmpty<Character>> followingTerminals = UtilCollections.setOf(new Terminal<>('c'));
		testCheckLL1VisitIsTrueDoesNotThrow
			(
				new TreeMap<>(),
				UtilCollections.setOf(new Empty<>()),
				UtilCollections.setOf(new Terminal<>('b')),
				followingTerminals
			);
	}

	public void testCheckLL1VisitIsTrueThrowsExceptionParserAmbiguous
		(
			Map<RuleUnion<Character, ?>, Set<TerminalOrEmpty<Character>>> visited,
			Set<TerminalOrEmpty<Character>> firstTerminals0,
			Set<TerminalOrEmpty<Character>> firstTerminals1,
			Set<TerminalOrEmpty<Character>> followingTerminals
		)
	{
		Assertions.assertThrows
			(
				ExceptionParserAmbiguous.class,
				() ->
				{
					testCheckLL1VisitIsTrue
						(
							visited,
							firstTerminals0,
							firstTerminals1,
							followingTerminals
						);
				}
			);
	}

	@Test
	public void testCheckLL1VisitIsTrueThrowsExceptionParserInput1()
	{
		testCheckLL1VisitIsTrueThrowsExceptionParserAmbiguous
			(
				new TreeMap<>(),
				UtilCollections.setOf(new Empty<>()),
				UtilCollections.setOf(new Empty<>()),
				Collections.emptySet()
			);
	}

	@Test
	public void testCheckLL1VisitIsTrueThrowsExceptionParserInput2()
	{
		testCheckLL1VisitIsTrueThrowsExceptionParserAmbiguous
			(
				new TreeMap<>(),
				UtilCollections.setOf(new Terminal<>('a'), new Terminal<>('b')),
				UtilCollections.setOf(new Terminal<>('a')),
				Collections.emptySet()
			);
	}

	@Test
	public void testCheckLL1VisitIsTrueThrowsExceptionParserInput3()
	{
		Set<TerminalOrEmpty<Character>> followingTerminals = UtilCollections.setOf(new Terminal<>('c'));
		testCheckLL1VisitIsTrueThrowsExceptionParserAmbiguous
			(
				new TreeMap<>(),
				UtilCollections.setOf(new Empty<>()),
				UtilCollections.setOf(new Terminal<>('c')),
				followingTerminals
			);
	}

	public void testParserLL1
		(
			Character terminal,
			Set<TerminalOrEmpty<Character>> firstTerminals0,
			Set<TerminalOrEmpty<Character>> firstTerminals1
		)
		throws ExceptionParserInput
	{
//		MockInputStack mockInputStack = mock(MockInputStack.class);
//		when(MOCK0.firstTerminals()).thenReturn(firstTerminals0);
//		when(MOCK0.parseLL1(mockInputStack)).thenReturn(new ParseTree<>(List.of('x', 'y'), Collections.emptyList()));
//		when(MOCK1.firstTerminals()).thenReturn(firstTerminals1);
//		when(MOCK1.parseLL1(mockInputStack)).thenReturn(null);
//		when(mockInputStack.peek()).thenReturn(terminal);
//		when(mockInputStack.getRange()).thenReturn(new Range(0, 1));
//		Assertions.assertEquals(List.of('x', 'y'), RULE.parseLL1(mockInputStack).getTarget());
	}

	public void testParserLL1DoesNotThrow
		(
			Character terminalOrEmpty,
			Set<TerminalOrEmpty<Character>> firstTerminals0,
			Set<TerminalOrEmpty<Character>> firstTerminals1
		)
	{
		Assertions.assertDoesNotThrow
			(
				() ->
				{
					testParserLL1
						(
							terminalOrEmpty,
							firstTerminals0,
							firstTerminals1
						);
				}
			);
	}

	@Test
	public void testParserLL1DoesNotThrow1()
	{
		testParserLL1DoesNotThrow
			(
				'a',
				Set.of(new Terminal<>('a')),
				Set.of(new Terminal<>('b'))
			);
	}

	@Test
	public void testParserLL1DoesNotThrow2()
	{
		testParserLL1DoesNotThrow
			(
				'a',
				Set.of(new Terminal<>('a')),
				Set.of(new Empty<>())
			);
	}

	@Test
	public void testParserLL1DoesNotThrow3()
	{
		testParserLL1DoesNotThrow
			(
				'b',
				Set.of(new Empty<>()),
				Set.of(new Terminal<>('a'))
			);
	}

	@Test
	public void testParserLL1ThrowsParserInput()
	{
		// TODO
//		Assertions.assertThrows
//			(
//				ExceptionParserInput.class,
//				() ->
//				{
//					testParserLL1
//						(
//							'c',
//							Set.of(new Terminal<>('a')),
//							Set.of(new Terminal<>('b'))
//						);
//				}
//			);
	}

	public void testParserLR(Rule<Character, ?> rule)
	{
//		ParseTree<? extends List<Character>> expected0 = new ParseTree<>(List.of('a', 'b'), Collections.emptyList());
//		List<Character> expected1 = List.of('c', 'd');
//		Stack<Rule<Character, ?>> rules = new Stack<>();
//		rules.push(rule);
//		rules.push(RULE);
//		MockInputStack mockInputStack = mock(MockInputStack.class);
//		when(MOCK0.parseLR(mockInputStack, rules)).thenReturn(expected0);
//		when(MOCK1.parseLR(mockInputStack, rules)).thenReturn(expected1);
//		Assertions.assertEquals(rule.parseLR(mockInputStack, rules), RULE.parseLR(mockInputStack, rules));
//		Assertions.assertEquals(rule, rules.pop());
	}

	@Test
	public void testParserLR()
	{
		testParserLR(MOCK0);
		testParserLR(MOCK1);
	}

	@Test
	public void testToString()
	{
		when(MOCK0.toStringId()).thenCallRealMethod();
		when(MOCK1.toStringId()).thenCallRealMethod();
		Assertions.assertEquals("Rule(#0->#1|#2)", RULE.toString());
	}
}