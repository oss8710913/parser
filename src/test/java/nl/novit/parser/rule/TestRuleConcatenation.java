package nl.novit.parser.rule;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.Terminal;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.parser.mock.MockRule;
import nl.novit.util.Tuple2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static org.mockito.Mockito.*;

public class TestRuleConcatenation
{
	public Map<RuleUnion<Character, ?>, Set<TerminalOrEmpty<Character>>> visited;
	public Set<TerminalOrEmpty<Character>> followingTerminals;
	public MockRule ruleLeft;
	public MockRule ruleRight;
	public RuleConcatenation<Character, Tuple2<List<Character>, List<Character>>, List<Character>, List<Character>> rule;

	@BeforeEach
	public void initialise()
	{
		visited = new TreeMap<>();
		followingTerminals = Set.of(new Terminal<>('a'));
		ruleLeft = mock(MockRule.class);
		ruleRight = mock(MockRule.class);
		rule = new RuleConcatenation<>(ruleLeft, ruleRight)
		{
			@Override
			public Integer getId()
			{
				return 0;
			}

			@Override
			public Tuple2<List<Character>, List<Character>> getTarget(List<Character> left, List<Character> right)
			{
				return new Tuple2<>(left, right);
			}
		};
	}

	public void testParseUnger()
	{
	}

	public void testInitializeFirstTerminals(boolean firstTerminalsLeft, boolean firstTerminalsRight)
	{
		Set<RuleUnion<Character, ?>> visited = Collections.emptySet();
		when(ruleLeft.initializeFirstTerminals(visited)).thenReturn(firstTerminalsLeft);
		when(ruleRight.initializeFirstTerminals(visited)).thenReturn(firstTerminalsRight);
		Assertions.assertEquals(firstTerminalsLeft || firstTerminalsRight, rule.initializeFirstTerminals(visited));
		verify(ruleLeft).initializeFirstTerminals(visited);
		verify(ruleRight).initializeFirstTerminals(visited);
	}

	@Test
	public void testInitializeFirstTerminalsFalseFalse()
	{
		testInitializeFirstTerminals(false, false);
	}

	@Test
	public void testInitializeFirstTerminalsFalseTrue()
	{
		testInitializeFirstTerminals(false, true);
	}

	@Test
	public void testInitializeFirstTerminalsTrueFalse()
	{
		testInitializeFirstTerminals(true, false);
	}

	@Test
	public void testInitializeFirstTerminalsTrueTrue()
	{
		testInitializeFirstTerminals(true, true);
	}

	public void testFirstTerminals(Set<TerminalOrEmpty<Character>> expected, Set<TerminalOrEmpty<Character>> setLeft, Set<TerminalOrEmpty<Character>> setRight)
	{
		when(ruleLeft.firstTerminals()).thenReturn(setLeft);
		when(ruleRight.firstTerminals()).thenReturn(setRight);
		Assertions.assertEquals(expected, rule.firstTerminals());
	}

	@Test
	public void testFirstTerminals()
	{
		TerminalOrEmpty<Character> empty = new Empty<>();
		TerminalOrEmpty<Character> a = new Terminal<>('a');
		TerminalOrEmpty<Character> b = new Terminal<>('b');
		testFirstTerminals(Set.of(empty), Set.of(empty), Set.of(empty));
		testFirstTerminals(Set.of(b), Set.of(empty), Set.of(b));
		testFirstTerminals(Set.of(empty, b), Set.of(empty), Set.of(empty, b));
		testFirstTerminals(Set.of(a), Set.of(a), Set.of(empty));
		testFirstTerminals(Set.of(a), Set.of(a), Set.of(b));
		testFirstTerminals(Set.of(a), Set.of(a), Set.of(empty, b));
		testFirstTerminals(Set.of(empty, a), Set.of(empty, a), Set.of(empty));
		testFirstTerminals(Set.of(a, b), Set.of(empty, a), Set.of(b));
		testFirstTerminals(Set.of(empty, a, b), Set.of(empty, a), Set.of(empty, b));
	}

	public void testCheckLL1(Set<TerminalOrEmpty<Character>> firstTerminalsRight)
		throws ExceptionParserAmbiguous
	{
		when(ruleRight.firstTerminals()).thenReturn(firstTerminalsRight);
		rule.checkLL1(visited, followingTerminals);
		verify(ruleLeft, times(1)).checkLL1(visited, ruleRight.firstTerminals());
		verify(ruleRight, times(1)).checkLL1(visited, followingTerminals);
	}

	@Test
	public void testCheckLL1WithEmptyTerminal()
		throws ExceptionParserAmbiguous
	{
		testCheckLL1(Set.of(new Empty<>()));
		verify(ruleLeft, times(1)).checkLL1(visited, followingTerminals);
	}

	@Test
	public void testCheckLL1WithoutEmptyTerminal()
		throws ExceptionParserAmbiguous
	{
		testCheckLL1(Collections.emptySet());
		verify(ruleLeft, never()).checkLL1(visited, followingTerminals);
	}

	@Test
	public void testParseLL1()
		throws ExceptionParserInput
	{
		// TODO
//		List<Character> left = List.of('a', 'b');
//		List<Character> right = List.of('c', 'd');
//		MockInputStack mockInputStack = mock(MockInputStack.class);
//		Ranges ranges = new Ranges();
//		when(MOCK_LEFT.parseLL1(mockInputStack, ranges)).thenReturn(left);
//		when(MOCK_RIGHT.parseLL1(mockInputStack, ranges)).thenReturn(right);
//		Assertions.assertEquals(RULE.getTarget(left, right), RULE.parseLL1(mockInputStack, ranges));
//		InOrder inOrder = Mockito.inOrder(MOCK_LEFT, MOCK_RIGHT);
//		inOrder.verify(MOCK_LEFT, times(1)).parseLL1(mockInputStack, ranges);
//		inOrder.verify(MOCK_RIGHT, times(1)).parseLL1(mockInputStack, ranges);
		// TODO Verify mockranges
	}

	@Test
	public void testParseLR()
	{
		// TODO
//		List<Character> left = List.of('a', 'b');
//		List<Character> right = List.of('c', 'd');
//		MockInputStack mockInputStack = mock(MockInputStack.class);
//		MockStack mockStack = mock(MockStack.class);
//		Ranges ranges = new Ranges();
//
//		when(MOCK_LEFT.parseLR(mockInputStack, mockStack, ranges)).thenReturn(left);
//		when(MOCK_RIGHT.parseLR(mockInputStack, mockStack, ranges)).thenReturn(right);
//		Assertions.assertEquals(RULE.getTarget(left, right), RULE.parseLR(mockInputStack, mockStack, ranges));
//		Mockito.verify(mockStack, times(1)).pop();
//		InOrder inOrder = Mockito.inOrder(MOCK_LEFT, MOCK_RIGHT);
//		inOrder.verify(MOCK_RIGHT, times(1)).parseLR(mockInputStack, mockStack, ranges);
//		inOrder.verify(MOCK_LEFT, times(1)).parseLR(mockInputStack, mockStack, ranges);
		// TODO verify inputStack and ranges
	}

	@Test
	public void testToString()
	{
		when(ruleLeft.toStringId()).thenReturn("#1");
		when(ruleRight.toStringId()).thenReturn("#2");
		Assertions.assertEquals("Rule(#0->#1#2)", rule.toString());
	}
}