package nl.novit.parser.rule;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.Terminal;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.parser.mock.MockRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

public class TestRuleIdentity
{
	public static RuleIdentity<Character, Character, List<Character>> RULE;
	public static MockRule MOCK;

	@BeforeEach
	public void initialise()
	{
		MOCK = mock(MockRule.class);
		RULE = new RuleIdentity<>(MOCK)
		{
			@Override
			public Character getTarget(List<Character> characters)
			{
				return characters.get(0);
			}
		};
	}

	@Test
	public void testGetId()
	{
		when(MOCK.getId()).thenReturn(12345);
		Assertions.assertEquals(12345, RULE.getId());
		verify(MOCK, atLeast(1)).getId();
	}

	public void testInitializeFirstTerminals(boolean firstTerminals)
	{
		Set<RuleUnion<Character, ?>> visited = Collections.emptySet();
		when(MOCK.initializeFirstTerminals(visited)).thenReturn(firstTerminals);
		Assertions.assertEquals(firstTerminals, RULE.initializeFirstTerminals(visited));
		verify(MOCK).initializeFirstTerminals(visited);
	}

	@Test
	public void testInitializeFirstTerminalsFalse()
	{
		testInitializeFirstTerminals(false);
	}

	@Test
	public void testInitializeFirstTerminalsTrue()
	{
		testInitializeFirstTerminals(true);
	}

	public void testFirstTerminals(Set<TerminalOrEmpty<Character>> expected)
	{
		when(MOCK.firstTerminals()).thenReturn(expected);
		Assertions.assertEquals(expected, RULE.firstTerminals());
	}

	@Test
	public void testFirstTerminals()
	{
		testFirstTerminals(Set.of(new Empty<>()));
		testFirstTerminals(Set.of(new Terminal<>('a')));
	}

	@Test
	public void testCheckLL1()
		throws ExceptionParserAmbiguous
	{
		RULE.checkLL1(Collections.emptyMap(), Collections.emptySet());
		verify(MOCK, times(1)).checkLL1(Collections.emptyMap(), Collections.emptySet());
	}

	@Test
	public void testParseLL1()
		throws ExceptionParserInput
	{
		// TODO
//		final List<Character> list = List.of('a', 'b');
//		MockInputStack mockInputStack = mock(MockInputStack.class);
//		Ranges mockRanges = mock(Ranges.class);
//		when(MOCK.parseLL1(mockInputStack, mockRanges)).thenReturn(list);
//		Assertions.assertEquals(RULE.getTarget(list), RULE.parseLL1(mockInputStack, mockRanges));
//		verify(MOCK, times(1)).parseLL1(mockInputStack, mockRanges);
		// TODO verify Ranges
	}

	@Test
	public void testParseLR()
	{
		// TODO
//		final List<Character> list = List.of('a', 'b');
//		MockInputStack mockInputStack = mock(MockInputStack.class);
//		MockStack mockStack = mock(MockStack.class);
//		Ranges mockRanges = mock(Ranges.class);
//		when(MOCK.parseLR(mockInputStack, mockStack, mockRanges)).thenReturn(list);
//		Assertions.assertEquals(RULE.getTarget(list), RULE.parseLR(mockInputStack, mockStack, mockRanges));
//		verify(MOCK, times(1)).parseLR(mockInputStack, mockStack, mockRanges);
		// TODO verify inputStack and ranges
	}

	@Test
	public void testToString()
	{
		when(MOCK.toString()).thenReturn("rule");
		Assertions.assertEquals("rule", RULE.toString());
	}
}