package nl.novit.parser.rule;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class TestRuleEmpty
{
	public static final RuleEmpty<Character, List<Character>> RULE = new RuleEmpty<>()
	{
		@Override
		public Integer getId()
		{
			return 0;
		}

		@Override
		public List<Character> getTarget()
		{
			return new ArrayList<>();
		}
	};

	@Test
	public void testInitializeFirstTerminals()
	{
		Assertions.assertFalse(RULE.initializeFirstTerminals(Collections.emptySet()));
	}

	@Test
	public void testFirstTerminals()
	{
		Assertions.assertEquals(Set.of(new Empty<>()), RULE.firstTerminals());
	}

	@Test
	public void testCheckLL1()
	{
		RULE.checkLL1(Collections.emptyMap(), Collections.emptySet());
	}

	@Test
	public void testParseLL1()
		throws ExceptionParserInput
	{
		// TODO
//		MockInputStack mockInputStack = mock(MockInputStack.class);
//		Ranges ranges = new Ranges();
//		Assertions.assertEquals(RULE.getTarget(), RULE.parseLL1(mockInputStack, ranges));
//		verify(mockInputStack, never()).pop();
		// TODO verify ranges
	}

	@Test
	public void testParseLR()
	{
		// TODO
//		MockInputStack mockInputStack = mock(MockInputStack.class);
//
//		MockStack mockStack = mock(MockStack.class);
//		Ranges mockRanges = mock(Ranges.class);
//
//		Assertions.assertEquals(RULE.getTarget(), RULE.parseLR(mockInputStack, mockStack, mockRanges));
//		verify(mockStack, times(1)).pop();
		// TODO verify inputStack and ranges
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("Rule(#0->ϵ)", RULE.toString());
	}
}