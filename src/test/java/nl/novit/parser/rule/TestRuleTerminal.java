package nl.novit.parser.rule;

import nl.novit.parser.common.Range;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Terminal;
import nl.novit.parser.mock.MockInputStackForward;
import nl.novit.parser.mock.MockInputStackReverse;
import nl.novit.parser.mock.MockStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

public class TestRuleTerminal
{
	public static RuleTerminal<Character, List<Character>> RULE_TERMINAL_A = new RuleTerminal<>('a')
	{
		@Override
		public Integer getId()
		{
			return 0;
		}

		@Override
		public List<Character> getTarget()
		{
			return new ArrayList<>();
		}
	};

	@Test
	public void testInitializeFirstTerminals()
	{
		Assertions.assertFalse(RULE_TERMINAL_A.initializeFirstTerminals(Collections.emptySet()));
	}

	@Test
	public void testFirstTerminals()
	{
		Assertions.assertEquals(Set.of(new Terminal<>('a')), RULE_TERMINAL_A.firstTerminals());
	}

	@Test
	public void testCheckLL1()
	{
		RULE_TERMINAL_A.checkLL1(Collections.emptyMap(), Collections.emptySet());
	}

	public void testParseLL1(Character character)
		throws ExceptionParserInput
	{
		MockInputStackForward mockInputStack = mock(MockInputStackForward.class);
		when(mockInputStack.peek()).thenReturn(new Terminal<>(character));
		when(mockInputStack.getRange()).thenReturn(new Range(0, 1));
		Assertions.assertEquals(RULE_TERMINAL_A.getTarget(), RULE_TERMINAL_A.parseLL1(mockInputStack).getTarget());
		verify(mockInputStack, times(1)).pop();
	}

	@Test
	public void testParseLL1DoesNotThrow()
	{
		Assertions.assertDoesNotThrow
			(
				() ->
				{
					testParseLL1('a');
				}
			);
	}

	@Test
	public void testParseLL1ThrowsExceptionParserInput()
	{
		Assertions.assertThrows
			(
				ExceptionParserInput.class,
				() ->
				{
					testParseLL1('b');
				}
			);
	}

	@Test
	public void testParseLR()
	{
		MockInputStackReverse mockInputStack = mock(MockInputStackReverse.class);
		when(mockInputStack.peek()).thenReturn(new Terminal<>('a'));
		when(mockInputStack.getRange()).thenReturn(new Range(0, 1));
		MockStack mockStack = mock(MockStack.class);
		Assertions.assertEquals(RULE_TERMINAL_A.getTarget(), RULE_TERMINAL_A.parseLR(mockInputStack, mockStack).getTarget());
		verify(mockStack, times(1)).pop();
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("Rule(#0->a)", RULE_TERMINAL_A.toString());
	}
}