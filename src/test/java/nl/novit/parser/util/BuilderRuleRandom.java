package nl.novit.parser.util;

import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class BuilderRuleRandom
	extends BuilderRule<Character, List<Character>>
{
	public static final Random RANDOM = new Random(System.currentTimeMillis());
	public final int iterations;
	public final List<Rule<Character, List<Character>>> rulesStart;
	public final int maximumSizeUnion;
	public final boolean recursive;

	public BuilderRuleRandom
		(
			int iterations,
			List<Character> characters,
			boolean includeEmpty,
			int maximumSizeUnion,
			boolean recursive
		)
	{
		this.iterations = iterations;
		this.rulesStart = characters.stream().map(character -> list(terminal(character))).collect(Collectors.toList());
		if (includeEmpty)
		{
			this.rulesStart.add(emptyList());
		}
		this.maximumSizeUnion = maximumSizeUnion;
		this.recursive = recursive;
	}

	public Rule<Character, List<Character>> pick(final List<Rule<Character, List<Character>>> rules)
	{
		return rules.get(RANDOM.nextInt(rules.size()));
	}

	public Rule<Character, List<Character>> createRuleUnion(final List<Rule<Character, List<Character>>> rules)
	{
		RuleUnion<Character, List<Character>> result = new RuleUnion<>();
		int size = RANDOM.nextInt(this.maximumSizeUnion);
		if (this.recursive && RANDOM.nextBoolean())
		{
			result.add(result);
			size -= 1;
		}
		for (int index = 0; index <= size; index++)
		{
			result.add(pick(rules));
		}
		return result;
	}

	public Rule<Character, List<Character>> createRuleConcatenation(final List<Rule<Character, List<Character>>> rules)
	{
		Rule<Character, List<Character>> ruleLeft = pick(rules);
		Rule<Character, List<Character>> ruleRight = pick(rules);
		return concatenationListList(ruleLeft, ruleRight);
	}

	public Rule<Character, List<Character>> createRule(final List<Rule<Character, List<Character>>> rules)
	{
		return RANDOM.nextBoolean() ? createRuleUnion(rules) : createRuleConcatenation(rules);
	}

	@Override
	public Rule<Character, List<Character>> rule()
	{
		final List<Rule<Character, List<Character>>> rules = new ArrayList<>(this.rulesStart);
		for (int iteration = 0; iteration < this.iterations; iteration++)
		{
			rules.add(createRule(rules));
		}
		return rules.get(rules.size() - 1);
	}
}