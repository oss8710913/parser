package nl.novit.parser.util;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.common.Range;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.mock.MockParser;
import nl.novit.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

public class TestParserString
{
	@Test
	public void testParse()
		throws ExceptionParserInput
	{
		String input = "";
		ParseTree<? extends List<Character>> expected = new ParseTree<>(null, new Range(0, 0));
		MockParser parser = Mockito.mock(MockParser.class);
		Mockito.doReturn(expected).when(parser).parse(Util.convert(input));
		Assertions.assertEquals(expected, new StringParserDefault<>(parser).parse(input));
	}
}