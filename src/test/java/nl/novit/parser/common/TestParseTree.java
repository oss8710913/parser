package nl.novit.parser.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

public class TestParseTree
{
	@Test
	public void testConvertIndex()
	{
		Assertions.assertEquals(0, ParseTree.convertIndex(null, 0));
		Assertions.assertEquals(0, ParseTree.convertIndex(List.of(new Range(1, 2), new Range(2, 4)), 0));
		Assertions.assertEquals(2, ParseTree.convertIndex(List.of(new Range(1, 2), new Range(2, 4)), 1));
		Assertions.assertEquals(4, ParseTree.convertIndex(List.of(new Range(1, 2), new Range(2, 4)), 2));
	}

	@Test
	public void testConvertTree()
	{
		Assertions.assertEquals(List.of(), ParseTree.convertTree(new ParseTree<>(List.of(), new Range(1, 3))));
		Assertions.assertEquals(List.of(new Range(1, 3)), ParseTree.convertTree(new ParseTree<>(List.of('a'), new Range(1, 3))));
		Assertions.assertEquals(List.of(new Range(1, 5), new Range(1, 5)), ParseTree.convertTree(new ParseTree<>(List.of('a', 'b'), new Range(1, 5))));
		Assertions.assertEquals
			(
				List.of(new Range(1, 5), new Range(1, 5)),
				ParseTree.convertTree
					(
						new ParseTree<>
							(
								List.of('a', 'b'),
								List.of
									(
										new ParseTree<>('a', new Range(1, 3))
									),
								new Range(1, 5)
							)
					)
			);
		Assertions.assertEquals
			(
				List.of(new Range(1, 3), new Range(3, 5)),
				ParseTree.convertTree
					(
						new ParseTree<>
							(
								List.of('a', 'b'),
								List.of
									(
										new ParseTree<>('a', new Range(1, 3)),
										new ParseTree<>('b', new Range(3, 5))
									),
								new Range(1, 5)
							)
					)
			);
		Assertions.assertEquals
			(
				List.of(new Range(1, 3), new Range(3, 5), new Range(5, 6)),
				ParseTree.convertTree
					(
						new ParseTree<>
							(
								List.of('a', 'b', 'c'),
								List.of
									(
										new ParseTree<>('a', new Range(1, 3)),
										new ParseTree<>
											(
												List.of('b', 'c'),
												List.of
													(
														new ParseTree<>('b', new Range(3, 5)),
														new ParseTree<>('c', new Range(5, 6))
													),
												new Range(3, 6)
											)
									),
								new Range(1, 6)
							)
					)
			);
	}

	@Test
	public void testEquals()
	{
		Assertions.assertTrue
			(
				new ParseTree<>
					(
						'a',
						Collections.emptyList(),
						new Range(1, 3)
					)
					.equals
						(
							new ParseTree<>
								(
									'a',
									Collections.emptyList(),
									new Range(1, 3)
								)
						)
			);
		Assertions.assertTrue
			(
				new ParseTree<>
					(
						'a',
						List.of
							(
								new ParseTree<>
									(
										'a',
										Collections.emptyList(),
										new Range(1, 2)
									),
								new ParseTree<>
									(
										'a',
										Collections.emptyList(),
										new Range(2, 3)
									)
							),
						new Range(1, 3)
					)
					.equals
						(
							new ParseTree<>
								(
									'a',
									List.of
										(
											new ParseTree<>
												(
													'a',
													Collections.emptyList(),
													new Range(1, 2)
												),
											new ParseTree<>
												(
													'a',
													Collections.emptyList(),
													new Range(2, 3)
												)
										),
									new Range(1, 3)
								)
						)
			);
		Assertions.assertFalse
			(
				new ParseTree<>
					(
						'a',
						Collections.emptyList(),
						new Range(1, 3)
					)
					.equals
						(
							new ParseTree<>
								(
									'b',
									Collections.emptyList(),
									new Range(1, 3)
								)
						)
			);
		Assertions.assertFalse
			(
				new ParseTree<>
					(
						'a',
						List.of
							(
								new ParseTree<>
									(
										'a',
										Collections.emptyList(),
										new Range(1, 2)
									),
								new ParseTree<>
									(
										'a',
										Collections.emptyList(),
										new Range(2, 3)
									)
							),
						new Range(1, 3)
					)
					.equals
						(
							new ParseTree<>
								(
									'a',
									List.of
										(
											new ParseTree<>
												(
													'a',
													Collections.emptyList(),
													new Range(1, 3)
												)
										),
									new Range(1, 3)
								)
						)
			);
		Assertions.assertFalse
			(
				new ParseTree<>
					(
						'a',
						Collections.emptyList(),
						new Range(1, 3)
					)
					.equals
						(
							new ParseTree<>
								(
									'a',
									Collections.emptyList(),
									new Range(1, 2)
								)
						)
			);
		Assertions.assertFalse
			(
				new ParseTree<>
					(
						'a',
						List.of
							(
								new ParseTree<>
									(
										'a',
										Collections.emptyList(),
										new Range(1, 2)
									),
								new ParseTree<>
									(
										'a',
										Collections.emptyList(),
										new Range(2, 3)
									)
							),
						new Range(1, 3)
					)
					.equals
						(
							new ParseTree<>
								(
									'a',
									List.of
										(
											new ParseTree<>
												(
													'a',
													Collections.emptyList(),
													new Range(1, 2)
												),
											new ParseTree<>
												(
													'b',
													Collections.emptyList(),
													new Range(2, 3)
												)
										),
									new Range(1, 3)
								)
						)
			);
	}

	@Test
	public void testConvertRange()
	{
		Assertions.assertEquals(new ParseTree<>(null, new Range(0, 0)), new ParseTree<>(null, new Range(0, 0)).convertRange(List.of()));
		Assertions.assertEquals
			(
				new ParseTree<>('a', new Range(0, 2)),
				new ParseTree<>('a', new Range(0, 1)).convertRange(List.of(new Range(0, 2)))
			);
		Assertions.assertEquals
			(
				new ParseTree<>('a', new Range(0, 5)),
				new ParseTree<>('a', new Range(0, 2)).convertRange(List.of(new Range(0, 2), new Range(2, 5)))
			);
		Assertions.assertEquals
			(
				new ParseTree<>
					(
						"abc",
						List.of
							(
								new ParseTree<>("a", new Range(0, 1)),
								new ParseTree<>
									(
										"bc",
										List.of
											(
												new ParseTree<>("b", new Range(1, 2)),
												new ParseTree<>("c", new Range(2, 4))
											),
										new Range(1, 4)
									)
							),
						new Range(0, 4)
					),
				new ParseTree<>
					(
						"abc",
						List.of
							(
								new ParseTree<>("a", new Range(0, 1)),
								new ParseTree<>
									(
										"bc",
										List.of
											(
												new ParseTree<>("b", new Range(1, 2)),
												new ParseTree<>("c", new Range(2, 4))
											),
										new Range(1, 4)
									)
							),
						new Range(0, 4)
					)
					.convertRange(List.of(new Range(0, 1), new Range(1, 2), new Range(2, 3), new Range(3, 4)))
			);
		Assertions.assertEquals
			(
				new ParseTree<>
					(
						"abc",
						List.of
							(
								new ParseTree<>("a", new Range(0, 2)),
								new ParseTree<>
									(
										"bc",
										List.of
											(
												new ParseTree<>("b", new Range(2, 4)),
												new ParseTree<>("c", new Range(4, 11))
											),
										new Range(2, 11)
									)
							),
						new Range(0, 11)
					),
				new ParseTree<>
					(
						"abc",
						List.of
							(
								new ParseTree<>("a", new Range(0, 1)),
								new ParseTree<>
									(
										"bc",
										List.of
											(
												new ParseTree<>("b", new Range(1, 2)),
												new ParseTree<>("c", new Range(2, 4))
											),
										new Range(1, 4)
									)
							),
						new Range(0, 4)
					)
					.convertRange(List.of(new Range(0, 2), new Range(2, 4), new Range(4, 7), new Range(7, 11)))
			);
	}

	@Test
	public void testFindRange()
	{
		String a = "a";
		String b = "b";
		String c = "c";
		String d = "d";
		String x = "x";
		String abcd = "abcd";
		String bcd = "bcd";
		String cd = "cd";
		ParseTree<?> parseTree =
			new ParseTree<>
				(
					abcd,
					List.of
						(
							new ParseTree<>(a, new Range(0, 2)),
							new ParseTree<>
								(
									bcd,
									List.of
										(
											new ParseTree<>(b, new Range(2, 4)),
											new ParseTree<>
												(
													cd,
													List.of
														(
															new ParseTree<>(c, new Range(4, 8)),
															new ParseTree<>(d, new Range(8, 11))
														),
													new Range(4, 11)
												)
										),
									new Range(2, 11)
								)
						),
					new Range(0, 11)
				);
		Assertions.assertEquals(new Range(0, 2), parseTree.find(a).getRange());
		Assertions.assertEquals(new Range(2, 4), parseTree.find(b).getRange());
		Assertions.assertEquals(new Range(4, 8), parseTree.find(c).getRange());
		Assertions.assertEquals(new Range(8, 11), parseTree.find(d).getRange());
		Assertions.assertEquals(new Range(0, 11), parseTree.find(abcd).getRange());
		Assertions.assertEquals(new Range(2, 11), parseTree.find(bcd).getRange());
		Assertions.assertEquals(new Range(4, 11), parseTree.find(cd).getRange());
		Assertions.assertNull(parseTree.find(x));
	}
}