package nl.novit.parser.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestRange
{
	public void testStart(Integer expected, Integer end)
	{
		Assertions.assertEquals(expected, new Range(expected, end).start());
	}

	@Test
	public void testStart()
	{
		testStart(0, 0);
		testStart(0, 1);
		testStart(0, 1);
	}

	public void testEnd(Integer start, Integer expected)
	{
		Assertions.assertEquals(expected, new Range(start, expected).end());
	}

	@Test
	public void testEnd()
	{
		testEnd(0, 0);
		testEnd(0, 1);
		testEnd(1, 1);
	}

	@Test
	public void testIsEmpty()
	{
		Assertions.assertTrue(new Range(0, 0).isEmpty());
		Assertions.assertFalse(new Range(0, 1).isEmpty());
		Assertions.assertTrue(new Range(1, 1).isEmpty());
	}

	@Test
	public void testHead()
	{
		Assertions.assertEquals(new Range(0, 1), new Range(0, 2).head());
		Assertions.assertEquals(new Range(0, 0), new Range(0, 1).head());
	}

	@Test
	public void testTail()
	{
		Assertions.assertEquals(new Range(1, 2), new Range(0, 2).tail());
		Assertions.assertEquals(new Range(2, 2), new Range(1, 2).tail());
	}

	@Test
	public void testSize()
	{
		Assertions.assertEquals(0, new Range(0, 0).size());
		Assertions.assertEquals(1, new Range(0, 1).size());
		Assertions.assertEquals(0, new Range(1, 1).size());
	}
}