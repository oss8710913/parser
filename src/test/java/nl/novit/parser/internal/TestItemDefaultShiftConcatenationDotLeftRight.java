package nl.novit.parser.internal;

import nl.novit.parser.mock.MockRuleConcatenation;
import nl.novit.parser.mock.MockRuleDefault;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestItemDefaultShiftConcatenationDotLeftRight
{
	@Test
	public void testGetActive()
	{
		Assertions.assertEquals(MockRuleDefault.RULE_0, new ItemDefaultShiftConcatenationDotLeftRight<>(MockRuleConcatenation.RULE_0, null).getActive());
	}

	@Test
	public void testClose()
	{
		// TODO
	}
}