package nl.novit.parser.internal;

import nl.novit.parser.common.Range;
import nl.novit.parser.mock.MockInput;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestInput
{
	public static final List<Character> LIST_A = List.of('a');
	public static final List<Character> LIST_AB = List.of('a', 'b');

	@Test
	public void testCompareTo()
	{
		Assertions.assertEquals(-1, new MockInput(LIST_A).compareTo(new MockInput(LIST_AB)));
		Assertions.assertEquals(-1, new MockInput(LIST_AB).compareTo(new MockInput(LIST_AB, new Range(1, 2))));
		Assertions.assertEquals(0, new MockInput(LIST_AB).compareTo(new MockInput(LIST_AB)));
		Assertions.assertEquals(1, new MockInput(LIST_AB).compareTo(new MockInput(LIST_A)));
	}

	@Test
	public void testIsEmpty()
	{
		Assertions.assertFalse(new MockInput(LIST_A).isEmpty());
		Assertions.assertTrue(new MockInput(LIST_A, new Range(1, 1)).isEmpty());
	}
}