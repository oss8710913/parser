package nl.novit.parser.internal;

import nl.novit.parser.mock.MockItemDefaultReduceUnion;
import nl.novit.parser.mock.MockRuleUnion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestItemDefaultReduceUnion
{
	public static final MockItemDefaultReduceUnion ITEM = new MockItemDefaultReduceUnion(MockRuleUnion.RULE_0, MockRuleUnion.RULE_1);

	@Test
	public void testGetReduced()
	{
		Assertions.assertEquals(1, ITEM.getReduced());
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("Item(" + ITEM.getRuleLHS().toStringId() + "->" + ITEM.getRuleRHS().toStringId() + ".)", ITEM.toString());
	}
}