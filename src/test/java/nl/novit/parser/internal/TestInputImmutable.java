package nl.novit.parser.internal;

import nl.novit.parser.common.Range;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TestInputImmutable
{
	@Test
	public void testPeek()
	{
		Assertions.assertEquals(new Empty<>(), new InputImmutable<Character>(new ArrayList<>()).peek());
		Assertions.assertEquals(new Terminal<>('a'), new InputImmutable<>(TestInput.LIST_AB).peek());
		Assertions.assertEquals(new Terminal<>('b'), new InputImmutable<>(TestInput.LIST_AB, new Range(1, 2)).peek());
		Assertions.assertEquals(new Empty<>(), new InputImmutable<>(TestInput.LIST_AB, new Range(2, 2)).peek());
	}

	@Test
	public void testPop()
	{
		InputImmutable<Character> input = new InputImmutable<>(TestInput.LIST_AB);
		Assertions.assertEquals(new Terminal<>('a'), input.peek());
		Assertions.assertEquals(new Terminal<>('b'), input.pop().peek());
		Assertions.assertEquals(new Empty<>(), input.pop().pop().peek());
		Assertions.assertNull(input.pop().pop().pop());
	}
}