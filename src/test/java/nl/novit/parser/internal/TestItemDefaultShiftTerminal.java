package nl.novit.parser.internal;

import nl.novit.parser.mock.MockRuleTerminal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;

public class TestItemDefaultShiftTerminal
{
	public static final ItemDefaultShiftTerminal<Character> ITEM = new ItemDefaultShiftTerminal<>(new MockRuleTerminal<>(TestTerminal.TERMINAL_A.terminal), null);

	@Test
	public void testGetActive()
	{
		Assertions.assertEquals(TestTerminal.TERMINAL_A, ITEM.getActive());
	}

	@Test
	public void testClose()
	{
		Assertions.assertEquals(Collections.emptySet(), ITEM.close(TestEmpty.EMPTY));
		Assertions.assertEquals(Collections.emptySet(), ITEM.close(TestTerminal.TERMINAL_A));
	}
}