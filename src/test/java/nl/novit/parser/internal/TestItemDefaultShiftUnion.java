package nl.novit.parser.internal;

import nl.novit.parser.mock.MockRuleUnion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestItemDefaultShiftUnion
{
	public static final ItemDefaultShiftUnion<Character> ITEM = new ItemDefaultShiftUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_1, null);

	@Test
	public void testGetActive()
	{
		Assertions.assertEquals(MockRuleUnion.RULE_1, ITEM.getActive());
	}

	@Test
	public void testClose()
	{
		// TODO
	}
}