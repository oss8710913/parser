package nl.novit.parser.internal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TestInputStackReverse
{
	@Test
	public void testPeekPop()
	{
		Assertions.assertEquals(new Empty<>(), new InputStackReverse<>(new ArrayList<Character>()).peek());
		InputStackReverse<Character> input = new InputStackReverse<>(TestInput.LIST_AB);
		Assertions.assertEquals(new Terminal<>('b'), input.peek());
		input.pop();
		Assertions.assertEquals(new Terminal<>('a'), input.peek());
		input.pop();
		Assertions.assertEquals(new Empty<>(), input.peek());
	}
}