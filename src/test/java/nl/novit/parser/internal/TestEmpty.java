package nl.novit.parser.internal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.TreeMap;

public class TestEmpty
{
	public static final Empty<Character> EMPTY = new Empty<>();

	@Test
	public void testIsEmpty()
	{
		Assertions.assertTrue(EMPTY.isEmpty());
	}

	@Test
	public void testGetTerminalOrEmpty()
	{
		Assertions.assertEquals(TestTerminal.TERMINAL_A, EMPTY.getTerminalOrEmpty(Map.of(EMPTY, TestTerminal.TERMINAL_A)));
	}

	@Test
	public void testGetRuleOrTerminal()
	{
		Assertions.assertNull(EMPTY.getRuleOrTerminal(new TreeMap<>()));
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("ϵ", EMPTY.toString());
	}
}