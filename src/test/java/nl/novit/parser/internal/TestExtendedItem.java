package nl.novit.parser.internal;

import nl.novit.parser.mock.MockItemDefault;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestExtendedItem
{
	public MockItemDefault item0;
	public MockItemDefault item1;

	@BeforeEach
	public void initialise()
	{
		this.item0 = mock(MockItemDefault.class);
		this.item1 = mock(MockItemDefault.class);
	}

	@Test
	public void testCompareTo1()
	{
		when(this.item0.compareTo(this.item1)).thenReturn(-1);
		Assertions.assertEquals(-1, new ExtendedItem<>(this.item0, TestTerminal.TERMINAL_A).compareTo(new ExtendedItem<>(item1, TestTerminal.TERMINAL_A)));
	}

	@Test
	public void testCompareTo2()
	{
		when(this.item0.compareTo(this.item1)).thenReturn(1);
		Assertions.assertEquals(1, new ExtendedItem<>(this.item0, TestTerminal.TERMINAL_A).compareTo(new ExtendedItem<>(this.item1, TestTerminal.TERMINAL_A)));
	}

	@Test
	public void testCompareTo3()
	{
		when(this.item0.compareTo(this.item1)).thenReturn(0);
		Assertions.assertEquals(-1, new ExtendedItem<>(this.item0, TestTerminal.TERMINAL_A).compareTo(new ExtendedItem<>(this.item1, TestTerminal.TERMINAL_B)));
		Assertions.assertEquals(0, new ExtendedItem<>(this.item0, TestTerminal.TERMINAL_A).compareTo(new ExtendedItem<>(this.item1, TestTerminal.TERMINAL_A)));
		Assertions.assertEquals(1, new ExtendedItem<>(this.item0, TestTerminal.TERMINAL_B).compareTo(new ExtendedItem<>(this.item1, TestTerminal.TERMINAL_A)));
	}

	@Test
	public void testToString()
	{
		when(this.item0.toString()).thenReturn("<item>");
		Assertions.assertEquals("(<item> [a])", new ExtendedItem<>(this.item0, TestTerminal.TERMINAL_A).toString());
	}
}
