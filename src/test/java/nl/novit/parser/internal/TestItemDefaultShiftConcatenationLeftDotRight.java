package nl.novit.parser.internal;

import nl.novit.parser.mock.MockRuleConcatenation;
import nl.novit.parser.mock.MockRuleDefault;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestItemDefaultShiftConcatenationLeftDotRight
{
	@Test
	public void testGetActive()
	{
		Assertions.assertEquals(MockRuleDefault.RULE_1, new ItemDefaultShiftConcatenationLeftDotRight<>(MockRuleConcatenation.RULE_0, null).getActive());
	}

	@Test
	public void testClose()
	{
		// TODO
	}
}