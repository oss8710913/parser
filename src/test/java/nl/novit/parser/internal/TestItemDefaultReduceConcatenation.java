package nl.novit.parser.internal;

import nl.novit.parser.mock.MockItemDefaultReduceConcatenation;
import nl.novit.parser.mock.MockRuleConcatenation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestItemDefaultReduceConcatenation
{
	public static final MockItemDefaultReduceConcatenation ITEM = new MockItemDefaultReduceConcatenation(MockRuleConcatenation.RULE_0);

	@Test
	public void testGetReduced()
	{
		Assertions.assertEquals(2, ITEM.getReduced());
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("Item(" + ITEM.getRuleLHS().toStringId() + "->" + ITEM.getRuleLHS().ruleLeft.toStringId() + ITEM.getRuleLHS().ruleRight.toStringId() + ".)", ITEM.toString());
	}
}