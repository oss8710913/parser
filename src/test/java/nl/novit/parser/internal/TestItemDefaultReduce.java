package nl.novit.parser.internal;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.mock.MockItemDefaultReduce;
import nl.novit.parser.mock.MockState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class TestItemDefaultReduce
{
	public MockItemDefaultReduce item0;
	public MockItemDefaultReduce item1;
	public MockState<?> state;

	@BeforeEach
	public void initialise()
	{
		this.item0 = Mockito.mock(MockItemDefaultReduce.class);
		this.item1 = Mockito.mock(MockItemDefaultReduce.class);
		this.state = Mockito.mock(MockState.class);
		Mockito.when(this.item0.equals(this.item0)).thenReturn(true);
		Mockito.when(this.item0.equals(this.item1)).thenReturn(false);
		Mockito.when(this.item1.equals(this.item0)).thenReturn(false);
		Mockito.when(this.item1.equals(this.item1)).thenReturn(true);
		Mockito.when(this.item0.close(TestTerminal.TERMINAL_A)).thenCallRealMethod();
		Mockito.when(this.item0.add(this.state, TestTerminal.TERMINAL_A)).thenCallRealMethod();
	}

	@Test
	public void testClose()
	{
		Assertions.assertEquals(Collections.emptySet(), this.item0.close(TestEmpty.EMPTY));
		Assertions.assertEquals(Collections.emptySet(), this.item0.close(TestTerminal.TERMINAL_A));
	}

	@Test
	public void testAddTrue()
	{
		Map<TerminalOrEmpty<Character>, ItemDefaultReduce<Character, ?>> reduce = new TreeMap<>();
		Mockito.when(this.state.getReduce()).thenReturn(reduce);
		Mockito.when(this.state.getShift()).thenReturn(Collections.emptyMap());
		Assertions.assertTrue(this.item0.add(state, TestTerminal.TERMINAL_A));
		Assertions.assertEquals(Map.of(TestTerminal.TERMINAL_A, this.item0), reduce);
	}

	@Test
	public void testAddFalse()
	{
		Mockito.when(this.state.getReduce()).thenReturn(Map.of(TestTerminal.TERMINAL_A, this.item0));
		Mockito.when(this.state.getShift()).thenReturn(Collections.emptyMap());
		Assertions.assertFalse(this.item0.add(this.state, TestTerminal.TERMINAL_A));
	}

	@Test
	public void testAddConflictShiftReduce()
	{
		Assertions.assertThrows(ExceptionParserAmbiguous.class, () ->
		{
			Mockito.when(this.state.getReduce()).thenReturn(new TreeMap<>());
			Mockito.when(this.state.getShift()).thenReturn(Map.of(TestTerminal.TERMINAL_A, Collections.emptySet()));
			this.item0.add(state, TestTerminal.TERMINAL_A);
		});
	}

	@Test
	public void testAddConflictReduceReduce()
	{
		Assertions.assertThrows(ExceptionParserAmbiguous.class, () ->
		{
			Mockito.when(this.state.getReduce()).thenReturn(Map.of(TestTerminal.TERMINAL_A, this.item1));
			Mockito.when(this.state.getShift()).thenReturn(Collections.emptyMap());
			this.item0.add(this.state, TestTerminal.TERMINAL_A);
		});
	}
}