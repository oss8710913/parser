package nl.novit.parser.internal;

import nl.novit.parser.mock.MockRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

public class TestStackRuleOrTerminalOrEmpty
{
	@Test
	public void test()
	{
		Assertions.assertEquals(0, new StackRuleOrTerminalOrEmpty<Character>(Collections.emptyList()).size());
		StackRuleOrTerminalOrEmpty<Character> stack = new StackRuleOrTerminalOrEmpty<>(List.of('a', 'b', 'c'));
		Assertions.assertEquals(3, stack.size());
		Assertions.assertEquals(3, stack.sizeTerminals());
		Assertions.assertEquals(new Terminal<>('a'), stack.peek());
		Assertions.assertEquals(new Terminal<>('a'), stack.pop());
		Assertions.assertEquals(2, stack.sizeTerminals());
		stack.push(Mockito.mock(MockRule.class));
		Assertions.assertEquals(2, stack.sizeTerminals());
		Assertions.assertEquals(new Empty<>(), stack.peek());
		Assertions.assertEquals(new Empty<>(), stack.pop());
		Assertions.assertEquals(new Terminal<>('b'), stack.peek());
		Assertions.assertEquals(new Terminal<>('b'), stack.pop());
		Assertions.assertEquals(new Terminal<>('c'), stack.peek());
		Assertions.assertEquals(new Terminal<>('c'), stack.pop());
		Assertions.assertEquals(new Empty<>(), stack.peek());
		Assertions.assertEquals(new Empty<>(), stack.pop());
		Assertions.assertEquals(new Empty<>(), stack.pop());
	}
}