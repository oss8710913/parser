package nl.novit.parser.internal;

import nl.novit.parser.mock.MockItemDefault;
import nl.novit.parser.mock.MockRuleDefault;
import nl.novit.parser.mock.MockRuleUnion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestItemDefault
{
	@Test
	public void testCompareToLHS()
	{
		Assertions.assertEquals(MockRuleUnion.RULE_0.compareTo(MockRuleUnion.RULE_1), ItemDefault.compareToLHS(new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_0), new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_1, MockRuleUnion.RULE_0)));
		Assertions.assertEquals(MockRuleUnion.RULE_0.compareTo(MockRuleUnion.RULE_0), ItemDefault.compareToLHS(new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_0), new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_1)));
		Assertions.assertEquals(MockRuleUnion.RULE_1.compareTo(MockRuleUnion.RULE_0), ItemDefault.compareToLHS(new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_1, MockRuleUnion.RULE_0), new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_0)));
	}

	@Test
	public void testCompareToLHSRHS()
	{
		Assertions.assertEquals(MockRuleUnion.RULE_0.compareTo(MockRuleUnion.RULE_1), ItemDefault.compareToLHSRHS(new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_0), new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_1, MockRuleUnion.RULE_0)));
		Assertions.assertEquals(MockRuleUnion.RULE_0.compareTo(MockRuleUnion.RULE_1), ItemDefault.compareToLHSRHS(new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_0), new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_1)));
		Assertions.assertEquals(MockRuleUnion.RULE_0.compareTo(MockRuleUnion.RULE_0), ItemDefault.compareToLHSRHS(new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_0), new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_0)));
		Assertions.assertEquals(MockRuleUnion.RULE_1.compareTo(MockRuleUnion.RULE_0), ItemDefault.compareToLHSRHS(new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_1), new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_0)));
		Assertions.assertEquals(MockRuleUnion.RULE_1.compareTo(MockRuleUnion.RULE_0), ItemDefault.compareToLHSRHS(new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_1, MockRuleUnion.RULE_0), new ItemDefaultReduceUnion<>(MockRuleUnion.RULE_0, MockRuleUnion.RULE_0)));
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("Item(" + MockRuleDefault.RULE_0.toStringId() + "->", MockItemDefault.ITEM.toString());
	}
}