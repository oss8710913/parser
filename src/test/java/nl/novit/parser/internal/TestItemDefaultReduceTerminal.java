package nl.novit.parser.internal;

import nl.novit.parser.mock.MockItemDefaultReduceTerminal;
import nl.novit.parser.mock.MockRuleTerminal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestItemDefaultReduceTerminal
{
	public static final MockItemDefaultReduceTerminal ITEM = new MockItemDefaultReduceTerminal(MockRuleTerminal.RULE_A);

	@Test
	public void testGetReduced()
	{
		Assertions.assertEquals(1, ITEM.getReduced());
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("Item(" + ITEM.getRuleLHS().toStringId() + "->" + ITEM.getRuleLHS().terminal + ".)", ITEM.toString());
	}
}