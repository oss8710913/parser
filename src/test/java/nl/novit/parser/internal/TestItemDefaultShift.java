package nl.novit.parser.internal;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.mock.MockItemDefaultReduce;
import nl.novit.parser.mock.MockItemDefaultShift;
import nl.novit.parser.mock.MockState;
import nl.novit.util.UtilCollections;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class TestItemDefaultShift
{
	public MockItemDefaultShift item;
	public ExtendedItem<Character, ItemDefaultShift<Character, ?>> extendedItem;
	public MockState<?> state;

	@BeforeEach
	public void initialise()
	{
		this.item = Mockito.mock(MockItemDefaultShift.class);
		this.extendedItem = new ExtendedItem<>(this.item, TestTerminal.TERMINAL_A);
		this.state = Mockito.mock(MockState.class);
		Mockito.when(this.item.add(this.state, TestTerminal.TERMINAL_A)).thenCallRealMethod();
	}

	@Test
	public void testAddTrue()
	{
		Mockito.when(this.item.getActive()).thenReturn(TestTerminal.TERMINAL_A);
		Mockito.when(this.state.getReduce()).thenReturn(Collections.emptyMap());
		Set<ExtendedItem<Character, ItemDefaultShift<Character, ?>>> extendedItems = new TreeSet<>();
		Mockito.when(this.state.getShift()).thenReturn(UtilCollections.mapOf(TestTerminal.TERMINAL_A, extendedItems));
		Assertions.assertTrue(this.item.add(this.state, TestTerminal.TERMINAL_A));
		Assertions.assertEquals(Set.of(this.extendedItem), extendedItems);
	}

	@Test
	public void testAddFalse()
	{
		Mockito.when(this.item.getActive()).thenReturn(TestTerminal.TERMINAL_A);
		Mockito.when(this.state.getReduce()).thenReturn(Collections.emptyMap());
		Set<ExtendedItem<Character, ItemDefaultShift<Character, ?>>> extendedItems = new TreeSet<>();
		extendedItems.add(this.extendedItem);
		Mockito.when(this.state.getShift()).thenReturn(UtilCollections.mapOf(TestTerminal.TERMINAL_A, extendedItems));
		Assertions.assertFalse(this.item.add(this.state, TestTerminal.TERMINAL_A));
	}

	@Test
	public void testAddConflictShiftReduce()
	{
		Assertions.assertThrows(ExceptionParserAmbiguous.class, () ->
		{
			Mockito.when(this.item.getActive()).thenReturn(TestTerminal.TERMINAL_A);
			Mockito.when(this.state.getReduce()).thenReturn(Map.of(TestTerminal.TERMINAL_A, Mockito.mock(MockItemDefaultReduce.class)));
			this.item.add(state, TestTerminal.TERMINAL_A);
		});
	}
}
