package nl.novit.parser.internal;

import nl.novit.parser.mock.MockItemDefaultReduceEmpty;
import nl.novit.parser.mock.MockRuleEmpty;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestItemDefaultReduceEmpty
{
	public static final MockItemDefaultReduceEmpty ITEM = new MockItemDefaultReduceEmpty(MockRuleEmpty.RULE_0);

	@Test
	public void testGetReduced()
	{
		Assertions.assertEquals(0, ITEM.getReduced());
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("Item(" + ITEM.getRuleLHS().toStringId() + "->ϵ.)", ITEM.toString());
	}
}