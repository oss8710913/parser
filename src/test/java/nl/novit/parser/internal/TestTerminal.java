package nl.novit.parser.internal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class TestTerminal
{
	public static final Terminal<Character> TERMINAL_A = new Terminal<>('a');
	public static final Terminal<Character> TERMINAL_B = new Terminal<>('b');

	@Test
	public void testIsEmpty()
	{
		Assertions.assertFalse(TERMINAL_A.isEmpty());
	}

	@Test
	public void testGetTerminalOrEmpty()
	{
		Assertions.assertEquals('a', TERMINAL_A.getTerminalOrEmpty(Map.of(TERMINAL_A, 'a')));
		Assertions.assertEquals('a', TERMINAL_A.getTerminalOrEmpty(Map.of(TERMINAL_A, 'a', TERMINAL_B, 'b')));
		Assertions.assertEquals('b', TERMINAL_B.getTerminalOrEmpty(Map.of(TERMINAL_A, 'a', TERMINAL_B, 'b')));
	}

	@Test
	public void testGetRuleOrTerminal()
	{
		Assertions.assertEquals('a', TERMINAL_A.getRuleOrTerminal(Map.of(TERMINAL_A, 'a')));
		Assertions.assertEquals('a', TERMINAL_A.getRuleOrTerminal(Map.of(TERMINAL_A, 'a', TERMINAL_B, 'b')));
		Assertions.assertEquals('b', TERMINAL_B.getRuleOrTerminal(Map.of(TERMINAL_A, 'a', TERMINAL_B, 'b')));
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals(TERMINAL_A.terminal.toString(), TERMINAL_A.toString());
		Assertions.assertEquals(TERMINAL_B.terminal.toString(), TERMINAL_B.toString());
	}
}