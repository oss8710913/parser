package nl.novit.parser.internal;

import nl.novit.parser.mock.MockRuleDefault;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestRuleOrTerminalOrEmpty
{
	public static RuleOrTerminalOrEmpty<Character> EMPTY = TestEmpty.EMPTY;
	public static RuleOrTerminalOrEmpty<Character> TERMINAL_A = TestTerminal.TERMINAL_A;
	public static RuleOrTerminalOrEmpty<Character> TERMINAL_B = TestTerminal.TERMINAL_B;
	public static RuleOrTerminalOrEmpty<Character> RULE_0 = MockRuleDefault.RULE_0;
	public static RuleOrTerminalOrEmpty<Character> RULE_1 = MockRuleDefault.RULE_1;

	@Test
	public void testCompareTo()
	{
		Assertions.assertEquals(0, EMPTY.compareTo(EMPTY));
		Assertions.assertEquals(-1, EMPTY.compareTo(TERMINAL_A));
		Assertions.assertEquals(-1, EMPTY.compareTo(TERMINAL_B));
		Assertions.assertEquals(-1, EMPTY.compareTo(RULE_0));
		Assertions.assertEquals(-1, EMPTY.compareTo(RULE_1));
		Assertions.assertEquals(1, TERMINAL_A.compareTo(EMPTY));
		Assertions.assertEquals(0, TERMINAL_A.compareTo(TERMINAL_A));
		Assertions.assertEquals(-1, TERMINAL_A.compareTo(TERMINAL_B));
		Assertions.assertEquals(-1, TERMINAL_A.compareTo(RULE_0));
		Assertions.assertEquals(-1, TERMINAL_A.compareTo(RULE_1));
		Assertions.assertEquals(1, TERMINAL_B.compareTo(EMPTY));
		Assertions.assertEquals(1, TERMINAL_B.compareTo(TERMINAL_A));
		Assertions.assertEquals(0, TERMINAL_B.compareTo(TERMINAL_B));
		Assertions.assertEquals(-1, TERMINAL_B.compareTo(RULE_0));
		Assertions.assertEquals(-1, TERMINAL_B.compareTo(RULE_1));
		Assertions.assertEquals(1, RULE_0.compareTo(EMPTY));
		Assertions.assertEquals(1, RULE_0.compareTo(TERMINAL_A));
		Assertions.assertEquals(1, RULE_0.compareTo(TERMINAL_B));
		Assertions.assertEquals(0, RULE_0.compareTo(RULE_0));
		Assertions.assertEquals(-1, RULE_0.compareTo(RULE_1));
		Assertions.assertEquals(1, RULE_1.compareTo(EMPTY));
		Assertions.assertEquals(1, RULE_1.compareTo(TERMINAL_A));
		Assertions.assertEquals(1, RULE_1.compareTo(TERMINAL_B));
		Assertions.assertEquals(1, RULE_1.compareTo(RULE_0));
		Assertions.assertEquals(0, RULE_1.compareTo(RULE_1));
	}
}