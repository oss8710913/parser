package nl.novit.parser.parser;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.rule.Rule;
import nl.novit.util.Wrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

public class TestParserDefault
	extends Tests
{
	@Test
	public void testParserDefault()
	{
		Wrapper<Rule<Character, List<Character>>> actual = new Wrapper<>();
		ParserDefault<Character, List<Character>> parser = new ParserDefault<>(this.rule)
		{
			@Override
			public ParseTree<? extends List<Character>> parse(List<Character> input)
				throws ExceptionParserInput
			{
				return null;
			}

			@Override
			public void preparse(Rule<Character, List<Character>> ruleStart)
				throws ExceptionParserAmbiguous
			{
				actual.value = ruleStart;
			}
		};
		Assertions.assertEquals(this.rule, parser.ruleStart);
		Mockito.verify(rule, Mockito.atLeastOnce()).initializeFirstTerminals();
		Assertions.assertEquals(this.rule, actual.value);
		Assertions.assertEquals(this.rule, parser.getRuleStart());
	}
}