package nl.novit.parser.parser;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.InputStackReverse;
import nl.novit.parser.internal.StackRuleOrTerminalOrEmpty;
import nl.novit.parser.internal.Terminal;
import nl.novit.parser.internal.TestEmpty;
import nl.novit.parser.mock.MockParserLALR1;
import nl.novit.parser.mock.MockState;
import nl.novit.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class TestParserLALR1
	extends Tests
{
	public MockState<?> state;
	public MockParserLALR1<?> parser;

	@BeforeEach
	public void initialise()
		throws ExceptionParserInput
	{
		super.initialise();
		this.state = Mockito.mock(MockState.class);
		this.parser = Mockito.mock(MockParserLALR1.class);
		Mockito.when(this.parser.parseToRuleStack(Mockito.any())).thenCallRealMethod();
		Mockito.when(this.parser.parse(Mockito.any())).thenCallRealMethod();
		Mockito.when(this.parser.getRuleStart()).thenReturn(this.rule);
		Mockito.doReturn(this.state).when(this.parser).getState();
	}

	@Test
	public void testParseStackSize3()
		throws ExceptionParserInput
	{
		List<Character> input = Util.convert("xxxx");
		Mockito.when(this.rule.equals(new Terminal<>('x'))).thenReturn(true);
		Mockito.doReturn(EXPECTED).when(this.rule).parseLR(new InputStackReverse<>(input), new Stack<>());
		Mockito.doAnswer
			(
				(i) ->
				{
					((Stack<?>) i.getArgument(0)).pop();
					return 0;
				}
			).when(this.state).parse(Mockito.any(), Mockito.any(), Mockito.anyInt());
		Assertions.assertEquals(EXPECTED, this.parser.parse(input));
		Mockito.verify(this.state, Mockito.times(3)).parse(Mockito.any(), Mockito.any(), Mockito.anyInt());
	}

	@Test
	public void testParseEquals1()
		throws ExceptionParserInput
	{
		List<Character> input = Util.convert("x");
		Mockito.when(this.rule.equals(new Terminal<>('x'))).thenReturn(false, true);
		Mockito.doReturn(EXPECTED).when(this.rule).parseLR(new InputStackReverse<>(input), new Stack<>());
		Assertions.assertEquals(EXPECTED, this.parser.parse(input));
		Mockito.verify(this.state, Mockito.times(1)).parse(new StackRuleOrTerminalOrEmpty<>(input), new Stack<>(), 1);
	}

	@Test
	public void testParseEquals2()
		throws ExceptionParserInput
	{
		List<Character> input = Util.convert("x");
		Mockito.when(this.rule.equals(new Terminal<>('x'))).thenReturn(false, false, true);
		Mockito.doReturn(EXPECTED).when(this.rule).parseLR(new InputStackReverse<>(input), new Stack<>());
		Assertions.assertEquals(EXPECTED, this.parser.parse(input));
		Mockito.verify(this.state, Mockito.times(2)).parse(new StackRuleOrTerminalOrEmpty<>(input), new Stack<>(), 1);
	}

	@Test
	public void testPreparse()
	{
		Mockito.when(this.rule.extendedItems(TestEmpty.EMPTY)).thenReturn(Collections.emptySet());
		Mockito.doCallRealMethod().when(this.parser).preparse(this.rule);
		Mockito.doReturn(this.state).when(this.parser).getState(Collections.emptySet());
		this.parser.preparse(this.rule);
		Assertions.assertEquals(this.state, this.parser.state);
//		Mockito.verify(this.state, Mockito.atLeastOnce()).addOrMerge(Collections.emptyMap(), Collections.emptySet()); // TODO
	}
}