package nl.novit.parser.parser;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.InputImmutable;
import nl.novit.util.Util;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

public class TestParserUnger
	extends Tests
{
	@Test
	public void testParse()
		throws ExceptionParserInput
	{
		List<Character> input = Util.convert("a");
		new ParserUnger<>(this.rule).parse(input);
		Mockito.verify(this.rule, Mockito.atLeastOnce()).parseUnger(new InputImmutable<>(input), Collections.emptySet());
	}
}