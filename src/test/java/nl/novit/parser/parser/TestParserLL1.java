package nl.novit.parser.parser;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.InputStackForward;
import nl.novit.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Set;
import java.util.TreeMap;

public class TestParserLL1
	extends Tests
{
	@Test
	public void testParse()
		throws ExceptionParserInput
	{
		List<Character> input = Util.convert("");
		Mockito.doReturn(EXPECTED).when(this.rule).parseLL1(new InputStackForward<>(input));
		ParserLL1<Character, List<Character>> parser = new ParserLL1<>(this.rule);
		Assertions.assertEquals(EXPECTED, parser.parse(input));
	}

	@Test
	public void testParseExceptionParserInput()
		throws ExceptionParserInput
	{
		Assertions.assertThrows
			(
				ExceptionParserInput.class,
				() ->
				{
					List<Character> input = Util.convert("x");
					new ParserLL1<>(this.rule).parse(input);
				}
			);
	}

	@Test
	public void testPreparse()
	{
		new ParserLL1<>(this.rule);
		Mockito.verify(this.rule, Mockito.atLeastOnce()).checkLL1(new TreeMap<>(), Set.of(new Empty<>()));
	}
}