package nl.novit.parser.parser;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.common.Range;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.mock.MockRule;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class Tests
{
	public final ParseTree<? extends List<Character>> EXPECTED = new ParseTree<>(new ArrayList<>(), new Range(0, 1));
	public MockRule rule;

	@BeforeEach
	public void initialise()
		throws ExceptionParserInput
	{
		this.rule = Mockito.mock(MockRule.class);
	}
}
