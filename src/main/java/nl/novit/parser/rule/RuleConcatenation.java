package nl.novit.parser.rule;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.common.Range;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.Input;
import nl.novit.parser.internal.InputImmutable;
import nl.novit.parser.internal.InputStackForward;
import nl.novit.parser.internal.InputStackReverse;
import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.ItemDefaultReduceConcatenation;
import nl.novit.parser.internal.ItemDefaultShiftConcatenationDotLeftRight;
import nl.novit.parser.internal.ItemDefaultShiftConcatenationLeftDotRight;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.util.Tuple2Comparable;
import nl.novit.util.Util;
import nl.novit.util.UtilCollections;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

public abstract class RuleConcatenation<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget, TypeSourceLeft, TypeSourceRight>
	extends RuleDefault<TypeTerminal, TypeTarget>
{
	public static final System.Logger LOGGER = System.getLogger(RuleConcatenation.class.getCanonicalName());
	public final Rule<TypeTerminal, TypeSourceLeft> ruleLeft;
	public final Rule<TypeTerminal, TypeSourceRight> ruleRight;

	public RuleConcatenation(Rule<TypeTerminal, TypeSourceLeft> ruleLeft, Rule<TypeTerminal, TypeSourceRight> ruleRight)
	{
		this.ruleLeft = ruleLeft;
		this.ruleRight = ruleRight;
		LOGGER.log(System.Logger.Level.DEBUG, toString());
	}

	public abstract TypeTarget getTarget(TypeSourceLeft left, TypeSourceRight right);

	public ParseTree<? extends TypeTarget> parseTree(ParseTree<? extends TypeSourceLeft> left, ParseTree<? extends TypeSourceRight> right)
	{
		return new ParseTree<>(getTarget(left.getTarget(), right.getTarget()), List.of(left, right));
	}

	@Override
	public ParseTree<? extends TypeTarget> parseUnger
		(
			InputImmutable<TypeTerminal> input,
			Set<Tuple2Comparable<Rule<TypeTerminal, ?>, Input<TypeTerminal>>> visited
		)
		throws ExceptionParserInput
	{
		ParseTree<? extends TypeTarget> result = null;
		ExceptionParserInput exception = new ExceptionParserInput(input.getRange().start());
		for (int index = input.getRange().start(); exception != null && index <= input.getRange().end(); index++)
		{
			try
			{
				ParseTree<? extends TypeSourceLeft> left = this.ruleLeft.parseUnger(new InputImmutable<>(input, new Range(input.getRange().start(), index)), visited);
				ParseTree<? extends TypeSourceRight> right = this.ruleRight.parseUnger(new InputImmutable<>(input, new Range(index, input.getRange().end())), visited);
				exception = null;
				result = parseTree(left, right);
			}
			catch (ExceptionParserInput exceptionNext)
			{
				if (exception.index < exceptionNext.index)
				{
					exception = exceptionNext;
				}
			}
		}
		if (exception == null)
		{
			return result;
		}
		else
		{
			throw exception;
		}
	}

	@Override
	public boolean initializeFirstTerminals(final Set<RuleUnion<TypeTerminal, ?>> visited)
	{
		return Util.or(this.ruleLeft.initializeFirstTerminals(visited), this.ruleRight.initializeFirstTerminals(visited));
	}

	@Override
	public Set<TerminalOrEmpty<TypeTerminal>> firstTerminals()
	{
		Set<TerminalOrEmpty<TypeTerminal>> result = new TreeSet<>(this.ruleLeft.firstTerminals());
		if (result.remove(new Empty<TypeTerminal>()))
		{
			result.addAll(this.ruleRight.firstTerminals());
		}
		return result;
	}

	@Override
	public void checkLL1
		(
			Map<RuleUnion<TypeTerminal, ?>, Set<TerminalOrEmpty<TypeTerminal>>> visited,
			Set<TerminalOrEmpty<TypeTerminal>> followingTerminals
		)
		throws ExceptionParserAmbiguous
	{
		this.ruleLeft.checkLL1(visited, this.ruleRight.firstTerminals());
		if (this.ruleRight.firstTerminals().contains(new Empty<TypeTerminal>()))
		{
			this.ruleLeft.checkLL1(visited, followingTerminals);
		}
		this.ruleRight.checkLL1(visited, followingTerminals);
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLL1(InputStackForward<TypeTerminal> input)
		throws ExceptionParserInput
	{
		return parseTree(this.ruleLeft.parseLL1(input), this.ruleRight.parseLL1(input));
	}

	@Override
	public Set<Item<TypeTerminal, ?>> items()
	{
		final ItemDefaultReduceConcatenation<TypeTerminal> itemReduce = new ItemDefaultReduceConcatenation<>(this);
		final ItemDefaultShiftConcatenationLeftDotRight<TypeTerminal> itemShiftLeftDotRight = new ItemDefaultShiftConcatenationLeftDotRight<>(this, itemReduce);
		final ItemDefaultShiftConcatenationDotLeftRight<TypeTerminal> itemShiftDotLeftRight = new ItemDefaultShiftConcatenationDotLeftRight<>(this, itemShiftLeftDotRight);
		return UtilCollections.setOf(itemShiftDotLeftRight);
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLR
		(
			InputStackReverse<TypeTerminal> input,
			Stack<Rule<TypeTerminal, ?>> rules
		)
	{
		rules.pop();
		ParseTree<? extends TypeSourceRight> right = this.ruleRight.parseLR(input, rules);
		ParseTree<? extends TypeSourceLeft> left = this.ruleLeft.parseLR(input, rules);
		return parseTree(left, right);
	}

	@Override
	public void random(List<TypeTerminal> result)
	{
		this.ruleLeft.random(result);
		this.ruleRight.random(result);
	}

	@Override
	public String toString()
	{
		return super.toString() + this.ruleLeft.toStringId() + this.ruleRight.toStringId() + ")";
	}
}