package nl.novit.parser.rule;

import nl.novit.util.Util;

import java.util.Random;

public abstract class RuleDefault<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends Rule<TypeTerminal, TypeTarget>
{
	public static final System.Logger LOGGER = System.getLogger(RuleDefault.class.getCanonicalName());
	public static final Random RANDOM = new Random(System.currentTimeMillis());
	public static Integer nextId = 0;
	public final Integer id;

	public RuleDefault()
	{
		this.id = nextId;
		nextId += 1;
		LOGGER.log(System.Logger.Level.DEBUG, "Rule #" + this.id + " " + Util.getCanonicalName(getClass()));
	}

	@Override
	public Integer getId()
	{
		return this.id;
	}
}