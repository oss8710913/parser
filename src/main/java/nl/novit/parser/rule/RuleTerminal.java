package nl.novit.parser.rule;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Input;
import nl.novit.parser.internal.InputImmutable;
import nl.novit.parser.internal.InputStackForward;
import nl.novit.parser.internal.InputStackReverse;
import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.ItemDefaultReduceTerminal;
import nl.novit.parser.internal.ItemDefaultShiftTerminal;
import nl.novit.parser.internal.Terminal;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.util.Tuple2Comparable;
import nl.novit.util.UtilCollections;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public abstract class RuleTerminal<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends RuleDefault<TypeTerminal, TypeTarget>
{
	public static final System.Logger LOGGER = System.getLogger(RuleTerminal.class.getCanonicalName());
	public final Terminal<TypeTerminal> terminal;

	public RuleTerminal(TypeTerminal terminal)
	{
		LOGGER.log(System.Logger.Level.DEBUG, "Character: '" + terminal + "'");
		this.terminal = new Terminal<>(terminal);
	}

	public abstract TypeTarget getTarget();

	public ParseTree<? extends TypeTarget> parseTree(Input<TypeTerminal> input)
	{
		return new ParseTree<>(getTarget(), Collections.emptyList(), input.rangeTerminal());
	}

	@Override
	public ParseTree<? extends TypeTarget> parseUnger
		(
			InputImmutable<TypeTerminal> input,
			Set<Tuple2Comparable<Rule<TypeTerminal, ?>, Input<TypeTerminal>>> visited
		)
		throws ExceptionParserInput
	{
		if (this.terminal.equals(input.peek()))
		{
			if (input.pop().isEmpty())
			{
				return parseTree(input);
			}
			else
			{
				throw new ExceptionParserInput(input.getRange().start() + 1);
			}
		}
		else
		{
			throw new ExceptionParserInput(input.getRange().start());
		}
	}

	@Override
	public boolean initializeFirstTerminals(final Set<RuleUnion<TypeTerminal, ?>> visited)
	{
		return false;
	}

	@Override
	public Set<TerminalOrEmpty<TypeTerminal>> firstTerminals()
	{
		return Set.of(this.terminal);
	}

	@Override
	public void checkLL1
		(
			Map<RuleUnion<TypeTerminal, ?>, Set<TerminalOrEmpty<TypeTerminal>>> visited,
			Set<TerminalOrEmpty<TypeTerminal>> followingTerminals
		)
	{
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLL1(InputStackForward<TypeTerminal> input)
		throws ExceptionParserInput
	{
		if (this.terminal.equals(input.peek()))
		{
			ParseTree<? extends TypeTarget> result = parseTree(input);
			input.pop();
			return result;
		}
		else
		{
			throw new ExceptionParserInput(input.getRange().start());
		}
	}

	@Override
	public Set<Item<TypeTerminal, ?>> items()
	{
		final ItemDefaultReduceTerminal<TypeTerminal> itemReduce = new ItemDefaultReduceTerminal<>(this);
		final ItemDefaultShiftTerminal<TypeTerminal> itemShift = new ItemDefaultShiftTerminal<>(this, itemReduce);
		return UtilCollections.setOf(itemShift);
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLR
		(
			InputStackReverse<TypeTerminal> input,
			Stack<Rule<TypeTerminal, ?>> rules
		)
	{
		rules.pop();
		ParseTree<? extends TypeTarget> result = parseTree(input);
		input.pop();
		return result;
	}

	@Override
	public void random(List<TypeTerminal> result)
	{
		result.add(this.terminal.terminal);
	}

	@Override
	public String toString()
	{
		return super.toString() + this.terminal + ")";
	}
}