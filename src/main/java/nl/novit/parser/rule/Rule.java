package nl.novit.parser.rule;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.ExtendedItem;
import nl.novit.parser.internal.Input;
import nl.novit.parser.internal.InputImmutable;
import nl.novit.parser.internal.InputStackForward;
import nl.novit.parser.internal.InputStackReverse;
import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.RuleOrTerminal;
import nl.novit.parser.internal.RuleOrTerminalOrEmpty;
import nl.novit.parser.internal.RuleOrTerminalOrEmptyDefault;
import nl.novit.parser.internal.Terminal;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.util.Tuple2Comparable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

public abstract class Rule<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends RuleOrTerminalOrEmptyDefault<TypeTerminal>
	implements RuleOrTerminal<TypeTerminal>
{
	public abstract Integer getId();

	@Override
	public int compareTo(Empty<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(Terminal<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(Rule<TypeTerminal, ?> that)
	{
		return this.getId().compareTo(that.getId());
	}

	@Override
	public int compareTo(RuleOrTerminalOrEmpty<TypeTerminal> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public boolean isEmpty()
	{
		return false;
	}

	@Override
	public boolean isTerminal()
	{
		return false;
	}

	@Override
	public <Type> Type getTerminalOrEmpty(Map<TerminalOrEmpty<TypeTerminal>, Type> map)
	{
		return null;
	}

	@Override
	public <Type> Type getRuleOrTerminal(Map<RuleOrTerminal<TypeTerminal>, Type> map)
	{
		return map.get(this);
	}

	public abstract ParseTree<? extends TypeTarget> parseUnger
		(
			InputImmutable<TypeTerminal> input,
			Set<Tuple2Comparable<Rule<TypeTerminal, ?>, Input<TypeTerminal>>> visited
		)
		throws ExceptionParserInput;

	public abstract boolean initializeFirstTerminals(final Set<RuleUnion<TypeTerminal, ?>> visited);

	public void initializeFirstTerminals()
	{
		while (initializeFirstTerminals(new TreeSet<>()))
		{
		}
	}

	public abstract Set<TerminalOrEmpty<TypeTerminal>> firstTerminals();

	public boolean isPossiblyEmpty()
	{
		return this.firstTerminals().contains(new Empty<TypeTerminal>());
	}

	public abstract void checkLL1
		(
			Map<RuleUnion<TypeTerminal, ?>, Set<TerminalOrEmpty<TypeTerminal>>> visited,
			Set<TerminalOrEmpty<TypeTerminal>> followingTerminals
		)
		throws ExceptionParserAmbiguous;

	public abstract ParseTree<? extends TypeTarget> parseLL1(InputStackForward<TypeTerminal> input)
		throws ExceptionParserInput;

	public abstract Set<Item<TypeTerminal, ?>> items();

	public Set<ExtendedItem<TypeTerminal, ?>> extendedItems(TerminalOrEmpty<TypeTerminal> lookaheadTerminal)
	{
		Set<ExtendedItem<TypeTerminal, ?>> result = new TreeSet<>();
		for (Item<TypeTerminal, ?> item: items())
		{
			result.add(new ExtendedItem<>(item, lookaheadTerminal));
		}
		return result;
	}

	public abstract ParseTree<? extends TypeTarget> parseLR
		(
			InputStackReverse<TypeTerminal> input,
			Stack<Rule<TypeTerminal, ?>> rules
		);

	public abstract void random(List<TypeTerminal> result);

	public List<TypeTerminal> random()
	{
		List<TypeTerminal> result = new ArrayList<>();
		random(result);
		return result;
	}

	public String toStringId()
	{
		return "#" + this.getId().toString();
	}

	@Override
	public String toString()
	{
		return "Rule(" + toStringId() + "->";
	}
}