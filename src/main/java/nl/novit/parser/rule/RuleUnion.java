package nl.novit.parser.rule;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.Input;
import nl.novit.parser.internal.InputImmutable;
import nl.novit.parser.internal.InputStackForward;
import nl.novit.parser.internal.InputStackReverse;
import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.ItemDefaultReduceUnion;
import nl.novit.parser.internal.ItemDefaultShiftUnion;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.util.Tuple2Comparable;
import nl.novit.util.Util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

public class RuleUnion<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends RuleDefault<TypeTerminal, TypeTarget>
{
	public static final System.Logger LOGGER = System.getLogger(RuleUnion.class.getCanonicalName());
	public final Set<TerminalOrEmpty<TypeTerminal>> firstTerminals;
	private final Map<Integer, Rule<TypeTerminal, ? extends TypeTarget>> rules;

	public RuleUnion()
	{
		this.rules = new TreeMap<>();
		this.firstTerminals = new TreeSet<>();
	}

	@Override
	public ParseTree<? extends TypeTarget> parseUnger
		(
			InputImmutable<TypeTerminal> input, Set<Tuple2Comparable<Rule<TypeTerminal, ?>,
			Input<TypeTerminal>>> visited
		)
		throws ExceptionParserInput
	{
		ParseTree<? extends TypeTarget> result = null;
		ExceptionParserInput exception = new ExceptionParserInput(input.getRange().start());
		Tuple2Comparable<Rule<TypeTerminal, ?>, Input<TypeTerminal>> tuple = new Tuple2Comparable<>(this, input);
		if (!visited.contains(tuple))
		{
			visited.add(tuple);
			Iterator<Rule<TypeTerminal, ? extends TypeTarget>> iterator = this.rules.values().iterator();
			while (exception != null && iterator.hasNext())
			{
				Rule<TypeTerminal, ? extends TypeTarget> rule = iterator.next();
				try
				{
					result = rule.parseUnger(input, visited);
					exception = null;
				}
				catch (ExceptionParserInput exceptionNext)
				{
					if (exception.index < exceptionNext.index)
					{
						exception = exceptionNext;
					}
				}
			}
			visited.remove(tuple);
		}
		if (exception == null)
		{
			return result;
		}
		else
		{
			throw exception;
		}
	}

	@Override
	public boolean initializeFirstTerminals(final Set<RuleUnion<TypeTerminal, ?>> visited)
	{
		boolean result = false;
		if (visited.add(this))
		{
			for (Rule<TypeTerminal, ? extends TypeTarget> rule: this.rules.values())
			{
				result = Util.or(result, rule.initializeFirstTerminals(visited));
				result = Util.or(result, this.firstTerminals.addAll(rule.firstTerminals()));
			}
		}
		return result;
	}

	@Override
	public Set<TerminalOrEmpty<TypeTerminal>> firstTerminals()
	{
		return this.firstTerminals;
	}

	public boolean checkLL1Visit(Map<RuleUnion<TypeTerminal, ?>, Set<TerminalOrEmpty<TypeTerminal>>> visited, Set<TerminalOrEmpty<TypeTerminal>> followingTerminals)
	{
		boolean result;
		Set<TerminalOrEmpty<TypeTerminal>> followingTerminalsVisited = visited.get(this);
		if (followingTerminalsVisited == null)
		{
			result = true;
			visited.put(this, new TreeSet<>(followingTerminals));
		}
		else
		{
			result = followingTerminalsVisited.addAll(followingTerminals);
		}
		return result;
	}

	public void checkLL1FirstTerminals()
		throws ExceptionParserAmbiguous
	{
		int count = 0;
		for (Rule<TypeTerminal, ? extends TypeTarget> rule: this.rules.values())
		{
			count += rule.firstTerminals().size();
		}
		if (this.firstTerminals.size() < count)
		{
			throw new ExceptionParserAmbiguous();
		}
	}

	public void checkLL1Recursive(Map<RuleUnion<TypeTerminal, ?>, Set<TerminalOrEmpty<TypeTerminal>>> visited, Set<TerminalOrEmpty<TypeTerminal>> followingTerminals)
		throws ExceptionParserAmbiguous
	{
		for (Rule<TypeTerminal, ? extends TypeTarget> rule: this.rules.values())
		{
			rule.checkLL1(visited, followingTerminals);
		}
	}

	public void checkLL1FollowingTerminals(Set<TerminalOrEmpty<TypeTerminal>> followingTerminals)
		throws ExceptionParserAmbiguous
	{
		if (this.firstTerminals.contains(new Empty<TypeTerminal>()))
		{
			Set<TerminalOrEmpty<TypeTerminal>> possibleTerminals = new TreeSet<>(this.firstTerminals);
			possibleTerminals.remove(new Empty<TypeTerminal>());
			possibleTerminals.addAll(followingTerminals);
			if (possibleTerminals.size() < this.firstTerminals.size() + followingTerminals.size() - 1)
			{
				throw new ExceptionParserAmbiguous();
			}
		}
	}

	@Override
	public void checkLL1
		(
			Map<RuleUnion<TypeTerminal, ?>, Set<TerminalOrEmpty<TypeTerminal>>> visited,
			Set<TerminalOrEmpty<TypeTerminal>> followingTerminals
		)
		throws ExceptionParserAmbiguous
	{
		if (checkLL1Visit(visited, followingTerminals))
		{
			checkLL1FirstTerminals();
			checkLL1Recursive(visited, followingTerminals);
			checkLL1FollowingTerminals(followingTerminals);
		}
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLL1(InputStackForward<TypeTerminal> input)
		throws ExceptionParserInput
	{
		Rule<TypeTerminal, ? extends TypeTarget> ruleParse = null;
		Rule<TypeTerminal, ? extends TypeTarget> ruleEmpty = null;
		for (Rule<TypeTerminal, ? extends TypeTarget> rule: this.rules.values())
		{
			if (rule.firstTerminals().contains(input.peek()))
			{
				ruleParse = rule;
			}
			if (rule.firstTerminals().contains(new Empty<TypeTerminal>()))
			{
				ruleEmpty = rule;
			}
		}
		if (ruleParse == null)
		{
			ruleParse = ruleEmpty;
		}
		if (ruleParse == null)
		{
			throw new ExceptionParserInput(input.getRange().start());
		}
		return ruleParse.parseLL1(input);
	}

	@Override
	public Set<Item<TypeTerminal, ?>> items()
	{
		Set<Item<TypeTerminal, ?>> result = new TreeSet<>();
		for (final Rule<TypeTerminal, ? extends TypeTarget> rule: this.rules.values())
		{
			final ItemDefaultReduceUnion<TypeTerminal> itemReduce = new ItemDefaultReduceUnion<>(this, rule);
			final ItemDefaultShiftUnion<TypeTerminal> itemShift = new ItemDefaultShiftUnion<>(this, rule, itemReduce);
			result.add(itemShift);
		}
		return result;
	}

	public void add(final Rule<TypeTerminal, ? extends TypeTarget> rule)
	{
		LOGGER.log(System.Logger.Level.DEBUG, "Rule #" + getId() + "\tadd: rule #" + rule.getId());
		this.rules.put(rule.getId(), rule);
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLR
		(
			InputStackReverse<TypeTerminal> input,
			Stack<Rule<TypeTerminal, ?>> rules
		)
	{
		rules.pop();
		return this.rules.get(rules.peek().getId()).parseLR(input, rules);
	}

	@Override
	public void random(List<TypeTerminal> result)
	{
		Iterator<Rule<TypeTerminal, ? extends TypeTarget>> iterator = this.rules.values().iterator();
		int random = RANDOM.nextInt(this.rules.size());
		for (int index = 0; index < random; index++)
		{
			iterator.next();
		}
		iterator.next().random(result);
	}

	@Override
	public String toString()
	{
		return super.toString() + Util.toString(this.rules.values(), "|", Rule::toStringId) + ")";
	}
}