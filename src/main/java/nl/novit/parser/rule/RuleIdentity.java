package nl.novit.parser.rule;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Input;
import nl.novit.parser.internal.InputImmutable;
import nl.novit.parser.internal.InputStackForward;
import nl.novit.parser.internal.InputStackReverse;
import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.util.Tuple2Comparable;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public abstract class RuleIdentity<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget, TypeSource>
	extends Rule<TypeTerminal, TypeTarget>
{
	public static final System.Logger LOGGER = System.getLogger(RuleIdentity.class.getCanonicalName());
	private final Rule<TypeTerminal, TypeSource> rule;

	public RuleIdentity(Rule<TypeTerminal, TypeSource> rule)
	{
		this.rule = rule;
	}

	public abstract TypeTarget getTarget(TypeSource source);

	public ParseTree<? extends TypeTarget> parseTree(ParseTree<? extends TypeSource> source)
	{
		return new ParseTree<>(getTarget(source.getTarget()), List.of(source));
	}

	@Override
	public Integer getId()
	{
		return this.rule.getId();
	}

	@Override
	public ParseTree<? extends TypeTarget> parseUnger
		(
			InputImmutable<TypeTerminal> input,
			Set<Tuple2Comparable<Rule<TypeTerminal, ?>, Input<TypeTerminal>>> visited
		)
		throws ExceptionParserInput
	{
		return parseTree(this.rule.parseUnger(input, visited));
	}

	@Override
	public boolean initializeFirstTerminals(final Set<RuleUnion<TypeTerminal, ?>> visited)
	{
		return this.rule.initializeFirstTerminals(visited);
	}

	@Override
	public Set<TerminalOrEmpty<TypeTerminal>> firstTerminals()
	{
		return this.rule.firstTerminals();
	}

	@Override
	public void checkLL1
		(
			Map<RuleUnion<TypeTerminal, ?>, Set<TerminalOrEmpty<TypeTerminal>>> visited,
			Set<TerminalOrEmpty<TypeTerminal>> followingTerminals
		)
		throws ExceptionParserAmbiguous
	{
		this.rule.checkLL1(visited, followingTerminals);
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLL1(InputStackForward<TypeTerminal> input)
		throws ExceptionParserInput
	{
		return parseTree(this.rule.parseLL1(input));
	}

	@Override
	public Set<Item<TypeTerminal, ?>> items()
	{
		return this.rule.items();
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLR
		(
			InputStackReverse<TypeTerminal> input,
			Stack<Rule<TypeTerminal, ?>> rules
		)
	{
		return parseTree(this.rule.parseLR(input, rules));
	}

	@Override
	public void random(List<TypeTerminal> result)
	{
		this.rule.random(result);
	}

	@Override
	public String toString()
	{
		return this.rule.toString();
	}
}