package nl.novit.parser.rule;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.Input;
import nl.novit.parser.internal.InputImmutable;
import nl.novit.parser.internal.InputStackForward;
import nl.novit.parser.internal.InputStackReverse;
import nl.novit.parser.internal.Item;
import nl.novit.parser.internal.ItemDefaultReduceEmpty;
import nl.novit.parser.internal.TerminalOrEmpty;
import nl.novit.util.Tuple2Comparable;
import nl.novit.util.UtilCollections;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public abstract class RuleEmpty<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends RuleDefault<TypeTerminal, TypeTarget>
{
	public static final System.Logger LOGGER = System.getLogger(RuleEmpty.class.getCanonicalName());

	public RuleEmpty()
	{
		super();
	}

	public abstract TypeTarget getTarget();

	public ParseTree<? extends TypeTarget> parseTree(Input<TypeTerminal> input)
	{
		return new ParseTree<>(getTarget(), Collections.emptyList(), input.rangeEmpty());
	}

	@Override
	public ParseTree<? extends TypeTarget> parseUnger
		(
			InputImmutable<TypeTerminal> input,
			Set<Tuple2Comparable<Rule<TypeTerminal, ?>, Input<TypeTerminal>>> visited
		)
		throws ExceptionParserInput
	{
		if (input.isEmpty())
		{
			return parseTree(input);
		}
		else
		{
			throw new ExceptionParserInput(input.getRange().start());
		}
	}

	@Override
	public boolean initializeFirstTerminals(final Set<RuleUnion<TypeTerminal, ?>> visited)
	{
		return false;
	}

	@Override
	public Set<TerminalOrEmpty<TypeTerminal>> firstTerminals()
	{
		return Set.of(new Empty<>());
	}

	@Override
	public void checkLL1
		(
			Map<RuleUnion<TypeTerminal, ?>, Set<TerminalOrEmpty<TypeTerminal>>> visited,
			Set<TerminalOrEmpty<TypeTerminal>> followingTerminals
		)
	{
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLL1(InputStackForward<TypeTerminal> input)
		throws ExceptionParserInput
	{
		return parseTree(input);
	}

	@Override
	public Set<Item<TypeTerminal, ?>> items()
	{
		final ItemDefaultReduceEmpty<TypeTerminal> itemReduce = new ItemDefaultReduceEmpty<>(this);
		return UtilCollections.setOf(itemReduce);
	}

	@Override
	public ParseTree<? extends TypeTarget> parseLR
		(
			InputStackReverse<TypeTerminal> input,
			Stack<Rule<TypeTerminal, ?>> rules
		)
	{
		rules.pop();
		return parseTree(input);
	}

	@Override
	public void random(List<TypeTerminal> result)
	{
	}

	@Override
	public String toString()
	{
		return super.toString() + "ϵ)";
	}
}