package nl.novit.parser.internal;

import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;

public abstract class ItemDefault<TypeTerminal extends Comparable<? super TypeTerminal>, TypeRule extends Rule<TypeTerminal, ?>>
	implements Item<TypeTerminal, TypeRule>
{
	public static final System.Logger LOGGER = System.getLogger(ItemDefault.class.getCanonicalName());

	public static <TypeTerminal extends Comparable<? super TypeTerminal>, TypeRule extends Rule<TypeTerminal, ?>> int compareToLHS(Item<TypeTerminal, TypeRule> itemA, Item<TypeTerminal, TypeRule> itemB)
	{
		return itemA.getRuleLHS().compareTo(itemB.getRuleLHS());
	}

	public static <TypeTerminal extends Comparable<? super TypeTerminal>, TypeRule extends RuleUnion<TypeTerminal, ?>> int compareToLHSRHS(ItemUnion<TypeTerminal> itemA, ItemUnion<TypeTerminal> itemB)
	{
		int result = compareToLHS(itemA, itemB);
		if (result == 0)
		{
			result = itemA.getRuleRHS().compareTo(itemB.getRuleRHS());
		}
		return result;
	}

	public final TypeRule ruleLHS;

	public ItemDefault(TypeRule ruleLHS)
	{
		this.ruleLHS = ruleLHS;
	}

	@Override
	public TypeRule getRuleLHS()
	{
		return this.ruleLHS;
	}

	@Override
	public boolean equals(Item<TypeTerminal, ?> that)
	{
		return this.compareTo(that) == 0;
	}

	@Override
	public String toString()
	{
		return "Item(" + getRuleLHS().toStringId() + "->";
	}
}