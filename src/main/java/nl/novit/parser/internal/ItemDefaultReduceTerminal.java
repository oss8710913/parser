package nl.novit.parser.internal;

import nl.novit.parser.rule.RuleTerminal;

public class ItemDefaultReduceTerminal<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends ItemDefaultReduce<TypeTerminal, RuleTerminal<TypeTerminal, ?>>
{
	public ItemDefaultReduceTerminal(RuleTerminal<TypeTerminal, ?> ruleLHS)
	{
		super(ruleLHS);
	}

	@Override
	public int compareTo(ItemDefaultReduceConcatenation<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultReduceEmpty<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultReduceTerminal<TypeTerminal> that)
	{
		return compareToLHS(this, that);
	}

	@Override
	public int compareTo(ItemDefaultReduceUnion<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(Item<TypeTerminal, ?> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public int getReduced()
	{
		return 1;
	}

	@Override
	public String toString()
	{
		return super.toString() + getRuleLHS().terminal + ".)";
	}
}