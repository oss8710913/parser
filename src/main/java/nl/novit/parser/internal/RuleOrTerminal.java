package nl.novit.parser.internal;

public interface RuleOrTerminal<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends RuleOrTerminalOrEmpty<TypeTerminal>
{
}