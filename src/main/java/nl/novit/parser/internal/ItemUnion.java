package nl.novit.parser.internal;

import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;

public interface ItemUnion<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends Item<TypeTerminal, RuleUnion<TypeTerminal, ?>>
{
	Rule<TypeTerminal, ?> getRuleRHS();
}