package nl.novit.parser.internal;

import java.util.List;
import java.util.Stack;

public class StackRuleOrTerminalOrEmpty<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends Stack<RuleOrTerminalOrEmpty<TypeTerminal>>
{
	public StackRuleOrTerminalOrEmpty(List<TypeTerminal> input)
	{
		for (int index = input.size() - 1; 0 <= index; index--)
		{
			push(new Terminal<>(input.get(index)));
		}
	}

	@Override
	public synchronized RuleOrTerminalOrEmpty<TypeTerminal> peek()
	{
		return isEmpty() ? new Empty<>() : super.peek();
	}

	@Override
	public synchronized RuleOrTerminalOrEmpty<TypeTerminal> pop()
	{
		return isEmpty() ? new Empty<>() : super.pop();
	}

	public int sizeTerminals()
	{
		int result = 0;
		for (RuleOrTerminalOrEmpty<TypeTerminal> ruleOrTerminalOrEmpty: this)
		{
			if (ruleOrTerminalOrEmpty.isTerminal())
			{
				result += 1;
			}
		}
		return result;
	}
}