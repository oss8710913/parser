package nl.novit.parser.internal;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.rule.Rule;

import java.util.Set;
import java.util.TreeSet;

public abstract class ItemDefaultReduce<TypeTerminal extends Comparable<? super TypeTerminal>, TypeRule extends Rule<TypeTerminal, ?>>
	extends ItemDefault<TypeTerminal, TypeRule>
{
	public ItemDefaultReduce(TypeRule ruleLHS)
	{
		super(ruleLHS);
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationDotLeftRight<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationLeftDotRight<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(ItemDefaultShiftTerminal<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(ItemDefaultShiftUnion<TypeTerminal> that)
	{
		return -1;
	}

	public abstract int getReduced();

	@Override
	public Set<ExtendedItem<TypeTerminal, ?>> close(TerminalOrEmpty<TypeTerminal> lookahead)
	{
		return new TreeSet<>();
	}

	public boolean add(State<TypeTerminal, ?> state, TerminalOrEmpty<TypeTerminal> lookAhead)
		throws ExceptionParserAmbiguous
	{
		boolean result;
		ItemDefaultReduce<TypeTerminal, ?> item = state.getReduce().get(lookAhead);
		if (item == null)
		{
			if (lookAhead.getRuleOrTerminal(state.getShift()) == null)
			{
				state.getReduce().put(lookAhead, this);
				result = true;
			}
			else
			{
				throw new ExceptionParserAmbiguous();
			}
		}
		else
		{
			if (item.equals(this))
			{
				result = false;
			}
			else
			{
				throw new ExceptionParserAmbiguous();
			}
		}
		return result;
	}
}