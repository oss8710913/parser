package nl.novit.parser.internal;

import nl.novit.parser.rule.RuleConcatenation;

import java.util.Set;
import java.util.TreeSet;

public class ItemDefaultShiftConcatenationDotLeftRight<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends ItemDefaultShift<TypeTerminal, RuleConcatenation<TypeTerminal, ?, ?, ?>>
{
	public ItemDefaultShiftConcatenationDotLeftRight(RuleConcatenation<TypeTerminal, ?, ?, ?> ruleLHS, ItemDefaultShiftConcatenationLeftDotRight<TypeTerminal> itemNext)
	{
		super(ruleLHS, itemNext);
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationDotLeftRight<TypeTerminal> that)
	{
		return compareToLHS(this, that);
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationLeftDotRight<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(ItemDefaultShiftTerminal<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(ItemDefaultShiftUnion<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(Item<TypeTerminal, ?> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public RuleOrTerminal<TypeTerminal> getActive()
	{
		return getRuleLHS().ruleLeft;
	}

	@Override
	public Set<ExtendedItem<TypeTerminal, ?>> close(TerminalOrEmpty<TypeTerminal> lookAhead)
	{
		Set<ExtendedItem<TypeTerminal, ?>> result = new TreeSet<>();
		for (TerminalOrEmpty<TypeTerminal> terminalOrEmpty: getRuleLHS().ruleRight.firstTerminals())
		{
			result.addAll((getRuleLHS().ruleLeft.extendedItems(terminalOrEmpty.isEmpty() ? lookAhead : terminalOrEmpty)));
		}
		return result;
	}

	@Override
	public String toString()
	{
		return super.toString() + "." + getRuleLHS().ruleLeft.toStringId() + getRuleLHS().ruleRight.toStringId() + ")";
	}
}