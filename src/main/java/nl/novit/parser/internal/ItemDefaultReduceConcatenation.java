package nl.novit.parser.internal;

import nl.novit.parser.rule.RuleConcatenation;

public class ItemDefaultReduceConcatenation<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends ItemDefaultReduce<TypeTerminal, RuleConcatenation<TypeTerminal, ?, ?, ?>>
{
	public ItemDefaultReduceConcatenation(RuleConcatenation<TypeTerminal, ?, ?, ?> ruleLHS)
	{
		super(ruleLHS);
	}

	@Override
	public int compareTo(ItemDefaultReduceConcatenation<TypeTerminal> that)
	{
		return compareToLHS(this, that);
	}

	@Override
	public int compareTo(ItemDefaultReduceEmpty<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(ItemDefaultReduceTerminal<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(ItemDefaultReduceUnion<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(Item<TypeTerminal, ?> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public int getReduced()
	{
		return 2;
	}

	@Override
	public String toString()
	{
		return super.toString() + getRuleLHS().ruleLeft.toStringId() + getRuleLHS().ruleRight.toStringId() + ".)";
	}
}