package nl.novit.parser.internal;

import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;

import java.util.Set;

public class ItemDefaultShiftUnion<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends ItemDefaultShift<TypeTerminal, RuleUnion<TypeTerminal, ?>>
	implements ItemUnion<TypeTerminal>
{
	public final Rule<TypeTerminal, ?> ruleRHS;

	public ItemDefaultShiftUnion(RuleUnion<TypeTerminal, ?> ruleLHS, Rule<TypeTerminal, ?> ruleRHS, ItemDefaultReduceUnion<TypeTerminal> itemNext)
	{
		super(ruleLHS, itemNext);
		this.ruleRHS = ruleRHS;
	}

	@Override
	public Rule<TypeTerminal, ?> getRuleRHS()
	{
		return this.ruleRHS;
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationDotLeftRight<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationLeftDotRight<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultShiftTerminal<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultShiftUnion<TypeTerminal> that)
	{
		return compareToLHSRHS(this, that);
	}

	@Override
	public int compareTo(Item<TypeTerminal, ?> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public RuleOrTerminal<TypeTerminal> getActive()
	{
		return this.ruleRHS;
	}

	@Override
	public Set<ExtendedItem<TypeTerminal, ?>> close(TerminalOrEmpty<TypeTerminal> lookAhead)
	{
		return this.ruleRHS.extendedItems(lookAhead);
	}

	@Override
	public String toString()
	{
		return super.toString() + "." + getRuleRHS().toStringId() + ")";
	}
}