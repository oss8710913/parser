package nl.novit.parser.internal;

public class ExtendedItem<TypeTerminal extends Comparable<? super TypeTerminal>, TypeItem extends Item<TypeTerminal, ?>>
	implements Comparable<ExtendedItem<TypeTerminal, ?>>
{
	public final TypeItem item;
	public final TerminalOrEmpty<TypeTerminal> lookAhead;

	public ExtendedItem(TypeItem item, TerminalOrEmpty<TypeTerminal> lookAhead)
	{
		this.item = item;
		this.lookAhead = lookAhead;
	}

	@Override
	public int compareTo(ExtendedItem<TypeTerminal, ?> that)
	{
		int result = this.item.compareTo(that.item);
		if (result == 0)
		{
			result = this.lookAhead.compareTo(that.lookAhead);
		}
		return result;
	}

	public boolean equals(ExtendedItem<TypeTerminal, ?> that)
	{
		return compareTo(that) == 0;
	}

	@Override
	public boolean equals(Object that)
	{
		return that instanceof ExtendedItem && equals((ExtendedItem<TypeTerminal, ?>) that);
	}

	public TypeItem getItem()
	{
		return this.item;
	}

	public TerminalOrEmpty<TypeTerminal> getLookAhead()
	{
		return this.lookAhead;
	}

	@Override
	public String toString()
	{
		return "(" + this.item.toString() + " [" + this.lookAhead.toString() + "])";
	}
}