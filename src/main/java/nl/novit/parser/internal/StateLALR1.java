package nl.novit.parser.internal;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.util.Util;

import java.util.Map;
import java.util.Set;

public class StateLALR1<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends State<TypeTerminal, StateLALR1<TypeTerminal>>
{
	public static final System.Logger LOGGER = System.getLogger(StateLALR1.class.getCanonicalName());
	public final Set<Item<TypeTerminal, ?>> items;

	public StateLALR1(Set<ExtendedItem<TypeTerminal, ?>> extendedItems)
		throws ExceptionParserAmbiguous
	{
		super(extendedItems);
		this.items = items();
	}

	@Override
	public StateLALR1<TypeTerminal> create(Set<ExtendedItem<TypeTerminal, ?>> extendedItems)
		throws ExceptionParserAmbiguous
	{
		return new StateLALR1<>(extendedItems);
	}

	@Override
	public int compareTo(StateLALR1<TypeTerminal> that)
	{
		return Util.compareIterable(this.items, that.items);
	}

	public void merge
		(
			StateLALR1<TypeTerminal> state,
			Set<StateLALR1<TypeTerminal>> changed
		)
		throws ExceptionParserAmbiguous
	{
		for (Map.Entry<TerminalOrEmpty<TypeTerminal>, ItemDefaultReduce<TypeTerminal, ?>> entry: state.getReduce().entrySet())
		{
			if (entry.getValue().add(this, entry.getKey()))
			{
				changed.add(this);
			}
		}
		for (Set<ExtendedItem<TypeTerminal, ItemDefaultShift<TypeTerminal, ?>>> extendedItems: state.getShift().values())
		{
			for (ExtendedItem<TypeTerminal, ItemDefaultShift<TypeTerminal, ?>> extendedItem: extendedItems)
			{
				ItemDefaultShift<TypeTerminal, ?> itemShift = extendedItem.item;
				TerminalOrEmpty<TypeTerminal> lookAhead = extendedItem.lookAhead;
				if (itemShift.add(this, lookAhead))
				{
					changed.add(this);
				}
			}
		}
	}
}