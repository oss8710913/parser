package nl.novit.parser.internal;

import nl.novit.parser.rule.RuleConcatenation;

import java.util.Set;

public class ItemDefaultShiftConcatenationLeftDotRight<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends ItemDefaultShift<TypeTerminal, RuleConcatenation<TypeTerminal, ?, ?, ?>>
{
	public ItemDefaultShiftConcatenationLeftDotRight(RuleConcatenation<TypeTerminal, ?, ?, ?> ruleLHS, ItemDefaultReduceConcatenation<TypeTerminal> itemNext)
	{
		super(ruleLHS, itemNext);
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationDotLeftRight<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationLeftDotRight<TypeTerminal> that)
	{
		return compareToLHS(this, that);
	}

	@Override
	public int compareTo(ItemDefaultShiftTerminal<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(ItemDefaultShiftUnion<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(Item<TypeTerminal, ?> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public RuleOrTerminal<TypeTerminal> getActive()
	{
		return getRuleLHS().ruleRight;
	}

	@Override
	public Set<ExtendedItem<TypeTerminal, ?>> close(TerminalOrEmpty<TypeTerminal> lookAhead)
	{
		return getRuleLHS().ruleRight.extendedItems(lookAhead);
	}

	@Override
	public String toString()
	{
		return super.toString() + getRuleLHS().ruleLeft.toStringId() + "." + getRuleLHS().ruleRight.toStringId() + ")";
	}
}