package nl.novit.parser.internal;

import nl.novit.parser.common.Range;

import java.util.List;

public class InputStackForward<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends Input<TypeTerminal>
{
	public InputStackForward(List<TypeTerminal> list)
	{
		super(list);
	}

	@Override
	public Range rangeEmpty()
	{
		return new Range(this.range.start(), this.range.start());
	}

	@Override
	public Range rangeTerminal()
	{
		return new Range(this.range.start(), this.range.start() + 1);
	}

	public TerminalOrEmpty<TypeTerminal> pop()
	{
		TerminalOrEmpty<TypeTerminal> result = peek();
		if (!isEmpty())
		{
			this.range = this.getRange().tail();
		}
		return result;
	}
}