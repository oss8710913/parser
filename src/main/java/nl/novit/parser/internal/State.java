package nl.novit.parser.internal;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.rule.Rule;
import nl.novit.util.Util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

public abstract class State<TypeTerminal extends Comparable<? super TypeTerminal>, TypeState extends State<TypeTerminal, TypeState>>
	implements Comparable<TypeState>
{
	public static final System.Logger LOGGER = System.getLogger(State.class.getCanonicalName());
	public static int nextId = 0;
	public final int id;
	public final Map<TerminalOrEmpty<TypeTerminal>, ItemDefaultReduce<TypeTerminal, ?>> reduce;
	public final Map<RuleOrTerminal<TypeTerminal>, Set<ExtendedItem<TypeTerminal, ItemDefaultShift<TypeTerminal, ?>>>> shift;
	public final Map<RuleOrTerminal<TypeTerminal>, TypeState> states;

	public State(Set<ExtendedItem<TypeTerminal, ?>> extendedItems)
		throws ExceptionParserAmbiguous
	{
		this.id = nextId++;
		this.reduce = new TreeMap<>();
		this.shift = new TreeMap<>();
		this.states = new TreeMap<>();
		for (ExtendedItem<TypeTerminal, ?> extendedItem: close(extendedItems))
		{
			extendedItem.getItem().add(this, extendedItem.getLookAhead());
		}
	}

	public abstract TypeState create(Set<ExtendedItem<TypeTerminal, ?>> extendedItems)
		throws ExceptionParserAmbiguous;

	public abstract void merge
		(
			TypeState state,
			Set<TypeState> changed
		)
		throws ExceptionParserAmbiguous;

	public Map<TerminalOrEmpty<TypeTerminal>, ItemDefaultReduce<TypeTerminal, ?>> getReduce()
	{
		return this.reduce;
	}

	public Map<RuleOrTerminal<TypeTerminal>, Set<ExtendedItem<TypeTerminal, ItemDefaultShift<TypeTerminal, ?>>>> getShift()
	{
		return this.shift;
	}

	public Map<RuleOrTerminal<TypeTerminal>, TypeState> getStates()
	{
		return this.states;
	}

	public Set<Item<TypeTerminal, ?>> items()
	{
		Set<Item<TypeTerminal, ?>> result = new TreeSet<>(getReduce().values());
		getShift().values().forEach(extendedItems -> extendedItems.stream().map(extendedItem -> extendedItem.item).forEach(result::add));
		return result;
	}

	public Set<ExtendedItem<TypeTerminal, ?>> extendedItems()
	{
		Set<ExtendedItem<TypeTerminal, ?>> result = new TreeSet<>();
		getReduce().forEach((key, value) -> result.add(new ExtendedItem<>(value, key)));
		getShift().values().forEach(result::addAll);
		return result;
	}

	public Set<ExtendedItem<TypeTerminal, ?>> close(Set<ExtendedItem<TypeTerminal, ?>> result)
	{
		List<ExtendedItem<TypeTerminal, ?>> extendedItemsProcessing = new LinkedList<>(result);
		while (!extendedItemsProcessing.isEmpty())
		{
			List<ExtendedItem<TypeTerminal, ?>> extendedItemsClosed = new LinkedList<>();
			for (ExtendedItem<TypeTerminal, ?> extendedItemProcessing: extendedItemsProcessing)
			{
				for (ExtendedItem<TypeTerminal, ?> extendedItemClosed: extendedItemProcessing.getItem().close(extendedItemProcessing.getLookAhead()))
				{
					if (result.add(extendedItemClosed))
					{
						extendedItemsClosed.add(extendedItemClosed);
					}
				}
			}
			extendedItemsProcessing = extendedItemsClosed;
		}
		return result;
	}

	public void statesNext
		(
			Map<TypeState, TypeState> existingStates,
			Set<TypeState> changed
		)
		throws ExceptionParserAmbiguous
	{
		for (Map.Entry<RuleOrTerminal<TypeTerminal>, Set<ExtendedItem<TypeTerminal, ItemDefaultShift<TypeTerminal, ?>>>> active2ExtendedItems: getShift().entrySet())
		{
			Set<ExtendedItem<TypeTerminal, ?>> extendedItemsNext = new TreeSet<>();
			RuleOrTerminal<TypeTerminal> active = active2ExtendedItems.getKey();
			Set<ExtendedItem<TypeTerminal, ItemDefaultShift<TypeTerminal, ?>>> extendedItems = active2ExtendedItems.getValue();
			for (ExtendedItem<TypeTerminal, ItemDefaultShift<TypeTerminal, ?>> extendedItem: extendedItems)
			{
				ItemDefaultShift<TypeTerminal, ?> itemShift = extendedItem.getItem();
				extendedItemsNext.add(new ExtendedItem<>(itemShift.getItemNext(), extendedItem.getLookAhead()));
			}
			TypeState state = create(extendedItemsNext);
			TypeState existing = existingStates.get(state);
			if (existing == null)
			{
				existingStates.put(state, state);
				changed.add(state);
				existing = state;
			}
			else
			{
				existing.merge
					(
						state,
						changed
					);
			}
			getStates().put(active, existing);
		}
	}

	public int parse
		(
			StackRuleOrTerminalOrEmpty<TypeTerminal> input,
			Stack<Rule<TypeTerminal, ?>> rules,
			int inputSize
		)
		throws ExceptionParserInput
	{
		int result;
		do
		{
			ItemDefaultReduce<TypeTerminal, ?> item = input.peek().getTerminalOrEmpty(getReduce());
			if (item == null)
			{
				State<TypeTerminal, ?> state = input.peek().getRuleOrTerminal(getStates());
				if (state == null)
				{
					throw new ExceptionParserInput(inputSize - input.sizeTerminals());
				}
				else
				{
					RuleOrTerminalOrEmpty<TypeTerminal> popped = input.pop();
					result = state.parse(input, rules, inputSize) - 1;
					if (result < 0)
					{
						input.push(popped);
					}
				}
			}
			else
			{
				rules.push(item.getRuleLHS());
				input.push(item.getRuleLHS());
				result = item.getReduced();
			}
		}
		while (result < 0);
		return result;
	}

	@Override
	public String toString()
	{
		return "State(" + Util.toString(extendedItems(), ", ") + ")";
	}
}