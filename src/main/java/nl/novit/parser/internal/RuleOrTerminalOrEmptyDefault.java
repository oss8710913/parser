package nl.novit.parser.internal;

public abstract class RuleOrTerminalOrEmptyDefault<TypeTerminal extends Comparable<? super TypeTerminal>>
	implements RuleOrTerminalOrEmpty<TypeTerminal>
{
	@Override
	public boolean equals(RuleOrTerminalOrEmpty<TypeTerminal> that)
	{
		return this.compareTo(that) == 0;
	}

	@Override
	public boolean equals(Object that)
	{
		return that instanceof RuleOrTerminalOrEmpty && equals((RuleOrTerminalOrEmpty<TypeTerminal>) that);
	}
}