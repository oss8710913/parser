package nl.novit.parser.internal;

import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleUnion;

public class ItemDefaultReduceUnion<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends ItemDefaultReduce<TypeTerminal, RuleUnion<TypeTerminal, ?>>
	implements ItemUnion<TypeTerminal>
{
	public final Rule<TypeTerminal, ?> ruleRHS;

	public ItemDefaultReduceUnion(RuleUnion<TypeTerminal, ?> ruleLHS, Rule<TypeTerminal, ?> ruleRHS)
	{
		super(ruleLHS);
		this.ruleRHS = ruleRHS;
	}

	@Override
	public Rule<TypeTerminal, ?> getRuleRHS()
	{
		return this.ruleRHS;
	}

	@Override
	public int compareTo(ItemDefaultReduceConcatenation<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultReduceEmpty<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultReduceTerminal<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultReduceUnion<TypeTerminal> that)
	{
		return compareToLHSRHS(this, that);
	}

	@Override
	public int compareTo(Item<TypeTerminal, ?> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public int getReduced()
	{
		return 1;
	}

	@Override
	public String toString()
	{
		return super.toString() + getRuleRHS().toStringId() + ".)";
	}
}