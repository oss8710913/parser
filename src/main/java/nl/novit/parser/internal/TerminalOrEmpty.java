package nl.novit.parser.internal;

public interface TerminalOrEmpty<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends RuleOrTerminalOrEmpty<TypeTerminal>
{
}