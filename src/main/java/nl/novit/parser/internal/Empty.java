package nl.novit.parser.internal;

import nl.novit.parser.rule.Rule;

import java.util.Map;

public class Empty<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends RuleOrTerminalOrEmptyDefault<TypeTerminal>
	implements TerminalOrEmpty<TypeTerminal>
{
	@Override
	public int compareTo(Rule<TypeTerminal, ?> that)
	{
		return -1;
	}

	@Override
	public int compareTo(Terminal<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(Empty<TypeTerminal> that)
	{
		return 0;
	}

	@Override
	public int compareTo(RuleOrTerminalOrEmpty<TypeTerminal> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public boolean isEmpty()
	{
		return true;
	}

	@Override
	public boolean isTerminal()
	{
		return false;
	}

	@Override
	public <Type> Type getTerminalOrEmpty(Map<TerminalOrEmpty<TypeTerminal>, Type> map)
	{
		return map.get(this);
	}

	@Override
	public <Type> Type getRuleOrTerminal(Map<RuleOrTerminal<TypeTerminal>, Type> map)
	{
		return null;
	}

	@Override
	public String toString()
	{
		return "ϵ";
	}
}