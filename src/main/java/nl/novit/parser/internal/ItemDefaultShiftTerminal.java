package nl.novit.parser.internal;

import nl.novit.parser.rule.RuleTerminal;

import java.util.Set;
import java.util.TreeSet;

public class ItemDefaultShiftTerminal<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends ItemDefaultShift<TypeTerminal, RuleTerminal<TypeTerminal, ?>>
{
	public ItemDefaultShiftTerminal(RuleTerminal<TypeTerminal, ?> ruleLHS, ItemDefaultReduceTerminal<TypeTerminal> itemNext)
	{
		super(ruleLHS, itemNext);
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationDotLeftRight<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultShiftConcatenationLeftDotRight<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultShiftTerminal<TypeTerminal> that)
	{
		return compareToLHS(this, that);
	}

	@Override
	public int compareTo(ItemDefaultShiftUnion<TypeTerminal> that)
	{
		return -1;
	}

	@Override
	public int compareTo(Item<TypeTerminal, ?> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public RuleOrTerminal<TypeTerminal> getActive()
	{
		return getRuleLHS().terminal;
	}

	@Override
	public Set<ExtendedItem<TypeTerminal, ?>> close(TerminalOrEmpty<TypeTerminal> lookAhead)
	{
		return new TreeSet<>();
	}

	@Override
	public String toString()
	{
		return super.toString() + "." + getRuleLHS().terminal + ")";
	}
}