package nl.novit.parser.internal;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.rule.Rule;

import java.util.Set;
import java.util.TreeSet;

public abstract class ItemDefaultShift<TypeTerminal extends Comparable<? super TypeTerminal>, TypeRule extends Rule<TypeTerminal, ?>>
	extends ItemDefault<TypeTerminal, TypeRule>
{
	public final Item<TypeTerminal, ?> itemNext;

	public ItemDefaultShift(TypeRule ruleLHS, Item<TypeTerminal, ?> itemNext)
	{
		super(ruleLHS);
		this.itemNext = itemNext;
	}

	@Override
	public int compareTo(ItemDefaultReduceConcatenation<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultReduceEmpty<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultReduceTerminal<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(ItemDefaultReduceUnion<TypeTerminal> that)
	{
		return 1;
	}

	public abstract RuleOrTerminal<TypeTerminal> getActive();

	public Item<TypeTerminal, ?> getItemNext()
	{
		return this.itemNext;
	}

	public boolean add(State<TypeTerminal, ?> state, TerminalOrEmpty<TypeTerminal> lookAhead)
		throws ExceptionParserAmbiguous
	{
		if (getActive().getTerminalOrEmpty(state.getReduce()) == null)
		{
			Set<ExtendedItem<TypeTerminal, ItemDefaultShift<TypeTerminal, ?>>> extendedItems = state.getShift().computeIfAbsent(getActive(), k -> new TreeSet<>());
			return extendedItems.add(new ExtendedItem<>(this, lookAhead));
		}
		else
		{
			throw new ExceptionParserAmbiguous();
		}
	}
}