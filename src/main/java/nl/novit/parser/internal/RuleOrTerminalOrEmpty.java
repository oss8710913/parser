package nl.novit.parser.internal;

import nl.novit.parser.rule.Rule;

import java.util.Map;

public interface RuleOrTerminalOrEmpty<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends Comparable<RuleOrTerminalOrEmpty<TypeTerminal>>
{
	int compareTo(Rule<TypeTerminal, ?> that);

	int compareTo(Terminal<TypeTerminal> that);

	int compareTo(Empty<TypeTerminal> that);

	boolean equals(RuleOrTerminalOrEmpty<TypeTerminal> that);

	boolean isEmpty();

	boolean isTerminal();

	<Type> Type getTerminalOrEmpty(Map<TerminalOrEmpty<TypeTerminal>, Type> map);

	<Type> Type getRuleOrTerminal(Map<RuleOrTerminal<TypeTerminal>, Type> map);
}