package nl.novit.parser.internal;

import nl.novit.parser.common.Range;

import java.util.List;

public class InputStackReverse<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends Input<TypeTerminal>
{
	public InputStackReverse(List<TypeTerminal> list)
	{
		super(list);
	}

	@Override
	public Range rangeEmpty()
	{
		return new Range(this.range.end(), this.range.end());
	}

	@Override
	public Range rangeTerminal()
	{
		return new Range(this.range.end() - 1, this.range.end());
	}

	public TerminalOrEmpty<TypeTerminal> pop()
	{
		TerminalOrEmpty<TypeTerminal> result = peek();
		if (!isEmpty())
		{
			this.range = this.getRange().head();
		}
		return result;
	}
}