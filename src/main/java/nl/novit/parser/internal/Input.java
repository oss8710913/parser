package nl.novit.parser.internal;

import nl.novit.parser.common.Range;
import nl.novit.util.Util;

import java.util.List;

public abstract class Input<TypeTerminal extends Comparable<? super TypeTerminal>>
	implements Comparable<Input<TypeTerminal>>
{
	protected final List<TypeTerminal> list;
	protected Range range;

	public Input(List<TypeTerminal> list, Range range)
	{
		this.list = list;
		this.range = range;
	}

	public Input(List<TypeTerminal> list)
	{
		this(list, new Range(0, list.size()));
	}

	public Range getRange()
	{
		return this.range;
	}

	@Override
	public int compareTo(Input<TypeTerminal> that)
	{
		int result = this.range.compareTo(that.range);
		if (result == 0)
		{
			result = Util.compareIterable(this.list, that.list);
		}
		return result;
	}

	public boolean equals(Input<TypeTerminal> that)
	{
		return compareTo(that) == 0;
	}

	@Override
	public boolean equals(Object that)
	{
		return that instanceof Input && equals((Input<TypeTerminal>) that);
	}

	public boolean isEmpty()
	{
		return this.range.isEmpty();
	}

	public abstract Range rangeEmpty();

	public abstract Range rangeTerminal();

	public TerminalOrEmpty<TypeTerminal> peek()
	{
		return isEmpty() ? new Empty<>() : new Terminal<>(this.list.get(rangeTerminal().start()));
	}
}