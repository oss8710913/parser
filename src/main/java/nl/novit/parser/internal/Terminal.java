package nl.novit.parser.internal;

import nl.novit.parser.rule.Rule;

import java.util.Map;

public class Terminal<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends RuleOrTerminalOrEmptyDefault<TypeTerminal>
	implements RuleOrTerminal<TypeTerminal>, TerminalOrEmpty<TypeTerminal>
{
	public final TypeTerminal terminal;

	public Terminal(TypeTerminal terminal)
	{
		this.terminal = terminal;
	}

	@Override
	public int compareTo(Rule<TypeTerminal, ?> that)
	{
		return -1;
	}

	@Override
	public int compareTo(Terminal<TypeTerminal> that)
	{
		return this.terminal.compareTo(that.terminal);
	}

	@Override
	public int compareTo(Empty<TypeTerminal> that)
	{
		return 1;
	}

	@Override
	public int compareTo(RuleOrTerminalOrEmpty<TypeTerminal> that)
	{
		return -that.compareTo(this);
	}

	@Override
	public boolean isEmpty()
	{
		return false;
	}

	@Override
	public boolean isTerminal()
	{
		return true;
	}

	@Override
	public <Type> Type getTerminalOrEmpty(Map<TerminalOrEmpty<TypeTerminal>, Type> map)
	{
		return map.get(this);
	}

	@Override
	public <Type> Type getRuleOrTerminal(Map<RuleOrTerminal<TypeTerminal>, Type> map)
	{
		return map.get(this);
	}

	@Override
	public String toString()
	{
		return this.terminal.toString();
	}
}