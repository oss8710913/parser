package nl.novit.parser.internal;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.rule.Rule;

import java.util.Set;

public interface Item<TypeTerminal extends Comparable<? super TypeTerminal>, TypeRule extends Rule<TypeTerminal, ?>>
	extends Comparable<Item<TypeTerminal, ?>>
{
	TypeRule getRuleLHS();

	int compareTo(ItemDefaultReduceConcatenation<TypeTerminal> that);

	int compareTo(ItemDefaultReduceEmpty<TypeTerminal> that);

	int compareTo(ItemDefaultReduceTerminal<TypeTerminal> that);

	int compareTo(ItemDefaultReduceUnion<TypeTerminal> that);

	int compareTo(ItemDefaultShiftConcatenationDotLeftRight<TypeTerminal> that);

	int compareTo(ItemDefaultShiftConcatenationLeftDotRight<TypeTerminal> that);

	int compareTo(ItemDefaultShiftTerminal<TypeTerminal> that);

	int compareTo(ItemDefaultShiftUnion<TypeTerminal> that);

	boolean equals(Item<TypeTerminal, ?> that);

	Set<ExtendedItem<TypeTerminal, ?>> close(TerminalOrEmpty<TypeTerminal> lookAhead);

	boolean add(State<TypeTerminal, ?> state, TerminalOrEmpty<TypeTerminal> lookAhead)
		throws ExceptionParserAmbiguous;
}