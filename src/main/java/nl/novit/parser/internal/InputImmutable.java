package nl.novit.parser.internal;

import nl.novit.parser.common.Range;

import java.util.List;

public class InputImmutable<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends Input<TypeTerminal>
{
	public InputImmutable(List<TypeTerminal> list, Range range)
	{
		super(list, range);
	}

	public InputImmutable(InputImmutable<TypeTerminal> input, Range range)
	{
		this(input.list, range);
	}

	public InputImmutable(List<TypeTerminal> list)
	{
		super(list);
	}

	@Override
	public Range rangeEmpty()
	{
		return new Range(this.range.start(), this.range.start());
	}

	@Override
	public Range rangeTerminal()
	{
		return new Range(this.range.start(), this.range.start() + 1);
	}

	public InputImmutable<TypeTerminal> pop()
	{
		return isEmpty() ? null : new InputImmutable<>(this.list, this.getRange().tail());
	}
}