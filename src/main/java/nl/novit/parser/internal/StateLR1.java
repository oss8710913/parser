package nl.novit.parser.internal;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.util.Util;

import java.util.Set;

public class StateLR1<TypeTerminal extends Comparable<? super TypeTerminal>>
	extends State<TypeTerminal, StateLR1<TypeTerminal>>
{
	public static final System.Logger LOGGER = System.getLogger(StateLR1.class.getCanonicalName());
	public final Set<ExtendedItem<TypeTerminal, ?>> extendedItems;

	public StateLR1(Set<ExtendedItem<TypeTerminal, ?>> extendedItems)
		throws ExceptionParserAmbiguous
	{
		super(extendedItems);
		this.extendedItems = extendedItems();
	}

	@Override
	public StateLR1<TypeTerminal> create(Set<ExtendedItem<TypeTerminal, ?>> extendedItems)
		throws ExceptionParserAmbiguous
	{
		return new StateLR1<>(extendedItems);
	}

	@Override
	public int compareTo(StateLR1<TypeTerminal> that)
	{
		return Util.compareIterable(this.extendedItems, that.extendedItems);
	}

	@Override
	public void merge(StateLR1<TypeTerminal> state, Set<StateLR1<TypeTerminal>> changed)
		throws ExceptionParserAmbiguous
	{
	}
}