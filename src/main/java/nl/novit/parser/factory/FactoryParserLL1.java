package nl.novit.parser.factory;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.parser.Parser;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;

public class FactoryParserLL1
	implements FactoryParser
{
	public static final FactoryParser FACTORY_PARSER = new FactoryParserLL1();

	private FactoryParserLL1()
	{
	}

	@Override
	public <TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget> Parser<TypeTerminal, TypeTarget> parser(Rule<TypeTerminal, TypeTarget> rule)
		throws ExceptionParserAmbiguous
	{
		return new ParserLL1<>(rule);
	}
}