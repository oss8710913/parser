package nl.novit.parser.factory;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.parser.Parser;
import nl.novit.parser.parser.ParserLR1;
import nl.novit.parser.rule.Rule;

public class FactoryParserLR1
	implements FactoryParser
{
	public static final FactoryParser FACTORY_PARSER = new FactoryParserLR1();

	private FactoryParserLR1()
	{
	}

	@Override
	public <TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget> Parser<TypeTerminal, TypeTarget> parser(Rule<TypeTerminal, TypeTarget> rule)
		throws ExceptionParserAmbiguous
	{
		return new ParserLR1<>(rule);
	}
}