package nl.novit.parser.factory;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.parser.Parser;
import nl.novit.parser.parser.ParserUnger;
import nl.novit.parser.rule.Rule;

public class FactoryParserUnger
	implements FactoryParser
{
	public static final FactoryParser FACTORY_PARSER = new FactoryParserUnger();

	private FactoryParserUnger()
	{
	}

	@Override
	public <TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget> Parser<TypeTerminal, TypeTarget> parser(Rule<TypeTerminal, TypeTarget> rule)
		throws ExceptionParserAmbiguous
	{
		return new ParserUnger<>(rule);
	}
}