package nl.novit.parser.factory;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.parser.Parser;
import nl.novit.parser.parser.ParserLALR1;
import nl.novit.parser.rule.Rule;

public class FactoryParserLALR1
	implements FactoryParser
{
	public static final FactoryParser FACTORY_PARSER = new FactoryParserLALR1();

	private FactoryParserLALR1()
	{
	}

	@Override
	public <TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget> Parser<TypeTerminal, TypeTarget> parser(Rule<TypeTerminal, TypeTarget> rule)
		throws ExceptionParserAmbiguous
	{
		return new ParserLALR1<>(rule);
	}
}