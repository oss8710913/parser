package nl.novit.parser.factory;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.parser.Parser;
import nl.novit.parser.rule.Rule;

public interface FactoryParser
{
	<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget> Parser<TypeTerminal, TypeTarget> parser(Rule<TypeTerminal, TypeTarget> rule)
		throws ExceptionParserAmbiguous;
}