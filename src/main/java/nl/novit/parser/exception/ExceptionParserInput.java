package nl.novit.parser.exception;

public class ExceptionParserInput
	extends Exception
{
	public final int index;

	public ExceptionParserInput(int index)
	{
		this.index = index;
	}
}