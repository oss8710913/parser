package nl.novit.parser.util;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.common.Range;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.Parser;

import java.util.List;

public class ParserTwoStages<TypeTerminal extends Comparable<? super TypeTerminal>, TypeIntermediate extends Comparable<? super TypeIntermediate>, TypeTarget>
	implements Parser<TypeTerminal, TypeTarget>
{
	public final Parser<TypeTerminal, List<TypeIntermediate>> parserStageOne;
	public final Parser<TypeIntermediate, TypeTarget> parserStageTwo;

	public ParserTwoStages
		(
			Parser<TypeTerminal, List<TypeIntermediate>> parserStageOne,
			Parser<TypeIntermediate, TypeTarget> parserStageTwo
		)
	{
		this.parserStageOne = parserStageOne;
		this.parserStageTwo = parserStageTwo;
	}

	@Override
	public ParseTree<? extends TypeTarget> parse(List<TypeTerminal> input)
		throws ExceptionParserInput
	{
		ParseTree<? extends List<TypeIntermediate>> inputIntermediate = this.parserStageOne.parse(input);
		List<Range> ranges = ParseTree.convertTree(inputIntermediate);
		try
		{
			return this.parserStageTwo
				.parse(inputIntermediate.getTarget())
				.convertRange(ranges);
		}
		catch (ExceptionParserInput exception)
		{
			throw new ExceptionParserInput(ParseTree.convertIndex(ranges, exception.index));
		}
	}
}