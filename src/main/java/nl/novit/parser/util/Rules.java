package nl.novit.parser.util;

import nl.novit.parser.rule.Rule;
import nl.novit.parser.rule.RuleConcatenation;
import nl.novit.parser.rule.RuleEmpty;
import nl.novit.parser.rule.RuleIdentity;
import nl.novit.parser.rule.RuleTerminal;
import nl.novit.parser.rule.RuleUnion;
import nl.novit.util.Function1;
import nl.novit.util.Function2;
import nl.novit.util.Function3;
import nl.novit.util.Function4;
import nl.novit.util.Function5;
import nl.novit.util.Tuple2;
import nl.novit.util.Tuple3;
import nl.novit.util.Tuple4;
import nl.novit.util.Tuple5;
import nl.novit.util.Util;
import nl.novit.util.collections.ImmutableLinkedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Rules<TypeTerminal extends Comparable<? super TypeTerminal>>
{
	public final Map<TypeTerminal, RuleTerminal<TypeTerminal, TypeTerminal>> mapTerminal2Rule = new TreeMap<>();
	public final RuleEmpty<TypeTerminal, Void> empty =
		new RuleEmpty<>()
		{
			@Override
			public Void getTarget()
			{
				return null;
			}
		};
	public final Rule<TypeTerminal, Boolean> emptyTrue =
		identity1
			(
				x -> true,
				this.empty
			);
	public final Rule<TypeTerminal, Boolean> emptyFalse =
		identity1
			(
				x -> false,
				this.empty
			);

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> as(final Rule<TypeTerminal, Void> rule)
	{
		return
			identity1
				(
					x -> null,
					rule
				);
	}

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> empty(final TypeTarget target)
	{
		return
			identity1
				(
					x -> target,
					this.empty
				);
	}

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> empty()
	{
		return empty(null);
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> emptyList()
	{
		return
			identity1
				(
					x -> new ArrayList<>(),
					this.empty
				);
	}

	public <TypeTarget, Type0> Rule<TypeTerminal, TypeTarget> identity1
		(
			final Function1<TypeTarget, ? super Type0> function,
			final Rule<TypeTerminal, Type0> source
		)
	{
		return new RuleIdentity<>(source)
		{
			@Override
			public TypeTarget getTarget(final Type0 value)
			{
				return function.apply(value);
			}
		};
	}

	public <TypeTarget, Type0, Type1> Rule<TypeTerminal, TypeTarget> identity2
		(
			final Function2<TypeTarget, ? super Type0, ? super Type1> function,
			final Rule<TypeTerminal, Tuple2<Type0, Type1>> source
		)
	{
		return new RuleIdentity<>(source)
		{
			@Override
			public TypeTarget getTarget(final Tuple2<Type0, Type1> value)
			{
				return
					function.apply
						(
							value.value0,
							value.value1
						);
			}
		};
	}

	public <TypeTarget, Type0, Type1, Type2> Rule<TypeTerminal, TypeTarget> identity3
		(
			final Function3<TypeTarget, ? super Type0, ? super Type1, Type2> function,
			final Rule<TypeTerminal, Tuple3<Type0, Type1, Type2>> source
		)
	{
		return new RuleIdentity<>(source)
		{
			@Override
			public TypeTarget getTarget(final Tuple3<Type0, Type1, Type2> value)
			{
				return
					function.apply
						(
							value.value0,
							value.value1,
							value.value2
						);
			}
		};
	}

	public <TypeTarget, Type0, Type1, Type2, Type3> Rule<TypeTerminal, TypeTarget> identity4
		(
			final Function4<TypeTarget, ? super Type0, ? super Type1, ? super Type2, ? super Type3> function,
			final Rule<TypeTerminal, Tuple4<Type0, Type1, Type2, Type3>> source
		)
	{
		return new RuleIdentity<>(source)
		{
			@Override
			public TypeTarget getTarget(final Tuple4<Type0, Type1, Type2, Type3> value)
			{
				return
					function.apply
						(
							value.value0,
							value.value1,
							value.value2,
							value.value3
						);
			}
		};
	}

	public <TypeTarget, Type0, Type1, Type2, Type3, Type4> Rule<TypeTerminal, TypeTarget> identity5
		(
			final Function5<TypeTarget, ? super Type0, ? super Type1, ? super Type2, ? super Type3, ? super Type4> function,
			final Rule<TypeTerminal, Tuple5<Type0, Type1, Type2, Type3, Type4>> source
		)
	{
		return new RuleIdentity<>(source)
		{
			@Override
			public TypeTarget getTarget(Tuple5<Type0, Type1, Type2, Type3, Type4> value)
			{
				return
					function.apply
						(
							value.value0,
							value.value1,
							value.value2,
							value.value3,
							value.value4
						);
			}
		};
	}

	public Rule<TypeTerminal, Void> concatenation00
		(
			final Rule<TypeTerminal, Void> rule0,
			final Rule<TypeTerminal, Void> rule1
		)
	{
		return new RuleConcatenation<>(rule0, rule1)
		{
			@Override
			public Void getTarget
				(
					final Void left,
					final Void right
				)
			{
				return null;
			}
		};
	}

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> concatenation01
		(
			final Rule<TypeTerminal, Void> rule0,
			final Rule<TypeTerminal, TypeTarget> rule1
		)
	{
		return new RuleConcatenation<>(rule0, rule1)
		{
			@Override
			public TypeTarget getTarget
				(
					final Void left,
					final TypeTarget right
				)
			{
				return right;
			}
		};
	}

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> concatenation10
		(
			final Rule<TypeTerminal, TypeTarget> rule0,
			final Rule<TypeTerminal, Void> rule1
		)
	{
		return new RuleConcatenation<>(rule0, rule1)
		{
			@Override
			public TypeTarget getTarget
				(
					final TypeTarget left,
					final Void right
				)
			{
				return left;
			}
		};
	}

	public <TypeTarget0, TypeTarget1> Rule<TypeTerminal, Tuple2<TypeTarget0, TypeTarget1>> concatenation11
		(
			final Rule<TypeTerminal, TypeTarget0> rule0,
			final Rule<TypeTerminal, TypeTarget1> rule1
		)
	{
		return new RuleConcatenation<>(rule0, rule1)
		{
			@Override
			public Tuple2<TypeTarget0, TypeTarget1> getTarget
				(
					final TypeTarget0 left,
					final TypeTarget1 right
				)
			{
				return
					new Tuple2<>
						(
							left,
							right
						);
			}
		};
	}

	public <TypeTarget0, TypeTarget1, TypeTarget2> Rule<TypeTerminal, Tuple3<TypeTarget0, TypeTarget1, TypeTarget2>> concatenation12
		(
			final Rule<TypeTerminal, TypeTarget0> rule0,
			final Rule<TypeTerminal, Tuple2<TypeTarget1, TypeTarget2>> rule12
		)
	{
		return new RuleConcatenation<>(rule0, rule12)
		{
			@Override
			public Tuple3<TypeTarget0, TypeTarget1, TypeTarget2> getTarget
				(
					final TypeTarget0 left,
					final Tuple2<TypeTarget1, TypeTarget2> right
				)
			{
				return
					new Tuple3<>
						(
							left,
							right.value0,
							right.value1
						);
			}
		};
	}

	public <TypeTarget0, TypeTarget1, TypeTarget2> Rule<TypeTerminal, Tuple3<TypeTarget0, TypeTarget1, TypeTarget2>> concatenation21
		(
			final Rule<TypeTerminal, Tuple2<TypeTarget0, TypeTarget1>> rule01,
			final Rule<TypeTerminal, TypeTarget2> rule2
		)
	{
		return new RuleConcatenation<>(rule01, rule2)
		{
			@Override
			public Tuple3<TypeTarget0, TypeTarget1, TypeTarget2> getTarget
				(
					final Tuple2<TypeTarget0, TypeTarget1> left,
					final TypeTarget2 right
				)
			{
				return
					new Tuple3<>
						(
							left.value0,
							left.value1,
							right
						);
			}
		};
	}

	public <TypeTarget0, TypeTarget1, TypeTarget2> Rule<TypeTerminal, Tuple3<TypeTarget0, TypeTarget1, TypeTarget2>> concatenation111
		(
			final Rule<TypeTerminal, TypeTarget0> rule0,
			final Rule<TypeTerminal, TypeTarget1> rule1,
			final Rule<TypeTerminal, TypeTarget2> rule2
		)
	{
		return
			concatenation21
				(
					concatenation11
						(
							rule0,
							rule1
						),
					rule2
				);
	}

	public <TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3> Rule<TypeTerminal, Tuple4<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3>> concatenation13
		(
			final Rule<TypeTerminal, TypeTarget0> rule0,
			final Rule<TypeTerminal, Tuple3<TypeTarget1, TypeTarget2, TypeTarget3>> rule123
		)
	{
		return new RuleConcatenation<>(rule0, rule123)
		{
			@Override
			public Tuple4<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3> getTarget
				(
					final TypeTarget0 left,
					final Tuple3<TypeTarget1, TypeTarget2, TypeTarget3> right
				)
			{
				return
					new Tuple4<>
						(
							left,
							right.value0,
							right.value1,
							right.value2
						);
			}
		};
	}

	public <TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3> Rule<TypeTerminal, Tuple4<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3>> concatenation31
		(
			final Rule<TypeTerminal, Tuple3<TypeTarget0, TypeTarget1, TypeTarget2>> rule012,
			final Rule<TypeTerminal, TypeTarget3> rule3
		)
	{
		return new RuleConcatenation<>(rule012, rule3)
		{
			@Override
			public Tuple4<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3> getTarget
				(
					final Tuple3<TypeTarget0, TypeTarget1, TypeTarget2> left,
					final TypeTarget3 right
				)
			{
				return
					new Tuple4<>
						(
							left.value0,
							left.value1,
							left.value2,
							right
						);
			}
		};
	}

	public <TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3> Rule<TypeTerminal, Tuple4<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3>> concatenation1111
		(
			final Rule<TypeTerminal, TypeTarget0> rule0,
			final Rule<TypeTerminal, TypeTarget1> rule1,
			final Rule<TypeTerminal, TypeTarget2> rule2,
			final Rule<TypeTerminal, TypeTarget3> rule3
		)
	{
		return
			concatenation31
				(
					concatenation111
						(
							rule0, rule1, rule2
						),
					rule3
				);
	}

	public <TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4> Rule<TypeTerminal, Tuple5<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4>> concatenation14
		(
			final Rule<TypeTerminal, TypeTarget0> rule0,
			final Rule<TypeTerminal, Tuple4<TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4>> rule1234
		)
	{
		return new RuleConcatenation<>(rule0, rule1234)
		{
			@Override
			public Tuple5<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4> getTarget
				(
					final TypeTarget0 left,
					final Tuple4<TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4> right
				)
			{
				return
					new Tuple5<>
						(
							left,
							right.value0,
							right.value1,
							right.value2,
							right.value3
						);
			}
		};
	}

	public <TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4> Rule<TypeTerminal, Tuple5<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4>> concatenation41
		(
			final Rule<TypeTerminal, Tuple4<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3>> rule0123,
			final Rule<TypeTerminal, TypeTarget4> rule4
		)
	{
		return new RuleConcatenation<>(rule0123, rule4)
		{
			@Override
			public Tuple5<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4> getTarget
				(
					final Tuple4<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3> left,
					final TypeTarget4 right
				)
			{
				return
					new Tuple5<>
						(
							left.value0,
							left.value1,
							left.value2,
							left.value3,
							right
						);
			}
		};
	}

	public <TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4> Rule<TypeTerminal, Tuple5<TypeTarget0, TypeTarget1, TypeTarget2, TypeTarget3, TypeTarget4>> concatenation11111
		(
			final Rule<TypeTerminal, TypeTarget0> rule0,
			final Rule<TypeTerminal, TypeTarget1> rule1,
			final Rule<TypeTerminal, TypeTarget2> rule2,
			final Rule<TypeTerminal, TypeTarget3> rule3,
			final Rule<TypeTerminal, TypeTarget4> rule4
		)
	{
		return
			concatenation41
				(
					concatenation1111
						(
							rule0,
							rule1,
							rule2,
							rule3
						),
					rule4
				);
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> concatenationListList
		(
			final Rule<TypeTerminal, List<TypeTarget>> left,
			final Rule<TypeTerminal, List<TypeTarget>> right
		)
	{
		return new RuleConcatenation<>(left, right)
		{
			@Override
			public List<TypeTarget> getTarget
				(
					final List<TypeTarget> left,
					final List<TypeTarget> right
				)
			{
				List<TypeTarget> result = new ArrayList<>();
				result.addAll(left);
				result.addAll(right);
				return result;
			}
		};
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> concatenationElementElement
		(
			final Rule<TypeTerminal, TypeTarget> left,
			final Rule<TypeTerminal, TypeTarget> right
		)
	{
		return
			concatenationListList
				(
					list(left),
					list(right)
				);
	}

	public <TypeTarget, Left extends TypeTarget> Rule<TypeTerminal, List<TypeTarget>> concatenationElementList
		(
			final Rule<TypeTerminal, Left> left,
			final Rule<TypeTerminal, List<TypeTarget>> right
		)
	{
		return
			concatenationListList
				(
					list(left),
					right
				);
	}

	public <TypeTarget, Right extends TypeTarget> Rule<TypeTerminal, List<TypeTarget>> concatenationListElement
		(
			final Rule<TypeTerminal, List<TypeTarget>> left,
			final Rule<TypeTerminal, Right> right
		)
	{
		return
			concatenationListList
				(
					left,
					list(right)
				);
	}

	public Rule<TypeTerminal, List<TypeTerminal>> concatenationTerminals(final List<TypeTerminal> terminals)
	{
		final Rule<TypeTerminal, List<TypeTerminal>> result;
		if (terminals.isEmpty())
		{
			result = emptyList();
		}
		else
		{
			result =
				concatenationElementList
					(
						terminal(terminals.get(0)),
						concatenationTerminals(terminals.subList(1, terminals.size())));
		}
		return result;
	}

	public <TypeTarget> Rule<TypeTerminal, Void> dummy(final Rule<TypeTerminal, TypeTarget> rule)
	{
		return identity1(value -> null, rule);
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> flatten(final Rule<TypeTerminal, ? extends List<? extends List<? extends TypeTarget>>> rules)
	{
		return
			identity1
				(
					lists ->
						lists
							.stream()
							.flatMap(Collection::stream)
							.collect(Collectors.toList()),
					rules
				);
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> list(final Rule<TypeTerminal, ? extends TypeTarget> rule)
	{
		return
			identity1
				(
					x ->
					{
						List<TypeTarget> result = new ArrayList<>();
						result.add(x);
						return result;
					},
					rule
				);
	}

	public <TypeTarget extends Comparable<TypeTarget>> Rule<TypeTerminal, List<TypeTarget>> keywordOrNot
		(
			final Iterable<TypeTerminal> terminals,
			final Iterable<List<TypeTerminal>> keywords,
			final Function1<TypeTarget, TypeTerminal> convert,
			final TypeTarget prefixKeyword,
			final TypeTarget prefixNotKeyword
		)
	{
		final Set<List<TypeTarget>> setKeywords = new TreeSet<>(Util::compareIterable);
		keywords.forEach
			(
				keyword ->
					setKeywords.add
						(
							keyword
								.stream()
								.map(convert::apply)
								.collect(Collectors.toList())
						)
			);
		return
			identity3
				(
					(targetKeyword, targetNotkeyword, result) ->
					{
						final TypeTarget prefix = setKeywords.contains(result)
							? targetKeyword
							: targetNotkeyword;
						result.add(0, prefix);
						return result;
					},
					concatenation111
						(
							empty(prefixKeyword),
							empty(prefixNotKeyword),
							repetition
								(
									identity1
										(
											convert,
											unionTerminals(terminals)
										)
								)
						)
				);
	}

	public Rule<TypeTerminal, List<TypeTerminal>> notKeyword
		(
			final Iterable<TypeTerminal> terminals,
			final Iterable<List<TypeTerminal>> keywords
		)
	{
		class TreeKeyword
		{
			private final TreeMap<TypeTerminal, TreeKeyword> children;
			private boolean isEnd;

			public TreeKeyword()
			{
				this.children = new TreeMap<>();
				this.isEnd = false;
			}

			public void add(final List<TypeTerminal> keyword)
			{
				if (keyword.isEmpty())
				{
					this.isEnd = true;
				}
				else
				{
					TreeKeyword treeKeyword = this.children.computeIfAbsent(keyword.get(0), terminal -> new TreeKeyword());
					treeKeyword.add(keyword.subList(1, keyword.size()));
				}
			}

			public Rule<TypeTerminal, List<TypeTerminal>> rule(final Rule<TypeTerminal, List<TypeTerminal>> ruleTerminals)
			{
				final RuleUnion<TypeTerminal, List<TypeTerminal>> result = new RuleUnion<>();
				final Set<TypeTerminal> terminalsFree = new TreeSet<>();
				for (final TypeTerminal terminal: terminals)
				{
					TreeKeyword child = this.children.get(terminal);
					if (child == null)
					{
						terminalsFree.add(terminal);
					}
					else
					{
						result.add(concatenationElementList(terminal(terminal), child.rule(ruleTerminals)));
					}
				}
				result.add(concatenationElementList(unionTerminals(terminalsFree), ruleTerminals));
				if (!this.isEnd)
				{
					result.add(emptyList());
				}
				return result;
			}
		}
		final TreeKeyword treeKeyword = new TreeKeyword();
		keywords.forEach(treeKeyword::add);
		return treeKeyword.rule(optionList(repetition(unionTerminals(terminals))));
	}

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> option(final Rule<TypeTerminal, TypeTarget> rule)
	{
		return
			union
				(
					empty(),
					rule
				);
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> optionList(final Rule<TypeTerminal, List<TypeTarget>> rule)
	{
		return
			union
				(
					emptyList(),
					rule
				);
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> repetition
		(
			final Rule<TypeTerminal, TypeTarget> rule,
			final Rule<TypeTerminal, Void> separator
		)
	{
		final Rule<TypeTerminal, TypeTarget> element =
			concatenation01
				(
					separator,
					rule
				);
		final RuleUnion<TypeTerminal, ImmutableLinkedList<TypeTarget>> list = new RuleUnion<>();
		list.add
			(
				new RuleEmpty<>()
				{
					@Override
					public ImmutableLinkedList<TypeTarget> getTarget()
					{
						return new ImmutableLinkedList<>();
					}
				}
			);
		list.add
			(
				new RuleConcatenation<>(element, list)
				{
					@Override
					public ImmutableLinkedList<TypeTarget> getTarget(TypeTarget element, ImmutableLinkedList<TypeTarget> list)
					{
						return list.addFirst(element);
					}
				}
			);
		return
			new RuleConcatenation<>(rule, list)
			{
				@Override
				public List<TypeTarget> getTarget(TypeTarget element, ImmutableLinkedList<TypeTarget> list)
				{
					return new ArrayList<>(list.addFirst(element));
				}
			};
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> repetition(final Rule<TypeTerminal, TypeTarget> rule)
	{
		return
			repetition
				(
					rule,
					this.empty
				);
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> repetitionLR(final Rule<TypeTerminal, TypeTarget> rule)
	{
		final RuleUnion<TypeTerminal, List<TypeTarget>> result = new RuleUnion<>();
		result.add(list(rule));
		result.add
			(
				concatenationListElement
					(
						result,
						rule
					)
			);
		return result;
	}

	public Rule<TypeTerminal, TypeTerminal> terminal(final TypeTerminal terminal)
	{
		RuleTerminal<TypeTerminal, TypeTerminal> result = this.mapTerminal2Rule.get(terminal);
		if (result == null)
		{
			result = new RuleTerminal<>(terminal)
			{
				public TypeTerminal getTarget()
				{
					return terminal.terminal;
				}
			};
			this.mapTerminal2Rule.put(terminal, result);
		}
		return result;
	}

	public <TypeTarget> Rule<TypeTerminal, TypeTarget> terminal
		(
			final TypeTerminal terminal,
			final TypeTarget target
		)
	{
		return
			identity1
				(
					x -> target,
					terminal(terminal)
				);
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Iterable<Rule<TypeTerminal, ? extends TypeTarget>> rules
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		for (Rule<TypeTerminal, ? extends TypeTarget> rule: rules)
		{
			result.add(rule);
		}
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0,
			final Rule<TypeTerminal, ? extends TypeTarget> rule1
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		result.add(rule1);
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0,
			final Rule<TypeTerminal, ? extends TypeTarget> rule1,
			final Rule<TypeTerminal, ? extends TypeTarget> rule2
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		result.add(rule1);
		result.add(rule2);
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0,
			final Rule<TypeTerminal, ? extends TypeTarget> rule1,
			final Rule<TypeTerminal, ? extends TypeTarget> rule2,
			final Rule<TypeTerminal, ? extends TypeTarget> rule3
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		result.add(rule1);
		result.add(rule2);
		result.add(rule3);
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0,
			final Rule<TypeTerminal, ? extends TypeTarget> rule1,
			final Rule<TypeTerminal, ? extends TypeTarget> rule2,
			final Rule<TypeTerminal, ? extends TypeTarget> rule3,
			final Rule<TypeTerminal, ? extends TypeTarget> rule4
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		result.add(rule1);
		result.add(rule2);
		result.add(rule3);
		result.add(rule4);
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0,
			final Rule<TypeTerminal, ? extends TypeTarget> rule1,
			final Rule<TypeTerminal, ? extends TypeTarget> rule2,
			final Rule<TypeTerminal, ? extends TypeTarget> rule3,
			final Rule<TypeTerminal, ? extends TypeTarget> rule4,
			final Rule<TypeTerminal, ? extends TypeTarget> rule5
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		result.add(rule1);
		result.add(rule2);
		result.add(rule3);
		result.add(rule4);
		result.add(rule5);
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0,
			final Rule<TypeTerminal, ? extends TypeTarget> rule1,
			final Rule<TypeTerminal, ? extends TypeTarget> rule2,
			final Rule<TypeTerminal, ? extends TypeTarget> rule3,
			final Rule<TypeTerminal, ? extends TypeTarget> rule4,
			final Rule<TypeTerminal, ? extends TypeTarget> rule5,
			final Rule<TypeTerminal, ? extends TypeTarget> rule6
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		result.add(rule1);
		result.add(rule2);
		result.add(rule3);
		result.add(rule4);
		result.add(rule5);
		result.add(rule6);
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0,
			final Rule<TypeTerminal, ? extends TypeTarget> rule1,
			final Rule<TypeTerminal, ? extends TypeTarget> rule2,
			final Rule<TypeTerminal, ? extends TypeTarget> rule3,
			final Rule<TypeTerminal, ? extends TypeTarget> rule4,
			final Rule<TypeTerminal, ? extends TypeTarget> rule5,
			final Rule<TypeTerminal, ? extends TypeTarget> rule6,
			final Rule<TypeTerminal, ? extends TypeTarget> rule7
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		result.add(rule1);
		result.add(rule2);
		result.add(rule3);
		result.add(rule4);
		result.add(rule5);
		result.add(rule6);
		result.add(rule7);
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0,
			final Rule<TypeTerminal, ? extends TypeTarget> rule1,
			final Rule<TypeTerminal, ? extends TypeTarget> rule2,
			final Rule<TypeTerminal, ? extends TypeTarget> rule3,
			final Rule<TypeTerminal, ? extends TypeTarget> rule4,
			final Rule<TypeTerminal, ? extends TypeTarget> rule5,
			final Rule<TypeTerminal, ? extends TypeTarget> rule6,
			final Rule<TypeTerminal, ? extends TypeTarget> rule7,
			final Rule<TypeTerminal, ? extends TypeTarget> rule8
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		result.add(rule1);
		result.add(rule2);
		result.add(rule3);
		result.add(rule4);
		result.add(rule5);
		result.add(rule6);
		result.add(rule7);
		result.add(rule8);
		return result;
	}

	public <TypeTarget> RuleUnion<TypeTerminal, TypeTarget> union
		(
			final Rule<TypeTerminal, ? extends TypeTarget> rule0,
			final Rule<TypeTerminal, ? extends TypeTarget> rule1,
			final Rule<TypeTerminal, ? extends TypeTarget> rule2,
			final Rule<TypeTerminal, ? extends TypeTarget> rule3,
			final Rule<TypeTerminal, ? extends TypeTarget> rule4,
			final Rule<TypeTerminal, ? extends TypeTarget> rule5,
			final Rule<TypeTerminal, ? extends TypeTarget> rule6,
			final Rule<TypeTerminal, ? extends TypeTarget> rule7,
			final Rule<TypeTerminal, ? extends TypeTarget> rule8,
			final Rule<TypeTerminal, ? extends TypeTarget> rule9
		)
	{
		final RuleUnion<TypeTerminal, TypeTarget> result = new RuleUnion<>();
		result.add(rule0);
		result.add(rule1);
		result.add(rule2);
		result.add(rule3);
		result.add(rule4);
		result.add(rule5);
		result.add(rule6);
		result.add(rule7);
		result.add(rule8);
		result.add(rule9);
		return result;
	}

	public RuleUnion<TypeTerminal, TypeTerminal> unionTerminals
		(
			final Iterable<? extends TypeTerminal> terminals
		)
	{
		final RuleUnion<TypeTerminal, TypeTerminal> result = new RuleUnion<>();
		for (TypeTerminal terminal: terminals)
		{
			result.add(terminal(terminal));
		}
		return result;
	}

	public <TypeTarget> Rule<TypeTerminal, List<TypeTarget>> alternating
		(
			final Rule<TypeTerminal, ? extends TypeTarget> ruleA,
			final Rule<TypeTerminal, ? extends TypeTarget> ruleB
		)
	{
		final RuleUnion<TypeTerminal, List<TypeTarget>> ruleAB = new RuleUnion<>();
		final RuleUnion<TypeTerminal, List<TypeTarget>> ruleBA = new RuleUnion<>();
		ruleAB.add
			(
				emptyList()
			);
		ruleBA.add
			(
				emptyList()
			);
		ruleAB.add
			(
				concatenationElementList
					(
						ruleA,
						ruleBA
					)
			);
		ruleBA.add
			(
				concatenationElementList
					(
						ruleB,
						ruleAB
					)
			);
		return
			concatenationListList
				(
					union
						(
							emptyList(),
							list(ruleA)
						),
					ruleBA
				);
	}
}