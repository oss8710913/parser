package nl.novit.parser.util;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.Parser;
import nl.novit.util.Util;

public class StringParserDefault<TypeTarget>
	implements StringParser<TypeTarget>
{
	public final Parser<Character, TypeTarget> parser;

	public StringParserDefault(Parser<Character, TypeTarget> parser)
	{
		this.parser = parser;
	}

	public ParseTree<? extends TypeTarget> parse(String input)
		throws ExceptionParserInput
	{
		return this.parser.parse(Util.convert(input));
	}
}