package nl.novit.parser.util;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.factory.FactoryParser;
import nl.novit.parser.parser.Parser;
import nl.novit.parser.rule.Rule;

import java.util.List;

public class ParserLazy<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	implements Parser<TypeTerminal, TypeTarget>
{
	public final FactoryParser factory;
	public final Rule<TypeTerminal, TypeTarget> ruleStart;
	private Parser<TypeTerminal, TypeTarget> parser;

	public ParserLazy(FactoryParser factory, Rule<TypeTerminal, TypeTarget> ruleStart)
	{
		this.factory = factory;
		this.ruleStart = ruleStart;
		this.parser = null;
	}

	@Override
	public ParseTree<? extends TypeTarget> parse(List<TypeTerminal> input)
		throws ExceptionParserInput
	{
		if (this.parser == null)
		{
			this.parser = this.factory.parser(this.ruleStart);
		}
		return this.parser.parse(input);
	}
}