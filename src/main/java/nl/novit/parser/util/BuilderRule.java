package nl.novit.parser.util;

import nl.novit.parser.rule.Rule;

public abstract class BuilderRule<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends Rules<TypeTerminal>
{
	public abstract Rule<TypeTerminal, TypeTarget> rule();
}