package nl.novit.parser.util;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.util.UtilCollections;

import java.util.Set;

public interface StringParser<TypeTarget>
{
	Set<Character> CHARACTERS_LOWERCASE = Set.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
	Set<Character> CHARACTERS_UPPERCASE = Set.of('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
	Set<Character> CHARACTERS = UtilCollections.union(CHARACTERS_LOWERCASE, CHARACTERS_UPPERCASE);
	Set<Character> DIGITS = Set.of('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
	Set<Character> WHITESPACES = Set.of(' ', '\t', '\n');

	ParseTree<? extends TypeTarget> parse(String string)
		throws ExceptionParserInput;
}