package nl.novit.parser.parser;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.ExtendedItem;
import nl.novit.parser.internal.InputStackReverse;
import nl.novit.parser.internal.StackRuleOrTerminalOrEmpty;
import nl.novit.parser.internal.State;
import nl.novit.parser.rule.Rule;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

public abstract class ParserLR<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget, TypeState extends State<TypeTerminal, TypeState>>
	extends ParserDefault<TypeTerminal, TypeTarget>
{
	public static final System.Logger LOGGER = System.getLogger(ParserLR.class.getCanonicalName());
	public TypeState state;

	public ParserLR(Rule<TypeTerminal, TypeTarget> ruleStart)
		throws ExceptionParserAmbiguous
	{
		super(ruleStart);
	}

	public abstract TypeState getState(Set<ExtendedItem<TypeTerminal, ?>> extendedItems)
		throws ExceptionParserAmbiguous;

	public TypeState getState()
	{
		return this.state;
	}

	public void setState(TypeState state)
	{
		this.state = state;
	}

	public Stack<Rule<TypeTerminal, ?>> parseToRuleStack(List<TypeTerminal> input)
		throws ExceptionParserInput
	{
		Stack<Rule<TypeTerminal, ?>> result = new Stack<>();
		StackRuleOrTerminalOrEmpty<TypeTerminal> stack = new StackRuleOrTerminalOrEmpty<>(input);
		while (stack.size() != 1 || !this.getRuleStart().equals(stack.peek()))
		{
			this.getState().parse(stack, result, input.size());
		}
		return result;
	}

	public ParseTree<? extends TypeTarget> parse(List<TypeTerminal> input)
		throws ExceptionParserInput
	{
		return this.getRuleStart().parseLR(new InputStackReverse<>(input), parseToRuleStack(input));
	}

	@Override
	public void preparse(Rule<TypeTerminal, TypeTarget> ruleStart)
		throws ExceptionParserAmbiguous
	{
		this.state = getState(ruleStart.extendedItems(new Empty<>()));
		Set<TypeState> changed = new TreeSet<>();
		changed.add(this.state);
		Map<TypeState, TypeState> existingStates = new TreeMap<>();
		existingStates.put(this.state, this.state);
		while (!changed.isEmpty())
		{
			Set<TypeState> changedNext = new TreeSet<>();
			for (TypeState state: changed)
			{
				state.statesNext
					(
						existingStates,
						changedNext
					);
			}
			changed = changedNext;
		}
	}
}