package nl.novit.parser.parser;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.InputImmutable;
import nl.novit.parser.rule.Rule;

import java.util.List;
import java.util.TreeSet;

public class ParserUnger<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends ParserDefault<TypeTerminal, TypeTarget>
{
	public ParserUnger(Rule<TypeTerminal, TypeTarget> ruleStart)
		throws ExceptionParserAmbiguous
	{
		super(ruleStart);
	}

	@Override
	public ParseTree<? extends TypeTarget> parse(List<TypeTerminal> input)
		throws ExceptionParserInput
	{
		return this.ruleStart.parseUnger(new InputImmutable<>(input), new TreeSet<>());
	}

	@Override
	public void preparse(Rule<TypeTerminal, TypeTarget> ruleStart)
	{
	}
}