package nl.novit.parser.parser;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.internal.ExtendedItem;
import nl.novit.parser.internal.StateLALR1;
import nl.novit.parser.rule.Rule;

import java.util.Set;

public class ParserLALR1<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends ParserLR<TypeTerminal, TypeTarget, StateLALR1<TypeTerminal>>
{
	public static final System.Logger LOGGER = System.getLogger(ParserLALR1.class.getCanonicalName());

	public ParserLALR1(Rule<TypeTerminal, TypeTarget> ruleStart)
		throws ExceptionParserAmbiguous
	{
		super(ruleStart);
	}

	@Override
	public StateLALR1<TypeTerminal> getState(Set<ExtendedItem<TypeTerminal, ?>> extendedItems)
		throws ExceptionParserAmbiguous
	{
		return new StateLALR1<>(extendedItems);
	}
}