package nl.novit.parser.parser;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.internal.Empty;
import nl.novit.parser.internal.InputStackForward;
import nl.novit.parser.rule.Rule;

import java.util.List;
import java.util.Set;
import java.util.TreeMap;

public class ParserLL1<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends ParserDefault<TypeTerminal, TypeTarget>
{
	public static final System.Logger LOGGER = System.getLogger(ParserLL1.class.getCanonicalName());

	public ParserLL1(Rule<TypeTerminal, TypeTarget> ruleStart)
		throws ExceptionParserAmbiguous
	{
		super(ruleStart);
	}

	@Override
	public ParseTree<? extends TypeTarget> parse(List<TypeTerminal> input)
		throws ExceptionParserInput
	{
		InputStackForward<TypeTerminal> stack = new InputStackForward<>(input);
		ParseTree<? extends TypeTarget> result = this.ruleStart.parseLL1(stack);
		if (!stack.isEmpty())
		{
			throw new ExceptionParserInput(stack.getRange().start());
		}
		return result;
	}

	@Override
	public void preparse(Rule<TypeTerminal, TypeTarget> ruleStart)
		throws ExceptionParserAmbiguous
	{
		ruleStart.checkLL1(new TreeMap<>(), Set.of(new Empty<>()));
	}
}