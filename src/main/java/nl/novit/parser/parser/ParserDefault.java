package nl.novit.parser.parser;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.rule.Rule;

public abstract class ParserDefault<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	implements Parser<TypeTerminal, TypeTarget>
{
	public final Rule<TypeTerminal, TypeTarget> ruleStart;

	public ParserDefault(Rule<TypeTerminal, TypeTarget> ruleStart)
		throws ExceptionParserAmbiguous
	{
		this.ruleStart = ruleStart;
		this.ruleStart.initializeFirstTerminals();
		preparse(this.ruleStart);
	}

	public abstract void preparse(Rule<TypeTerminal, TypeTarget> ruleStart)
		throws ExceptionParserAmbiguous;

	public Rule<TypeTerminal, TypeTarget> getRuleStart()
	{
		return this.ruleStart;
	}
}