package nl.novit.parser.parser;

import nl.novit.parser.exception.ExceptionParserAmbiguous;
import nl.novit.parser.internal.ExtendedItem;
import nl.novit.parser.internal.StateLR1;
import nl.novit.parser.rule.Rule;

import java.util.Set;

public class ParserLR1<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
	extends ParserLR<TypeTerminal, TypeTarget, StateLR1<TypeTerminal>>
{
	public static final System.Logger LOGGER = System.getLogger(ParserLR1.class.getCanonicalName());

	public ParserLR1(Rule<TypeTerminal, TypeTarget> ruleStart)
		throws ExceptionParserAmbiguous
	{
		super(ruleStart);
	}

	@Override
	public StateLR1<TypeTerminal> getState(Set<ExtendedItem<TypeTerminal, ?>> extendedItems)
		throws ExceptionParserAmbiguous
	{
		return new StateLR1<>(extendedItems);
	}
}