package nl.novit.parser.parser;

import nl.novit.parser.common.ParseTree;
import nl.novit.parser.exception.ExceptionParserInput;

import java.util.List;

public interface Parser<TypeTerminal extends Comparable<? super TypeTerminal>, TypeTarget>
{
	ParseTree<? extends TypeTarget> parse(List<TypeTerminal> input)
		throws ExceptionParserInput;
}