package nl.novit.parser.common;

import nl.novit.util.Tuple2Comparable;

public class Range
	extends Tuple2Comparable<Integer, Integer>
{
	public Range(Integer start, Integer end)
	{
		super(start, end);
	}

	public Integer start()
	{
		return this.value0;
	}

	public Integer end()
	{
		return this.value1;
	}

	public boolean isEmpty()
	{
		return this.size() == 0;
	}

	public Range head()
	{
		return new Range(this.start(), this.end() - 1);
	}

	public Range tail()
	{
		return new Range(this.start() + 1, this.end());
	}

	public int size()
	{
		return this.end() - this.start();
	}
}