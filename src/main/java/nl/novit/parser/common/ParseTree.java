package nl.novit.parser.common;

import nl.novit.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class ParseTree<TypeTarget>
{
	public static int convertIndex(List<Range> ranges, int index)
	{
		return index == 0 ? 0 : ranges.get(index - 1).end();
	}

	public static List<Range> convertTree(final ParseTree<? extends List<?>> tree)
	{
		List<Range> result = new ArrayList<>();
		List<?> target = new ArrayList<>(tree.getTarget());
		tree.convertTree(target, result);
		if (!target.isEmpty())
		{
			result = tree.getTarget()
				.stream()
				.map(terminal -> tree.getRange())
				.collect(Collectors.toList());
		}
		return result;
	}

	public final TypeTarget target;
	public final List<ParseTree<?>> children;
	public final Range range;

	public ParseTree
		(
			TypeTarget target,
			List<ParseTree<?>> children,
			Range range
		)
	{
		this.target = target;
		this.children = children;
		this.range = range;
	}

	public ParseTree(TypeTarget target, List<ParseTree<?>> children)
	{
		this(target, children, new Range(children.get(0).getRange().start(), children.get(children.size() - 1).getRange().end()));
	}

	public ParseTree(TypeTarget target, Range range)
	{
		this(target, Collections.emptyList(), range);
	}

	public TypeTarget getTarget()
	{
		return this.target;
	}

	public List<ParseTree<?>> getChildren()
	{
		return this.children;
	}

	public Range getRange()
	{
		return this.range;
	}

	public boolean equals(ParseTree<?> that)
	{
		boolean result = Util.equalsNullable(this.target, that.target) && this.range.equals(that.range) && this.children.size() == that.children.size();
		for (int index = 0; result && index < this.children.size(); index++)
		{
			result = this.children.get(index).equals(that.children.get(index));
		}
		return result;
	}

	@Override
	public boolean equals(Object that)
	{
		return that instanceof ParseTree<?> && equals((ParseTree<?>) that);
	}

	public void convertTree(List<?> target, List<Range> result)
	{
		if (!target.isEmpty())
		{
			if (target.get(0) == getTarget())
			{
				target.remove(0);
				result.add(getRange());
			}
			else
			{
				for (ParseTree<?> child: getChildren())
				{
					child.convertTree(target, result);
				}
			}
		}
	}

	public ParseTree<TypeTarget> convertRange(final List<Range> ranges)
	{
		return new ParseTree<>
			(
				getTarget(),
				getChildren()
					.stream()
					.map(child -> child.convertRange(ranges))
					.collect(Collectors.toList()),
				new Range(convertIndex(ranges, getRange().start()), convertIndex(ranges, getRange().end()))
			);
	}

	public ParseTree<?> find(Object target)
	{
		ParseTree<?> result = null;
		Iterator<ParseTree<?>> iterator = getChildren().iterator();
		while (result == null && iterator.hasNext())
		{
			ParseTree<?> child = iterator.next();
			result = child.find(target);
		}
		if (result == null && getTarget() == target)
		{
			result = this;
		}
		return result;
	}
}