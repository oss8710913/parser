# Parser

This library offers a number of different parsing techniques for Java developers. With LL1, LR1 and LALR1 parsing it is covering a broad class of grammars.

A grammar is defined by interacting rules that simultaneously define the AST that is to be produced. No intermediate parser is generated. Rules and produced AST are completely typesafe, as can be seen in [this showcase example](src/test/java/integration/example6/TestExample6.java).


## Installation

This library depends on the [util library](https://gitlab.com/oss8710913/util/-/tree/v1.0.6?ref_type=tags). Please follow its 'Installation' instructions first.

Install this library by downloading it 

	git clone https://gitlab.com/oss8710913/parser.git

and executing

	./gradlew publishToMavenLocal

in its root folder.


## Usage

Add the following to the 'build.gradle' file of your project.

	repositories {
		...
		mavenLocal()
		...
	}

	dependencies {
		...
		implementation('nl.novit.util:util:1.2.2')
		implementation('nl.novit.parser:parser:1.4.1')
		...
	}


## Support
Please use 'GitLab Issues' for support on this library. Support is not guaranteed.
